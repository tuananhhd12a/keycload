﻿namespace CDB_BaoCao.ViewModels.Output
{
    public class BaoCaoDoiTuongTruyenTaiDuLieuResponsive
    {
        public string TenSoGTVT { get; set; }
        public string TenDonVi { get; set; }
        public int SoLuongXe { get; set; }
        public int SoLuongTuyen { get; set; }
        public int SoKhach_XuatBen { get; set; }
        public int SoKhach_DenBen { get; set; }
        public int SoLenh_XuatBen_CoDinh { get; set; }
        public int SoLenh_XuatBen_TangCuong { get; set; }
        public int SoLenh_XeBuyt { get; set; }
        public int SoLenh_DenBen_CoDinh { get; set; }
        public int SoLenh_DenBen_TangCuong { get; set; }
        public int SoLenh_TruyenTaiThanhCong { get; set; }
        public int SoLenh_TruyenTaiThatBai { get; set; }
        public int SoNgay_DaTruyenTai { get; set; }
        public DateTime LanTruyenTaiCuoi { get; set; }
    }
}
