﻿namespace CDB_BaoCao.ViewModels.Output
{
    public class BaoCaoDonViTruyenTaiResponsive
    {
        public Guid IdTruyenTai { get; set; }
        public string TenSoGTVT { get; set; }
        public string TenDonViTruyenNhan { get; set; }
        public string TenDonViTruyenTai { get; set; }
        public string TenLoaiTruyenTai { get; set; }
        public string TenDoiTuongTuyenTai { get; set; }
        public string MaLenhVanChuyen { get; set; }
        public int? MaLoi { get; set; }
        public string? MoTaLoi { get; set; }
        public DateTime ThoiDiemTruyenTai { get; set; }
        public string IdTrangThai { get; set; }
        public string TrangThai { get; set; }
        public string MaMauTruyenTai { get; set; }
    }
}
