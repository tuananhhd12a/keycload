﻿namespace CDB_BaoCao.ViewModels.Output
{
    public class DanhSachDonViTruyenTaiResponsive
    {
        public string MaSoThue { get; set; }
        public string TenDonVi { get; set; }
        public string DoiTuongTruyenTai { get; set; }
        public string MaSoGTVT { get; set; }
    }
}
