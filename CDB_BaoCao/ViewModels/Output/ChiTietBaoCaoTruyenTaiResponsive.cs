﻿namespace CDB_BaoCao.ViewModels.Output
{
    public class ChiTietBaoCaoTruyenTaiResponsive
    {
        public Guid IdTruyenTai { get; set; }
        public string TenDonViTruyenTai { get; set; }
        public string MaSoLenh { get; set; }
        public string LoaiTruyenTai { get; set; }
        public string TenDoiTuongTruyenTai { get; set; }
        public string TenDonViTruyenNhan { get; set; }
        public int MaLoi { get; set; }
        public string MoTaLoi { get; set; }
        public string DuLieuNhanDuoc { get; set; }
        public string FileXml { get; set; }
        public List<string> TepDinhKems { get; set; }
        public DateTime ThoiDiemTruyenTai { get; set; }
        public string TrangThaiTruyenTai { get; set; }
        public string MaMauTrangThaiTruyenTai { get; set; }
    }
}
