﻿namespace CDB_BaoCao.ViewModels.Input
{
    public class DanhSachDonViTruyenTaiParam
    {
        public string MaSoGTVT { get; set; }
        public string IdDoiTuongTruyenTai { get; set; }
    }
}
