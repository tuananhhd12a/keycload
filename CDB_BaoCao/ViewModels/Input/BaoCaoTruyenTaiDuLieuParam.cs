﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace CDB_BaoCao.ViewModels.Input
{
    public class BaoCaoTruyenTaiDuLieuParam
    {
        [Display(Name = "Mã sở GTVT")]
        public string MaSoGTVT { get; set; }

        [Display(Name = "Id Đối Tượng Truyền Tải")]
        public string IdDoiTuongTruyenTai { get; set; }

        public DateTime TuNgay { get; set; }

        public DateTime DenNgay { get; set; }
    }
}
