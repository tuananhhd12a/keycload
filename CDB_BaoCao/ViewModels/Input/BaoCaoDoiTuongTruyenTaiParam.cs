﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace CDB_BaoCao.ViewModels.Input
{
    public class BaoCaoDoiTuongTruyenTaiParam
    {
        [Display(Name = "Mã sở GTVT")]
        public string MaSoGTVT { get; set; }

        public DateTime TuNgay { get; set; }

        public DateTime DenNgay { get; set; }
    }
}
