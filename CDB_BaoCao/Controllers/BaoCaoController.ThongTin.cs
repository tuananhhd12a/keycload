﻿using CDB_BaoCao.ViewModels.Input;
using CDB_BaoCao.ViewModels.Output;
using Lib_Core.Objects.Parameters;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CDB_BaoCao.Controllers
{
    public partial class BaoCaoController
    {
        /// <summary>
        /// Danh sách sở giao thông vận tải
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<List<DanhSachSoGTVTResponsive>>))]
        [HttpGet("danh-sach-so-giao-thong-van-tai")]
        [Authorize()]
        public IActionResult DanhSachSoGTVT()
        {
            return Ok(baoCaoService.DanhSachSoGTVT());
        }

        /// <summary>
        /// Danh sách đối tượng truyền tải
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<List<DanhSachDoiTuongTuyenTaiResponsive>>))]
        [HttpGet("danh-sach-doi-tuong-truyen-tai")]
        [Authorize()]
        public IActionResult DanhSachDoiTuongTruyenTai()
        {
            return Ok(baoCaoService.DanhSachDoiTuongTruyenTai());
        }

    }
}
