﻿using Lib_Core.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CDB_BaoCao.Controllers
{
    public partial class BaoCaoController
    {
        [HttpGet("xem-file")]
        [Authorize(Roles = EPermission.BAO_CAO_XEM_FILE)]
        public IActionResult XemFile([FromQuery] string path)
        {
            try
            {
                var file = baoCaoService.LayFile(path, out string contentType, out string fileName);
                if (file == null)
                {
                    return NotFound();
                }
                return File(file, contentType);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("tai-file")]
        [Authorize(Roles = EPermission.BAO_CAO_TAI_FILE)]
        public IActionResult TaiFile([FromQuery] string path)
        {
            try
            {
                var file = baoCaoService.LayFile(path, out string contentType, out string fileName);
                if (file == null)
                {
                    return NotFound();
                }
                return File(file, contentType, fileName);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
