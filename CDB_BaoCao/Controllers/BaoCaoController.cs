﻿using CDB_BaoCao.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CDB_BaoCao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class BaoCaoController : ControllerBase
    {
        private readonly BaoCaoService baoCaoService;

        public BaoCaoController(BaoCaoService baoCaoService)
        {
            this.baoCaoService = baoCaoService;
        }
    }
}
