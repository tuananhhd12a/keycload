﻿using CDB_BaoCao.ViewModels.Output;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CDB_BaoCao.Controllers
{
    public partial class BaoCaoController
    {
        /// <summary>
        /// Chi tiết báo cáo truyền tải dữ liệu
        /// </summary>
        /// <param name="idTruyenTai"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<ChiTietBaoCaoTruyenTaiResponsive>))]
        [HttpGet("chi-tiet-bao-cao-truyen-tai-du-lieu")]
        [Authorize(Roles = EPermission.CHI_TIET_BAO_CAO_TRUYEN_TAI_DU_LIEU)]
        public IActionResult ChiTietBaoCaoTruyenTai([FromQuery] Guid idTruyenTai)
        {
            return Ok(baoCaoService.ChiTietBaoCaoTruyenTai(idTruyenTai));
        }
    }
}
