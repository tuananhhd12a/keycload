﻿using CDB_BaoCao.ViewModels.Input;
using CDB_BaoCao.ViewModels.Output;
using Lib_Core.Enum;
using Lib_Core.Objects.Parameters;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CDB_BaoCao.Controllers
{
    public partial class BaoCaoController
    {
        /// <summary>
        /// Báo cáo truyền tải dữ liệu
        /// </summary>
        /// <param name="devExData"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<BaoCaoDonViTruyenTaiResponsive>>))]
        [HttpPost("bao-cao-truyen-tai-du-lieu")]
        [Authorize(Roles = EPermission.DANH_SACH_BAO_CAO_TRUYEN_TAI_DU_LIEU)]
        public IActionResult BaoCaoTruyenTaiDuLieu([FromBody] DevExDataSourceLoadOptions<BaoCaoTruyenTaiDuLieuParam> devExData)
        {
            return Ok(baoCaoService.BaoCaoTruyenTaiDuLieu(devExData, User.ThongTin()));
        }

        /// <summary>
        /// Báo cáo truyền tải dữ liệu của doanh nghiệp
        /// </summary>
        /// <param name="devExData"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<BaoCaoDoiTuongTruyenTaiDuLieuResponsive>>))]
        [HttpPost("bao-cao-truyen-tai-du-lieu-cua-doanh-nghiep")]
        [Authorize(Roles = EPermission.DANH_SACH_BAO_CAO_TRUYEN_TAI_DU_LIEU_CUA_DOANH_NGHIEP)]
        public IActionResult BaoCaoDoanhNghiepTruyenTaiDuLieu([FromBody] DevExDataSourceLoadOptions<BaoCaoDoiTuongTruyenTaiParam> devExData)
        {
            return Ok(baoCaoService.BaoCaoDoiTuongTruyenTaiDuLieu(devExData, EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id, User.ThongTin()));
        }

        /// <summary>
        /// Báo cáo truyền tải dữ liệu của bến xe
        /// </summary>
        /// <param name="devExData"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<BaoCaoDoiTuongTruyenTaiDuLieuResponsive>>))]
        [HttpPost("bao-cao-truyen-tai-du-lieu-cua-ben-xe")]
        [Authorize(Roles = EPermission.DANH_SACH_BAO_CAO_TRUYEN_TAI_DU_LIEU_CUA_BEN_XE)]
        public IActionResult BaoCaoBenXeTruyenTaiDuLieu([FromBody] DevExDataSourceLoadOptions<BaoCaoDoiTuongTruyenTaiParam> devExData)
        {
            return Ok(baoCaoService.BaoCaoDoiTuongTruyenTaiDuLieu(devExData, EDoiTuongTruyenTai.BEN_XE.Id, User.ThongTin()));
        }

    }
}
