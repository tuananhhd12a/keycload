﻿using CDB_BaoCao.ViewModels.Input;
using CDB_BaoCao.ViewModels.Output;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.JWT;
using Lib_Core.Objects.Parameters;
using Lib_Core.Objects.Responsive;
using MongoDB.Driver;

namespace CDB_BaoCao.Services
{
    public partial class BaoCaoService
    {
        public DataResponsive<LoadResult> BaoCaoTruyenTaiDuLieu(DevExDataSourceLoadOptions<BaoCaoTruyenTaiDuLieuParam> devExData, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Báo cáo truyền tải dữ liệu");

                var custom = devExData.Custom;
                var loadOptions = devExData.loadOptions;
                loadOptions.StringToLower = true;
                if (custom == null)
                {
                    logger.LogError("Custom null");
                    return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.DU_LIEU_KHONG_HOP_LE, "Custom không được bỏ trống!");
                }
                if ((custom.DenNgay.Date - custom.TuNgay.Date).Days > Common.MaxDay)
                {
                    logger.LogError($"Ngày lọc không được quá {Common.MaxDay} ngày");
                    return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Ngày lọc không được quá {Common.MaxDay} ngày!");
                }
                logger.LogInformation($"Custom: {custom.Object2Json()}");

                var query = dbTruyenTai.AsQueryable()
                                        .Where(x => x.ThoiGianTruyenTai > custom.TuNgay && x.ThoiGianTruyenTai < custom.DenNgay);


                if (!string.IsNullOrWhiteSpace(custom.IdDoiTuongTruyenTai))
                {
                    query = query.Where(x => x.IdDoiTuongTruyenTai == custom.IdDoiTuongTruyenTai);
                }
                if (!string.IsNullOrWhiteSpace(custom.MaSoGTVT))
                {
                    query = query.Where(x => x.MaSoDonViTruyenTai == custom.MaSoGTVT);
                }

                var duLieu = query.Select(x => new BaoCaoDonViTruyenTaiResponsive
                {
                    IdTruyenTai = x._id,
                    TenSoGTVT = x.TenSoDonViTruyenTai,
                    TenLoaiTruyenTai = x.TenLoaiTruyenTai,
                    TenDonViTruyenNhan = x.TenDonViTruyenNhan,
                    TenDonViTruyenTai = x.TenDonViTruyenTai,
                    TenDoiTuongTuyenTai = x.TenDoiTuongTruyenTai,
                    MaLenhVanChuyen = x.MaLenhTiepNhan,
                    MaLoi = x.MaLoi == EMaLoi.THANH_CONG.Id ? null : x.MaLoi,
                    MoTaLoi = x.ThongTinLoi,
                    ThoiDiemTruyenTai = x.ThoiGianTruyenTai,
                    IdTrangThai = x.MaLoi == EMaLoi.THANH_CONG.Id ? ETrangThaiTruyenTai.THANH_CONG.Id : ETrangThaiTruyenTai.KHONG_THANH_CONG.Id,
                    TrangThai = x.MaLoi == EMaLoi.THANH_CONG.Id ? ETrangThaiTruyenTai.THANH_CONG.Name : ETrangThaiTruyenTai.KHONG_THANH_CONG.Name,
                    MaMauTruyenTai = x.MaLoi == EMaLoi.THANH_CONG.Id ? ETrangThaiTruyenTai.THANH_CONG.Description : ETrangThaiTruyenTai.KHONG_THANH_CONG.Description,
                });
                var data = DataSourceLoader.Load(duLieu, loadOptions);

                logger.LogInformation($"Báo cáo truyền tải dữ liệu thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Báo cáo truyền tải dữ liệu gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<LoadResult> BaoCaoDoiTuongTruyenTaiDuLieu(DevExDataSourceLoadOptions<BaoCaoDoiTuongTruyenTaiParam> devExData, string doiTuongTruyenTai, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Báo cáo đối tượng truyền tải dữ liệu - {doiTuongTruyenTai}");

                var custom = devExData.Custom;
                var loadOptions = devExData.loadOptions;
                loadOptions.StringToLower = true;
                if (custom == null)
                {
                    logger.LogError("Custom null");
                    return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.DU_LIEU_KHONG_HOP_LE, "Custom không được bỏ trống!");
                }
                if ((custom.DenNgay.Date - custom.TuNgay.Date).Days > Common.MaxDay)
                {
                    logger.LogError($"Ngày lọc không được quá {Common.MaxDay} ngày");
                    return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Ngày lọc không được quá {Common.MaxDay} ngày!");
                }
                logger.LogInformation($"Custom: {custom.Object2Json()}");

                var query = dbTruyenTai.AsQueryable()
                                       .Where(x => x.IdDoiTuongTruyenTai == doiTuongTruyenTai && x.ThoiGianTruyenTai > custom.TuNgay && x.ThoiGianTruyenTai < custom.DenNgay);


                if (!string.IsNullOrWhiteSpace(custom.MaSoGTVT))
                {
                    query = query.Where(x => x.MaSoDonViTruyenTai == custom.MaSoGTVT);
                }
                var baoCao = query.GroupBy(x => new { x.TenDonViTruyenTai, x.TenSoDonViTruyenTai })
                                  .Select(x => new
                                  {
                                      TenDonVi = x.Key.TenDonViTruyenTai,
                                      TenSoGTVT = x.Key.TenSoDonViTruyenTai,
                                      DanhSachXe = x.Select(x => x.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.BienKiemSoat).Distinct(),
                                      DanhSachTuyen = x.Select(x => x.NoiDungLenhVanChuyen.TuyenVanChuyen.MaSoTuyen).Distinct(),
                                      SoKhach_XuatBen = x.Sum(y => y.NoiDungBenDiXacNhan.SoKhachKhiKyLenh),
                                      SoKhach_DenBen = x.Sum(y => y.NoiDungBenDenXacNhan.SoKhachKhiKyLenh),
                                      SoLenh_XuatBen_CoDinh = x.Count(x => x.IdLoaiTruyenTai == ELoaiTruyenTai.XUAT_BEN.Id && (x.MaLoaiLenh == EMaLoaiLenh.TUYEN_CO_DINH.Id || x.MaLoaiLenh == EMaLoaiLenh.DI_THAY.Id)),
                                      SoLenh_XuatBen_TangCuong = x.Count(x => x.IdLoaiTruyenTai == ELoaiTruyenTai.XUAT_BEN.Id && (x.MaLoaiLenh == EMaLoaiLenh.TANG_CUONG.Id)),
                                      SoLenh_XeBuyt = x.Count(x => (x.MaLoaiLenh == EMaLoaiLenh.XE_BUYT.Id)),
                                      SoLenh_DenBen_CoDinh = x.Count(x => x.IdLoaiTruyenTai == ELoaiTruyenTai.DEN_BEN.Id && (x.MaLoaiLenh == EMaLoaiLenh.TUYEN_CO_DINH.Id || x.MaLoaiLenh == EMaLoaiLenh.DI_THAY.Id)),
                                      SoLenh_DenBen_TangCuong = x.Count(x => x.IdLoaiTruyenTai == ELoaiTruyenTai.DEN_BEN.Id && (x.MaLoaiLenh == EMaLoaiLenh.TANG_CUONG.Id)),
                                      SoLenh_TruyenTaiThanhCong = x.Count(x => x.MaLoi == EMaLoi.THANH_CONG.Id),
                                      SoLenh_TruyenTaiThatBai = x.Count(x => x.MaLoi != EMaLoi.THANH_CONG.Id),
                                      DanhSachNgayTruyenTai = x.Select(x => x.NgayTruyenTai).Distinct(),
                                      LanTruyenTaiCuoi = x.Max(x => x.ThoiGianTruyenTai),
                                  })
                                  .ToList()
                                  .Select(x => new BaoCaoDoiTuongTruyenTaiDuLieuResponsive()
                                  {
                                      TenDonVi = x.TenDonVi,
                                      TenSoGTVT = x.TenSoGTVT,
                                      SoLuongXe = x.DanhSachXe.Count(),
                                      SoLuongTuyen = x.DanhSachTuyen.Count(),
                                      SoKhach_XuatBen = x.SoKhach_XuatBen,
                                      SoKhach_DenBen = x.SoKhach_DenBen,
                                      SoLenh_XuatBen_CoDinh = x.SoLenh_XuatBen_CoDinh,
                                      SoLenh_XuatBen_TangCuong = x.SoLenh_XuatBen_TangCuong,
                                      SoLenh_XeBuyt = x.SoLenh_XeBuyt,
                                      SoLenh_DenBen_CoDinh = x.SoLenh_DenBen_CoDinh,
                                      SoLenh_DenBen_TangCuong = x.SoLenh_DenBen_TangCuong,
                                      SoLenh_TruyenTaiThanhCong = x.SoLenh_TruyenTaiThanhCong,
                                      SoLenh_TruyenTaiThatBai = x.SoLenh_TruyenTaiThatBai,
                                      SoNgay_DaTruyenTai = x.DanhSachNgayTruyenTai.Count(),
                                      LanTruyenTaiCuoi = x.LanTruyenTaiCuoi,
                                  });

                var data = DataSourceLoader.Load(baoCao, loadOptions);

                logger.LogInformation($"Báo cáo đối tượng truyền tải dữ liệu thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Báo cáo đối tượng truyền tải dữ liệu gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
