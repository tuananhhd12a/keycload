﻿using CDB_BaoCao.ViewModels.Input;
using CDB_BaoCao.ViewModels.Output;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lib_Core.Enum;
using Lib_Core.Objects.Parameters;
using Lib_Core.Objects.Responsive;
using MongoDB.Driver;

namespace CDB_BaoCao.Services
{
    public partial class BaoCaoService
    {
        public DataResponsive<List<DanhSachSoGTVTResponsive>> DanhSachSoGTVT()
        {
            try
            {
                logger.LogInformation($"Lấy danh sách sở GTVT");
                var danhSach = dbSoGTVT.Find(x => true)
                                       .Project(x => new DanhSachSoGTVTResponsive
                                       {
                                           MaSo = x.MaSoGTVT,
                                           TenSo = x.TenSoGTVT
                                       })
                                       .ToList()
                                       .OrderBy(x => x.TenSo)
                                       .ToList();
                logger.LogInformation($"Lấy danh sách sở GTVT thành công");
                return new DataResponsive<List<DanhSachSoGTVTResponsive>>().returnData(danhSach, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách sở GTVT gặp lỗi");
                return new DataResponsive<List<DanhSachSoGTVTResponsive>>().returnData(new List<DanhSachSoGTVTResponsive>(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<List<DanhSachDoiTuongTuyenTaiResponsive>> DanhSachDoiTuongTruyenTai()
        {
            var data = new List<DanhSachDoiTuongTuyenTaiResponsive>()
            {
                new DanhSachDoiTuongTuyenTaiResponsive()
                {
                    IdDoiTuong = EDoiTuongTruyenTai.BEN_XE.Id,
                    TenDoiTuong = EDoiTuongTruyenTai.BEN_XE.Name
                },
                new DanhSachDoiTuongTuyenTaiResponsive()
                {
                    IdDoiTuong = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id,
                    TenDoiTuong = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Name
                }
            };
            return new DataResponsive<List<DanhSachDoiTuongTuyenTaiResponsive>>().returnData(data, EMaLoi.THANH_CONG);
        }

        public DataResponsive<LoadResult> DanhSachDonViTruyenTai(DevExDataSourceLoadOptions<DanhSachDonViTruyenTaiParam> devExData)
        {
            try
            {
                logger.LogInformation($"Lấy danh sách đơn vị truyền tải");
                var custom = devExData.Custom;
                if (custom == null)
                {
                    logger.LogError("Custom null");
                    return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.DU_LIEU_KHONG_HOP_LE, "Custom không được bỏ trống!");
                }
                var loadOptions = devExData.loadOptions;
                loadOptions.StringToLower = true;

                var query = dbDonViHoatDong.AsQueryable().Where(x => x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id);
                if (!string.IsNullOrWhiteSpace(custom.MaSoGTVT))
                {
                    query = query.Where(x => x.MaSoGTVT == custom.MaSoGTVT);
                }
                if (!string.IsNullOrWhiteSpace(custom.IdDoiTuongTruyenTai))
                {
                    query = query.Where(x => x.DoiTuongTruyenTai.Id == custom.IdDoiTuongTruyenTai);
                }
                var donVis = query.Select(x => new DanhSachDonViTruyenTaiResponsive
                {
                    MaSoThue = x.MaSoThue,
                    TenDonVi = x.TenDonVi,
                    DoiTuongTruyenTai = x.DoiTuongTruyenTai.Id,
                    MaSoGTVT = x.MaSoGTVT,
                })
                .OrderBy(x => x.TenDonVi);

                var data = DataSourceLoader.Load(donVis, devExData.loadOptions);
                logger.LogInformation($"Lấy danh sách đơn vị truyền tải thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách đơn vị truyền tải gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
