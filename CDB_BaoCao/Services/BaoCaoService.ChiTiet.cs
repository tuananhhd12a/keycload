﻿using CDB_BaoCao.ViewModels.Output;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using MongoDB.Driver;

namespace CDB_BaoCao.Services
{
    public partial class BaoCaoService
    {
        public DataResponsive<ChiTietBaoCaoTruyenTaiResponsive> ChiTietBaoCaoTruyenTai(Guid idTruyenTai)
        {
            try
            {
                logger.LogInformation($"Chi tiết báo cáo truyền tải - {idTruyenTai}");
                var chiTiet = dbTruyenTai.Find(x => x._id == idTruyenTai)
                                         .Project(x => new ChiTietBaoCaoTruyenTaiResponsive()
                                         {
                                             IdTruyenTai = x._id,
                                             TenDonViTruyenNhan = x.TenDonViTruyenNhan,
                                             TenDonViTruyenTai = x.TenDonViTruyenTai,
                                             MaSoLenh = x.MaLenhTiepNhan,
                                             TenDoiTuongTruyenTai = x.TenDoiTuongTruyenTai,
                                             LoaiTruyenTai = x.TenLoaiTruyenTai,
                                             DuLieuNhanDuoc = x.DuLieuTiepNhan,
                                             FileXml = x.FileXml,
                                             TepDinhKems = x.DanhSachFileDinhKem,
                                             MaLoi = x.MaLoi,
                                             MoTaLoi = x.ThongTinLoi,
                                             ThoiDiemTruyenTai = x.ThoiGianTruyenTai,
                                             TrangThaiTruyenTai = x.MaLoi == EMaLoi.THANH_CONG.Id ? ETrangThaiTruyenTai.THANH_CONG.Name : ETrangThaiTruyenTai.KHONG_THANH_CONG.Name,
                                             MaMauTrangThaiTruyenTai = x.MaLoi == EMaLoi.THANH_CONG.Id ? ETrangThaiTruyenTai.THANH_CONG.Description : ETrangThaiTruyenTai.KHONG_THANH_CONG.Description,
                                         })
                                         .FirstOrDefault();
                logger.LogInformation($"Chi tiết báo cáo truyền tải thành công");
                return new DataResponsive<ChiTietBaoCaoTruyenTaiResponsive>().returnData(chiTiet, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Chi tiết báo cáo truyền tải gặp lỗi");
                return new DataResponsive<ChiTietBaoCaoTruyenTaiResponsive>().returnData(new ChiTietBaoCaoTruyenTaiResponsive(), EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
