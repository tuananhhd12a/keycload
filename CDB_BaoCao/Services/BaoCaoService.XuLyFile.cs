﻿using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.Mongo.LuuTru;
using Microsoft.Extensions.Caching.Distributed;
using Minio;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace CDB_BaoCao.Services
{
    public partial class BaoCaoService
    {
        public ConfigMinio LayCauHinhMinio()
        {
            logger.LogInformation($"Thực hiện lấy cấu hình minio");
            try
            {
                var key = $"CONFIG_MINIO_{ELoaiCauHinhLuuTru.TRUYEN_TAI.Id}";
                var _config = new ConfigMinio();
                var _cache = "";
                var redisError = false;
                var option = new DistributedCacheEntryOptions() { SlidingExpiration = Common.cacheTime };
                try
                {
                    _cache = distributedCache.GetString(key);
                }
                catch (Exception e)
                {
                    redisError = true;
                    logger.LogError(e, $"Lấy dữ liệu minio từ cache gặp lỗi");
                }
                if (string.IsNullOrWhiteSpace(_cache))
                {
                    logger.LogInformation($"Cache: {_cache}");

                    var config = dbMinio.Find(x => x.LoaiLuuTru == ELoaiCauHinhLuuTru.TRUYEN_TAI.Id).FirstOrDefault();
                    if (config == null)
                    {
                        logger.LogError($"Không tìm thấy cấu hình minio");
                        return null;
                    }
                    _config.EndPoint = config.EndPoint;
                    _config.Bucket = config.Bucket;
                    _config.AccessKey = config.AccessKey;
                    _config.SecretKey = config.SecretKey;
                    try
                    {
                        if (!redisError)
                        {
                            distributedCache.SetString(key, _config.Object2Json(), option);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogError(e, $"Lưu dữ liệu minio vào cache gặp lỗi");
                    }
                }
                else
                {
                    _config = JsonConvert.DeserializeObject<ConfigMinio>(_cache);
                }
                return _config;
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lỗi khi lấy cấu hình minio");
                return null;
            }
        }

        public byte[] LayFile(string objectName, out string contentType, out string fileName)
        {
            contentType = "";
            fileName = Guid.NewGuid().ToString();
            try
            {
                if (string.IsNullOrWhiteSpace(objectName))
                {
                    return null;
                }
                using (MemoryStream memoryStream = new())
                {
                    var configMinio = LayCauHinhMinio();
                    if (configMinio == null)
                    {
                        logger.LogWarning($"Chưa cấu hình minio");
                        return null;
                    }
                    var minioClient = new MinioClient()
                                        .WithEndpoint(configMinio.EndPoint)
                                        .WithCredentials(accessKey: configMinio.AccessKey, secretKey: configMinio.SecretKey); ;

                    GetObjectArgs getObjectArgs = new GetObjectArgs()
                                                    .WithBucket(configMinio.Bucket)
                                                    .WithObject(objectName)
                                                    .WithCallbackStream((stream) => stream.CopyTo(memoryStream));
                    
                    var info = minioClient.GetObjectAsync(getObjectArgs).GetAwaiter().GetResult();

                    contentType = info.ContentType;
                    if (contentType.Contains("pdf"))
                    {
                        fileName += ".pdf";
                    }
                    else if (contentType.Contains("xml"))
                    {
                        fileName += ".xml";
                    }
                    else if (contentType.Contains("image"))
                    {
                        fileName += ".jpg";
                    }
                    return memoryStream.ToArray();
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy file gặp lỗi");
                return null;
            }
        }
    }
}
