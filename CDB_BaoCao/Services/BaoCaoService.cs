﻿using AutoMapper;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.Mongo.LuuTru;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTin_TruyenTaiTrenDuong;
using Lib_Core.Objects.Mongo.TruyenTai;
using Microsoft.Extensions.Caching.Distributed;
using MongoDB.Driver;
using Lib_Core.Objects.Mongo;
using Lib_Core.Objects.Mongo.HanhChinh;

namespace CDB_BaoCao.Services
{
    public partial class BaoCaoService
    {
        private readonly ILogger<BaoCaoService> logger;
        private readonly IMapper mapper;
        private readonly IDistributedCache distributedCache;
        private readonly IMongoCollection<CauHinh_DonViTruyenNhan> dbDonViTruyenNhan;
        private readonly IMongoCollection<CauHinh_DonViHoatDong> dbDonViHoatDong;
        private readonly IMongoCollection<CauHinh_NhanVienKyLenh> dbNhanVien;
        private readonly IMongoCollection<ConfigMinio> dbMinio;
        private readonly IMongoCollection<CDB_LenhVanChuyen> dbTruyenTai;
        private readonly IMongoCollection<CDB_TruyenTaiTrenDuong> dbTruyenTaiTrenDuong;
        private readonly IMongoCollection<ThongTin_SoGiaoThongVanTai> dbSoGTVT;

        public BaoCaoService(ILogger<BaoCaoService> logger, IMapper mapper, IDistributedCache distributedCache, IDatabaseMongoSettings settings)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.distributedCache = distributedCache;

            //MongoDb
            var client = new MongoClient($"{settings.ConnectionString}");
            this.dbDonViTruyenNhan = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_DonViTruyenNhan>(nameof(CauHinh_DonViTruyenNhan));
            this.dbDonViHoatDong = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_DonViHoatDong>(nameof(CauHinh_DonViHoatDong));
            this.dbNhanVien = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_NhanVienKyLenh>(nameof(CauHinh_NhanVienKyLenh));
            this.dbTruyenTai = client.GetDatabase(settings.DatabaseName).GetCollection<CDB_LenhVanChuyen>(nameof(CDB_LenhVanChuyen));
            this.dbTruyenTaiTrenDuong = client.GetDatabase(settings.DatabaseName).GetCollection<CDB_TruyenTaiTrenDuong>(nameof(CDB_TruyenTaiTrenDuong));
            this.dbMinio = client.GetDatabase(settings.DatabaseName).GetCollection<ConfigMinio>(nameof(ConfigMinio));
            this.dbSoGTVT = client.GetDatabase(settings.DatabaseName).GetCollection<ThongTin_SoGiaoThongVanTai>(nameof(ThongTin_SoGiaoThongVanTai));

            //Create Index
            this.dbTruyenTai.Indexes.CreateOneAsync(
               Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.ThoiGianTruyenTai)
               , new CreateIndexOptions() { Background = true }
                );

            this.dbTruyenTai.Indexes.CreateOneAsync(
              Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.IdDoiTuongTruyenTai)
              , new CreateIndexOptions() { Background = true }
               );

            this.dbTruyenTai.Indexes.CreateOneAsync(
              Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.IdDoiTuongTruyenTai)
                                                   .Ascending(x => x.MaSoDonViTruyenTai)
              , new CreateIndexOptions() { Background = true }
               );

            this.dbTruyenTai.Indexes.CreateOneAsync(
               Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.ThoiGianTruyenTai)
                                                    .Ascending(x => x.IdDoiTuongTruyenTai)
                                                    .Ascending(x => x.MaSoDonViTruyenTai)
               , new CreateIndexOptions() { Background = true }
                );
        }
    }
}
