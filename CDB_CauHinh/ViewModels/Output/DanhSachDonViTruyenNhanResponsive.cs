﻿using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachDonViTruyenNhanResponsive
    {
        public Guid IdDonViTruyenNhan { get; set; }
        public string MaSoThue { get; set; }
        public string TenDonVi { get; set; }
        public string DiaChi { get; set; }
        public string NguoiDaiDien { get; set; }
        public string SDT_NguoiDaiDien { get; set; }
        public string Email { get; set; }
        public string IdTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string MaMauTrangThai { get; set; }
    }
}
