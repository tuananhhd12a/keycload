﻿using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class ChiTietNhanVienKyLenhResponsive
    {
        public Guid IdNhanVien { get; set; }
        public string LoaiChuKy { get; set; }
        public string HoTen { get; set; }
        public string MaDinhDanh { get; set; }
        public string GhiChu { get; set; }
        public string Serial { get; set; }
        public string PublicKey { get; set; }
        public string TepDinhKem { get; set; }
        public string IdTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string MaMauTrangThai { get; set; }
    }
}
