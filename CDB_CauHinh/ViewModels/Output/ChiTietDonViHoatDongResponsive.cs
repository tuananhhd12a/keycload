﻿using Lib_Core.Enum;
using System.Collections.Generic;
using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class ChiTietDonViHoatDongResponsive
    {
        public Guid IdDonVi { get; set; }
        public string TenDonViTruyenNhan { get; set; }
        public string MaSoThue { get; set; }
        public string MaBenXe { get; set; }
        public string TenDonVi { get; set; }
        public string Email { get; set; }
        public string MaSoGTVT { get; set; }
        public string DiaChi { get; set; }
        public string PublicKey { get; set; }
        public List<string> DanhSachFile { get; set; }
        public string IdDoiTuongTruyenTai { get; set; }
        public string TenDoiTuongTruyenTai { get; set; }
        public string IdTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string MaMauTrangThai { get; set; }
    }
}
