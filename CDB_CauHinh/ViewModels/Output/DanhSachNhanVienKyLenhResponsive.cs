﻿using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachNhanVienKyLenhResponsive
    {
        public Guid IdNhanVien { get; set; }
        public string HoTen { get; set; }
        public string MaDinhDanh { get; set; }
        public string GhiChu { get; set; }
        public string IdTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string MaMauTrangThai { get; set; }
    }
}
