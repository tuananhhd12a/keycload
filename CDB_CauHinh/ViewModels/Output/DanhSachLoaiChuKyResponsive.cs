﻿namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachLoaiChuKyResponsive
    {
        public string IdLoaiChuKy { get; set; }
        public string TenLoaiChuKy { get; set; }
    }
}
