﻿using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachCauHinhLuuTruResponsive
    {
        public Guid IdCauHinh { get; set; }

        public string EndPoint { get; set; }

        public string Bucket { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string LoaiLuuTru { get; set; }
    }
}
