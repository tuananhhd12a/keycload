﻿using Lib_Core.Enum;
using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class ChiTietDonViTruyenNhanResponsive
    {
        public Guid IdDonViTruyenNhan { get; set; }
        public string TenDonVi { get; set; }
        public string MaSoThue { get; set; }
        public string DiaChi { get; set; }
        public string NguoiDaiDien { get; set; }
        public string SDT_NguoiDaiDien { get; set; }
        public string Email { get; set; }
        public string NguoiPhuTrachKyThuat { get; set; }
        public string SDT_PhuTrachKyThuat { get; set; }
        public string IP_MayChu { get; set; }
        public string PublicKey { get; set; }
        public DateTime ThoiGianBatDauHieuLuc { get; set; }
        public DateTime ThoiGianKetThucHieuLuc { get; set; }
        public ETrangThaiCauHinh TrangThai { get; set; }
        public string GhiChu { get; set; }
        public bool IsDelete { get; set; }
    }
}
