﻿namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachDoiTuongTuyenTaiResponsive
    {
        public string IdDoiTuong { get; set; }
        public string TenDoiTuong { get; set; }
    }
}
