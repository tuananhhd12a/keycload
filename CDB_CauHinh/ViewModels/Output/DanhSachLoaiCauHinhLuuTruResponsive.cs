﻿namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachLoaiCauHinhLuuTruResponsive
    {
        public string IdLoaiCauHinh { get; set; }
        public string TenLoaiCauHinh { get; set; }
    }
}
