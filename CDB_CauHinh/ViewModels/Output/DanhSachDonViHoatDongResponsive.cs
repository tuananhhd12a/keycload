﻿using Lib_Core.Enum;
using Lib_Core.Objects.Mongo.CauHinh;
using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class DanhSachDonViHoatDongResponsive
    {
        public Guid IdDonVi { get; set; }
        public string MaSoThue { get; set; }
        public string TenDonVi { get; set; }
        public string Email { get; set; }
        public string LoaiDonVi { get; set; }
        public string MaSoGTVT { get; set; }
        public string IdTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string MaMauTrangThai { get; set; }
        public string TenDonViTruyenNhan { get; set; }
    }
}
