﻿using System;

namespace CDB_CauHinh.ViewModels.Output
{
    public class HuyKichHoatNhanVienKyLenhParam
    {
        public Guid IdNhanVien { get; set; }
    }
}
