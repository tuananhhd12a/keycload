﻿using System;

namespace CDB_CauHinh.ViewModels.Input
{
    public class TuChoiDonViHoatDongParam
    {
        public Guid IdDonVi { get; set; }
    }
}
