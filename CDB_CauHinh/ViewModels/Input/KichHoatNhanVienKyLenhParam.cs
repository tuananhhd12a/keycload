﻿using System;

namespace CDB_CauHinh.ViewModels.Input
{
    public class KichHoatNhanVienKyLenhParam
    {
        public Guid IdNhanVien { get; set; }
    }
}
