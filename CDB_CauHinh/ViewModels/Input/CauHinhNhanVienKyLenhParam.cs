﻿using Lib_Core.Attributes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CDB_CauHinh.ViewModels.Input
{
    public class CauHinhNhanVienKyLenhParam
    {

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Họ tên")]
        public string HoTen { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Loại chữ ký")]
        public string LoaiChuKy { get; set; }

        public string Serial { get; set; }

        public string PublicKey { get; set; }

        public IFormFile FileDinhKem { get; set; }

        public string GhiChu { get; set; }
    }
}
