﻿using System;

namespace CDB_CauHinh.ViewModels.Input
{
    public class TuChoiDonViTruyenNhanParam
    {
        public Guid IdDonViTruyenNhan { get; set; }
    }
}
