﻿using Lib_Core.Attributes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CDB_CauHinh.ViewModels.Input
{
    public class CauHinhDonViHoatDongParam
    {
        [GuidValidation(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("IdDonViTruyenNhan")]
        public Guid IdDonViTruyenNhan { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã số thuế")]
        public string MaSoThue { get; set; }

        [DisplayName("Mã bến xe")]
        public string MaBenXe { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Tên đơn vị")]
        public string TenDonVi { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Loại đơn vị")]
        public string LoaiDonVi { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã sở GTVT")]
        public string MaSoGTVT { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Địa chỉ")]
        public string DiaChi { get; set; }

        [DisplayName("PublicKey")]
        public string PublicKey { get; set; }

        [DisplayName("DanhSachFile")]
        public List<IFormFile> DanhSachFile { get; set; }
    }
}
