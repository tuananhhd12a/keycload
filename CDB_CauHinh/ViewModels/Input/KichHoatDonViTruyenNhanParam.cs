﻿using System;

namespace CDB_CauHinh.ViewModels.Input
{
    public class KichHoatDonViTruyenNhanParam
    {
        public Guid IdDonViTruyenNhan { get; set; }
    }
}
