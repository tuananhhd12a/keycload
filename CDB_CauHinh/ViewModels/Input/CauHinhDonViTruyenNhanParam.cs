﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CDB_CauHinh.ViewModels.Input
{
    public class CauHinhDonViTruyenNhanParam
    {
        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Tên đơn vị")]
        public string TenDonVi { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã số thuế")]
        public string MaSoThue { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Địa chỉ")]
        public string DiaChi { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Người đại diện")]
        public string NguoiDaiDien { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Số điện thoại người đại diện")]
        public string SDTNguoiDaiDien { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Người phụ trách kỹ thuật")]
        public string NguoiPhuTrachKyThuat { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Số điện thoại người phụ trách kỹ thuật")]
        public string SDTNguoiPhuTrachKyThuat { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Ip máy chủ")]
        public string IPMayChu { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("PublicKey")]
        public string PublicKey { get; set; }
    }
}
