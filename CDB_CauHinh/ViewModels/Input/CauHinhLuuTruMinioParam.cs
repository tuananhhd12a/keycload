﻿using System;
using System.ComponentModel;

namespace CDB_CauHinh.ViewModels.Input
{
    public class CauHinhLuuTruMinioParam
    {
        [DisplayName("IdCauHinh")]
        public Guid? IdCauHinh { get; set; }

        [DisplayName("EndPoint")]
        public string EndPoint { get; set; }

        [DisplayName("Bucket")]
        public string Bucket { get; set; }

        [DisplayName("AccessKey")]
        public string AccessKey { get; set; }

        [DisplayName("SecretKey")]
        public string SecretKey { get; set; }

        [DisplayName("LoaiLuuTru")]
        public string LoaiLuuTru { get; set; }
    }
}
