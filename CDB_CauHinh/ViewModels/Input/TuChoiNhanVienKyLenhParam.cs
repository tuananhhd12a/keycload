﻿using System;

namespace CDB_CauHinh.ViewModels.Input
{
    public class TuChoiNhanVienKyLenhParam
    {
        public Guid IdNhanVien { get; set; }
    }
}
