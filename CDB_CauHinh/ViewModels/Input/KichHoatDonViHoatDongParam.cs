﻿using System;

namespace CDB_CauHinh.ViewModels.Input
{
    public class KichHoatDonViHoatDongParam
    {
        public Guid IdDonVi { get; set; }
    }
}
