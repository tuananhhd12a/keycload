﻿using AutoMapper;
using CDB_CauHinh.Services;
using Lib_Core.Attributes;
using Lib_Core.Objects.Mongo;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

var cultureInfo = new CultureInfo("en-US");
var cultureInfoVn = new CultureInfo("vi-VN");
cultureInfo.NumberFormat = cultureInfoVn.NumberFormat;
CultureInfo.DefaultThreadCurrentCulture = cultureInfo;

AddConfiguration();
AddController(builder.Services);
AddSwagger(builder.Services);
AddMongo();
SetupJwt(builder.Services);
AddRedis(builder.Services);


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCors(s => s.AddDefaultPolicy(c => c.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));
builder.Logging.ClearProviders();
var logger = new LoggerConfiguration()
            .ReadFrom.Configuration(builder.Configuration, sectionName: "Serilog")
            .WriteTo.Console()
            .CreateLogger();
builder.Logging.AddSerilog(logger);
builder.WebHost.UseSerilog(logger);

AddService();

AddLog(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseSerilogRequestLogging();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();

void AddController(IServiceCollection services)
{
    services
        .AddControllers()
            .ConfigureApiBehaviorOptions(options =>
            {
                //options.SuppressModelStateInvalidFilter = true;
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var modelState = actionContext.ModelState;
                    var resError = new List<ErrorValidation>();
                    foreach (var keyModelStatePair in modelState)
                    {
                        var key = keyModelStatePair.Key;
                        var errors = keyModelStatePair.Value.Errors;
                        if (errors != null && errors.Count > 0)
                        {
                            if (errors.Count == 1)
                            {
                                var errorMessage =
                                    string.IsNullOrEmpty(errors[0].ErrorMessage)
                                        ? "The input was not valid."
                                        : errors[0].ErrorMessage;
                                resError.Add(new ErrorValidation(key, errorMessage));
                            }
                            else
                            {
                                var errorMessages = "";
                                for (var i = 0; i < errors.Count; i++)
                                {
                                    if (!string.IsNullOrWhiteSpace(errorMessages))
                                    {
                                        errorMessages += ", ";
                                    }

                                    errorMessages += string.IsNullOrEmpty(errors[i].ErrorMessage)
                                        ? "The input was not valid."
                                        : errors[i].ErrorMessage;
                                }

                                resError.Add(new ErrorValidation(key, errorMessages));

                            }
                        }
                    }

                    return new BadRequestObjectResult(
                        new DataResponsive<List<ErrorValidation>>
                        {
                            message = "Bad Request",
                            data = resError,
                            errorCode = 400,
                            status = false
                        });
                };
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.BinaryConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.DataSetConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.DataTableConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.DiscriminatedUnionConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.EntityKeyMemberConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.ExpandoObjectConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.JavaScriptDateTimeConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.KeyValuePairConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.RegexConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.UnixDateTimeConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.VersionConverter());
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.XmlNodeConverter());

            });

    int maxSize = 10485760;

    services.Configure<FormOptions>(options =>
    {
        options.MultipartBoundaryLengthLimit = maxSize;
        options.BufferBodyLengthLimit = maxSize;
        options.ValueLengthLimit = maxSize;
        options.MultipartBodyLengthLimit = maxSize;
    });

    services.Configure<KestrelServerOptions>(opt =>
    {
        opt.Limits.MaxRequestBodySize = maxSize;
        opt.Limits.MaxRequestBufferSize = maxSize;
    });
    services.AddMvc();
}

void AddSwagger(IServiceCollection services)
{
    services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "CAU HINH TRUYEN TAI API"
           ,
            Version = "v1"
           ,
            Description = ""
           ,
            TermsOfService = new Uri("https://sonphatgroup.vn/")
           ,
            Contact = new OpenApiContact
            {
                Name = "Sơn Phát C&T",
                Email = "sonphattn@gmail.com"
            }
        });

        c.DocInclusionPredicate((docName, apiDesc) =>
        {
            if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;
            // Exclude all DevExpress reporting controllers
            return !methodInfo.DeclaringType.AssemblyQualifiedName.StartsWith("DevExpress", StringComparison.OrdinalIgnoreCase);
        });

        var authority = builder.Configuration.GetSection("JwtBearerOptions:Authority").Value;
        c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
            Type = SecuritySchemeType.OAuth2,
            Flows = new OpenApiOAuthFlows()
            {
                Password = new OpenApiOAuthFlow()
                {
                    TokenUrl = new Uri($"{authority}/protocol/openid-connect/token"),
                    RefreshUrl = new Uri($"{authority}/protocol/openid-connect/token")
                }
            }
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="oauth2"
                }
            },
            new string[]{}
            }
        });

        //Chuyển mô tả(comment) sang swagger
        var filePath = Path.Combine(AppContext.BaseDirectory, "CommentXML.xml");
        c.IncludeXmlComments(filePath);
        c.OrderActionsBy(
            apiDesc => $"{apiDesc.ActionDescriptor.RouteValues["controller"]}_{apiDesc.HttpMethod}");
    });
}

void AddLog(IServiceCollection services)
{
    services.AddHttpContextAccessor();
    services.AddLogging();
}

void AddConfiguration()
{
    builder.Configuration.SetBasePath(Directory.GetCurrentDirectory());
    builder.Configuration.AddJsonFile("appsettings.json", true, true);
    builder.Configuration.AddEnvironmentVariables(prefix: "AppSonPhat_");
}

void AddService()
{
    builder.Services.AddScoped<CauHinhService>();

    var mapperConfig = new MapperConfiguration(mc =>
    {
        mc.AddProfile(new MapperProfile());
    });

    IMapper mapper = mapperConfig.CreateMapper();
    builder.Services.AddSingleton(mapper);
}

void AddMongo()
{
    builder.Services.Configure<DatabaseMongoSettings>(
               builder.Configuration.GetSection(nameof(DatabaseMongoSettings)));

    builder.Services.AddSingleton<IDatabaseMongoSettings>(sp =>
        sp.GetRequiredService<IOptions<DatabaseMongoSettings>>().Value);
}

void SetupJwt(IServiceCollection services)
{
    var configuration = builder.Configuration;
    services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            configuration.Bind("JwtBearerOptions", options);
            options.Validate();
        });
}

void AddRedis(IServiceCollection services)
{
    var configuration = builder.Configuration;
    string redis = configuration.GetConnectionString("Redis");
    services.AddStackExchangeRedisCache(options =>
    {
        options.Configuration = redis;
    });
}
