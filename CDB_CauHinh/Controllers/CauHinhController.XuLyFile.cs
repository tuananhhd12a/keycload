﻿using Lib_Core.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CDB_CauHinh.Controllers
{
    public partial class CauHinhController
    {
        /// <summary>
        /// Hàm lấy file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("lay-file")]
        [Authorize(Roles = EPermission.CAU_HINH_NHAN_VIEN_KY_LENH)]
        public IActionResult LayFile([FromQuery] string path)
        {
            try
            {
                var file = cauHinhService.LayFile(path, out string contentType);
                if (file == null)
                {
                    return NotFound();
                }
                return File(file, contentType);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
