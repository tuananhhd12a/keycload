﻿using CDB_CauHinh.Services;
using CDB_CauHinh.ViewModels.Input;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CDB_CauHinh.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class CauHinhController : ControllerBase
    {
        private readonly CauHinhService cauHinhService;

        public CauHinhController(CauHinhService cauHinhService)
        {
            this.cauHinhService = cauHinhService;
        }
    }
}
