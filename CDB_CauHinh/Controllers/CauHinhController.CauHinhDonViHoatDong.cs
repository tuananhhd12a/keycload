﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CDB_CauHinh.Controllers
{
    public partial class CauHinhController
    {
        /// <summary>
        /// Danh sách loại đối tượng hoạt động
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<List<DanhSachDoiTuongTuyenTaiResponsive>>))]
        [HttpPost("danh-sach-doi-tuong-hoat-dong")]
        [Authorize(Roles = EPermission.CAU_HINH_DON_VI_HOAT_DONG)]
        public IActionResult DanhSachDoiTuongHoatDong()
        {
            return Ok(cauHinhService.DanhSachDoiTuongHoatDong());
        }

        /// <summary>
        /// Danh sách đơn vị hoạt động
        /// </summary>
        /// <param name="loadOptionsBase"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<DanhSachDonViHoatDongResponsive>>))]
        [HttpPost("danh-sach-cau-hinh-don-vi-hoat-dong")]
        [Authorize(Roles = EPermission.DANH_SACH_CAU_HINH_DON_VI_HOAT_DONG)]
        public IActionResult DanhSachDonViHoatDong(DataSourceLoadOptionsBase loadOptionsBase)
        {
            return Ok(cauHinhService.DanhSachCauHinhDonViHoatDong(loadOptionsBase));
        }

        /// <summary>
        /// Cập nhật cấu hình đơn vị hoạt động
        /// </summary>
        /// <param name="cauHinhDonViHoatDong"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("cap-nhat-cau-hinh-don-vi-hoat-dong")]
        [Authorize(Roles = EPermission.CAU_HINH_DON_VI_HOAT_DONG)]
        public IActionResult CauHinhDonViHoatDong([FromForm] CauHinhDonViHoatDongParam cauHinhDonViHoatDong)
        {
            return Ok(cauHinhService.CauHinhDonViHoatDong(cauHinhDonViHoatDong, User.ThongTin()));
        }

        /// <summary>
        /// Chi tiết cấu hình đơn vị hoạt động
        /// </summary>
        /// <param name="idDonVi"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<ChiTietDonViHoatDongResponsive>))]
        [HttpGet("chi-tiet-cau-hinh-don-vi-hoat-dong")]
        [Authorize(Roles = EPermission.CHI_TIET_CAU_HINH_DON_VI_HOAT_DONG)]
        public IActionResult ChiTietDonViHoatDong([FromQuery] Guid idDonVi)
        {
            return Ok(cauHinhService.ChiTietDonViHoatDong(idDonVi));
        }

        /// <summary>
        /// Kích hoạt đơn vị hoạt động
        /// </summary>
        /// <param name="kichHoatDonViHoatDong"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("kich-hoat-don-vi-hoat-dong")]
        [Authorize(Roles = EPermission.KICH_HOAT_CAU_HINH_DON_VI_HOAT_DONG)]
        public IActionResult KichHoatDonViHoatDong([FromBody] KichHoatDonViHoatDongParam kichHoatDonViHoatDong)
        {
            return Ok(cauHinhService.KichHoatDonViHoatDong(kichHoatDonViHoatDong, User.ThongTin()));
        }

        /// <summary>
        /// Từ chối kích hoạt đơn vị hoạt động
        /// </summary>
        /// <param name="huyKichHoatDonViHoatDong"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("tu-choi-kich-hoat-don-vi-hoat-dong")]
        [Authorize(Roles = EPermission.TU_CHOI_KICH_HOAT_CAU_HINH_DON_VI_HOAT_DONG)]
        public IActionResult TuChoiKichHoatDonViHoatDong([FromBody] TuChoiDonViHoatDongParam huyKichHoatDonViHoatDong)
        {
            return Ok(cauHinhService.TuChoiKichHoatDonViHoatDong(huyKichHoatDonViHoatDong, User.ThongTin()));
        }
    }
}
