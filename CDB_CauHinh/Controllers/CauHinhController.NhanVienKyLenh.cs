﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CDB_CauHinh.Controllers
{
    public partial class CauHinhController
    {
        /// <summary>
        /// Danh sách loại cữ ký
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<List<DanhSachLoaiChuKyResponsive>>))]
        [HttpPost("danh-sach-loai-chu-ky")]
        [Authorize(Roles = EPermission.CAU_HINH_NHAN_VIEN_KY_LENH)]
        public IActionResult DanhSachLoaiChuKy()
        {
            return Ok(cauHinhService.DanhSachLoaiChuKy());
        }

        /// <summary>
        /// Danh sách nhân viên ký lệnh
        /// </summary>
        /// <param name="loadOptionsBase"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<DanhSachNhanVienKyLenhResponsive>>))]
        [HttpPost("danh-sach-nhan-vien-ky-lenh")]
        [Authorize(Roles = EPermission.DANH_SACH_NHAN_VIEN_KY_LENH)]
        public IActionResult DanhSachNhanVienKyLenh([FromBody] DataSourceLoadOptionsBase loadOptionsBase)
        {
            return Ok(cauHinhService.DanhSachNhanVienKyLenh(loadOptionsBase, User.ThongTin()));
        }


        /// <summary>
        /// Cập nhật cấu hình nhân viên ký lệnh
        /// </summary>
        /// <param name="cauHinhNhanVienKyLenh"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<string>))]
        [HttpPost("cap-nhat-cau-hinh-nhan-vien-ky-lenh")]
        [Authorize(Roles = EPermission.CAU_HINH_NHAN_VIEN_KY_LENH)]
        public IActionResult CauHinhNhanVienKyLenh([FromForm] CauHinhNhanVienKyLenhParam cauHinhNhanVienKyLenh)
        {
            return Ok(cauHinhService.CauHinhNhanVienKyLenh(cauHinhNhanVienKyLenh, User.ThongTin()));
        }

        /// <summary>
        /// Chi tiết nhân viên ký lệnh
        /// </summary>
        /// <param name="idNhanVien"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<ChiTietDonViHoatDongResponsive>))]
        [HttpPost("chi-tiet-nhan-vien-ky-lenh")]
        [Authorize(Roles = EPermission.CHI_TIET_NHAN_VIEN_KY_LENH)]
        public IActionResult ChiTietNhanVienKyLenh([FromQuery] Guid idNhanVien)
        {
            return Ok(cauHinhService.ChiTietNhanVienKyLenh(idNhanVien));
        }

        /// <summary>
        /// Kích hoạt nhân viên ký lệnh
        /// </summary>
        /// <param name="kichHoatNhanVienKyLenh"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("kich-hoat-nhan-vien-ky-lenh")]
        [Authorize(Roles = EPermission.KICH_HOAT_NHAN_VIEN_KY_LENH)]
        public IActionResult KichHoatNhanVienKyLenh([FromBody] KichHoatNhanVienKyLenhParam kichHoatNhanVienKyLenh)
        {
            return Ok(cauHinhService.KichHoatNhanVienKyLenh(kichHoatNhanVienKyLenh, User.ThongTin()));
        }

        /// <summary>
        /// Từ chối kích hoạt nhân viên ký lệnh
        /// </summary>
        /// <param name="tuChoiNhanVienKyLenh"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("tu-choi-kich-hoat-nhan-vien-ky-lenh")]
        [Authorize(Roles = EPermission.TU_CHOI_NHAN_VIEN_KY_LENH)]
        public IActionResult TuChoiKichHoatNhanVienKyLenh([FromBody] TuChoiNhanVienKyLenhParam tuChoiNhanVienKyLenh)
        {
            return Ok(cauHinhService.TuChoiKichHoatNhanVienKyLenh(tuChoiNhanVienKyLenh, User.ThongTin()));
        }

        /// <summary>
        /// Hủy kích hoạt nhân viên ký lệnh
        /// </summary>
        /// <param name="huyKichHoatNhanVienKyLenh"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("huy-kich-hoat-nhan-vien-ky-lenh")]
        [Authorize(Roles = EPermission.HUY_NHAN_VIEN_KY_LENH)]
        public IActionResult HuyKichHoatNhanVienKyLenh([FromBody] HuyKichHoatNhanVienKyLenhParam huyKichHoatNhanVienKyLenh)
        {
            return Ok(cauHinhService.HuyKichHoatNhanVienKyLenh(huyKichHoatNhanVienKyLenh, User.ThongTin()));
        }

    }
}
