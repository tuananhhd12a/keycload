﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data;
using Lib_Core.Enum;
using Lib_Core.Objects.Mongo.LuuTru;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CDB_CauHinh.Controllers
{
    public partial class CauHinhController
    {
        /// <summary>
        /// Danh sách loại cấu hình lưu trữ
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<List<DanhSachLoaiCauHinhLuuTruResponsive>>))]
        [HttpGet("danh-sach-loai-cau-hinh-luu-tru")]
        [Authorize(Roles = EPermission.CAU_HINH_LUU_TRU_FILE)]
        public IActionResult DanhSachLoaiCauHinhLuuTru()
        {
            return Ok(cauHinhService.DanhSachLoaiCauHinhLuuTru());
        }

        /// <summary>
        /// Danh sách cấu hình lưu trữ
        /// </summary>
        /// <param name="loadOptionsBase"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<DanhSachCauHinhLuuTruResponsive>>))]
        [HttpPost("danh-sach-cau-hinh-luu-tru")]
        [Authorize(Roles = EPermission.CAU_HINH_LUU_TRU_FILE)]
        public IActionResult DanhSachCauHinhLuuTru([FromBody] DataSourceLoadOptionsBase loadOptionsBase)
        {
            return Ok(cauHinhService.DanhSachCauHinhLuuTru(loadOptionsBase));
        }

        /// <summary>
        /// Cấu hình lưu trữ
        /// </summary>
        /// <param name="configMinio"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("cau-hinh-luu-tru-minio")]
        [Authorize(Roles = EPermission.CAU_HINH_LUU_TRU_FILE)]
        public IActionResult CauHinhLuuTru([FromBody] CauHinhLuuTruMinioParam configMinio)
        {
            return Ok(cauHinhService.CauHinhMinio(configMinio));
        }
    }
}
