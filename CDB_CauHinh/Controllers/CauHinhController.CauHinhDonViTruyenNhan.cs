﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CDB_CauHinh.Controllers
{
    public partial class CauHinhController
    {
        /// <summary>
        /// Danh sách cấu hình đơn vị truyền nhận
        /// </summary>
        /// <param name="loadOptions"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<DanhSachDonViTruyenNhanResponsive>>))]
        [HttpPost("danh-sach-cau-hinh-don-vi-truyen-nhan")]
        [Authorize(Roles = EPermission.DANH_SACH_CAU_HINH_DON_VI_TRUYEN_NHAN)]
        public IActionResult DanhSachDonViTruyenNhan([FromBody] DataSourceLoadOptionsBase loadOptions)
        {
            return Ok(cauHinhService.DanhSachDonViTruyenNhan(loadOptions));
        }

        /// <summary>
        /// Danh sách cấu hình đơn vị truyền nhận đang hoạt động
        /// </summary>
        /// <param name="loadOptions"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<CustomLoadResult<DanhSachDonViTruyenNhanResponsive>>))]
        [HttpPost("danh-sach-cau-hinh-don-vi-truyen-nhan-dang-hoat-dong")]
        [Authorize(Roles = EPermission.DANH_SACH_CAU_HINH_DON_VI_TRUYEN_NHAN)]
        public IActionResult DanhSachDonViTruyenNhanDangHoatDong([FromBody] DataSourceLoadOptionsBase loadOptions)
        {
            return Ok(cauHinhService.DanhSachDonViTruyenNhanDangHoatDong(loadOptions));
        }

        /// <summary>
        /// Chi tiết cấu hình đơn vị truyền nhận
        /// </summary>
        /// <param name="idDonViTruyenNhan"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<ChiTietDonViTruyenNhanResponsive>))]
        [HttpGet("chi-tiet-cau-hinh-don-vi-truyen-nhan")]
        [Authorize(Roles = EPermission.CHI_TIET_CAU_HINH_DON_VI_TRUYEN_NHAN)]
        public IActionResult ChiTietCauHinhDonViTruyenNhan([FromQuery] Guid idDonViTruyenNhan)
        {
            return Ok(cauHinhService.ChiTietCauHinhDonViTruyenNhan(idDonViTruyenNhan));
        }

        /// <summary>
        /// Cập nhật cấu hình đơn vị truyền nhận
        /// </summary>
        /// <param name="cauHinhDonViTruyenNhan"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("cap-nhat-cau-hinh-don-vi-truyen-nhan")]
        [Authorize(Roles = EPermission.CAU_HINH_DON_VI_TRUYEN_NHAN)]
        public IActionResult CauHinhDonViTruyenNhan([FromBody] CauHinhDonViTruyenNhanParam cauHinhDonViTruyenNhan)
        {
            return Ok(cauHinhService.CauHinhDonViTruyenNhan(cauHinhDonViTruyenNhan, User.ThongTin()));
        }

        /// <summary>
        /// Kích hoạt đơn vị truyền nhận
        /// </summary>
        /// <param name="kichHoatDonViTruyenNhan"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("kich-hoat-don-vi-truyen-nhan")]
        [Authorize(Roles = EPermission.KICH_HOAT_CAU_HINH_DON_VI_TRUYEN_NHAN)]
        public IActionResult KichHoatDonViTruyenNhan([FromBody] KichHoatDonViTruyenNhanParam kichHoatDonViTruyenNhan)
        {
            return Ok(cauHinhService.KichHoatDonViTruyenNhan(kichHoatDonViTruyenNhan, User.ThongTin()));
        }

        /// <summary>
        /// Từ chối kích hoạt đơn vị truyền nhận
        /// </summary>
        /// <param name="huyKichHoatDonViTruyenNhan"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("tu-choi-kich-hoat-don-vi-truyen-nhan")]
        [Authorize(Roles = EPermission.TU_CHOI_KICH_HOAT_CAU_HINH_DON_VI_TRUYEN_NHAN)]
        public IActionResult TuChoiKichHoatDonViTruyenNhan([FromBody] TuChoiDonViTruyenNhanParam huyKichHoatDonViTruyenNhan)
        {
            return Ok(cauHinhService.TuChoiKichHoatDonViTruyenNhan(huyKichHoatDonViTruyenNhan, User.ThongTin()));
        }
    }
}
