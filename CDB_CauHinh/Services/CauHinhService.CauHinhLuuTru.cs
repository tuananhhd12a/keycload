﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.Mongo.LuuTru;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using Minio.DataModel;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace CDB_CauHinh.Services
{
    public partial class CauHinhService
    {
        public DataResponsive<List<DanhSachLoaiCauHinhLuuTruResponsive>> DanhSachLoaiCauHinhLuuTru()
        {
            var data = new List<DanhSachLoaiCauHinhLuuTruResponsive>()
            {
                new DanhSachLoaiCauHinhLuuTruResponsive()
                {
                    IdLoaiCauHinh = ELoaiCauHinhLuuTru.CAU_HINH.Id,
                    TenLoaiCauHinh = ELoaiCauHinhLuuTru.CAU_HINH.Name
                },
                new DanhSachLoaiCauHinhLuuTruResponsive()
                {
                    IdLoaiCauHinh = ELoaiCauHinhLuuTru.TRUYEN_TAI.Id,
                    TenLoaiCauHinh = ELoaiCauHinhLuuTru.TRUYEN_TAI.Name
                }
            };
            return new DataResponsive<List<DanhSachLoaiCauHinhLuuTruResponsive>>().returnData(data, EMaLoi.THANH_CONG);
        }

        public DataResponsive<LoadResult> DanhSachCauHinhLuuTru(DataSourceLoadOptionsBase loadOptionsBase)
        {
            try
            {
                logger.LogInformation($"Lấy danh sách cấu hình lưu trữ");
                loadOptionsBase.StringToLower = true;
                var cauHinhs = dbMinio.Find(x => true)
                                      .Project(x => new DanhSachCauHinhLuuTruResponsive()
                                      {
                                          IdCauHinh = x._id,
                                          EndPoint = x.EndPoint,
                                          Bucket = x.Bucket,
                                          AccessKey = x.AccessKey,
                                          SecretKey = x.SecretKey,
                                          LoaiLuuTru = x.LoaiLuuTru,
                                      }).ToList();
                var data = DataSourceLoader.Load(cauHinhs, loadOptionsBase);
                logger.LogInformation($"Lấy danh sách cấu hình lưu trữ thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách cấu hình lưu trữ gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> CauHinhMinio(CauHinhLuuTruMinioParam configMinio)
        {
            try
            {
                logger.LogInformation($"Bắt đầu cấu hình minio: - {configMinio.Object2Json()}");
                var loaiLuuTru = ELoaiCauHinhLuuTru.GetLoaiCauHinhLuuTru(configMinio.LoaiLuuTru);
                if (loaiLuuTru == null)
                {
                    logger.LogError($"Loại lưu trữ không hợp lệm");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Loại lưu trữ không hợp lệ");
                }
                if (configMinio.IdCauHinh.HasValue)
                {
                    var cauHinh = dbMinio.Find(x => x._id == configMinio.IdCauHinh).FirstOrDefault();
                    if (cauHinh == null)
                    {
                        logger.LogError($"Không tìm thấy cấu hình");
                        return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "cấu hình"));
                    }

                    var _cauHinh = dbMinio.Find(x => x._id != configMinio.IdCauHinh && x.LoaiLuuTru == configMinio.LoaiLuuTru).FirstOrDefault();
                    if (_cauHinh != null)
                    {
                        logger.LogError($"Đã tồn tại cấu hình lưu trữ {configMinio.LoaiLuuTru}");
                        return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đã tồn tại cấu hình lưu trữ {configMinio.LoaiLuuTru}");
                    }

                    cauHinh.EndPoint = configMinio.EndPoint;
                    cauHinh.Bucket = configMinio.Bucket;
                    cauHinh.AccessKey = configMinio.AccessKey;
                    cauHinh.SecretKey = configMinio.SecretKey;
                    cauHinh.LoaiLuuTru = configMinio.LoaiLuuTru;
                    dbMinio.ReplaceOne(x => x._id == cauHinh._id, cauHinh);
                }
                else
                {
                    var _cauHinh = dbMinio.Find(x => x.LoaiLuuTru == configMinio.LoaiLuuTru).FirstOrDefault();
                    if (_cauHinh != null)
                    {
                        logger.LogError($"Đã tồn tại cấu hình lưu trữ {configMinio.LoaiLuuTru}");
                        return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đã tồn tại cấu hình lưu trữ {configMinio.LoaiLuuTru}");
                    }

                    var cauHinh = new ConfigMinio()
                    {
                        _id = Guid.NewGuid(),
                        EndPoint = configMinio.EndPoint,
                        Bucket = configMinio.Bucket,
                        AccessKey = configMinio.AccessKey,
                        SecretKey = configMinio.SecretKey,
                        LoaiLuuTru = configMinio.LoaiLuuTru,
                    };
                    dbMinio.InsertOne(cauHinh);
                }
                logger.LogInformation($"Cấu hình minio thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Cấu hình minio gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
