﻿using AutoMapper;
using CDB_CauHinh.ViewModels.Output;
using Lib_Core.Objects.Mongo.CauHinh;

namespace CDB_CauHinh.Services
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CauHinh_DonViTruyenNhan, ChiTietDonViTruyenNhanResponsive>()
                    .ForMember(dest => dest.IdDonViTruyenNhan, opt => opt.MapFrom(x => x._id));
        }
    }
}
