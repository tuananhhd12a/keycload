﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lib_Core.CheckSum;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.JWT;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;

namespace CDB_CauHinh.Services
{
    public partial class CauHinhService
    {
        public DataResponsive<LoadResult> DanhSachDonViTruyenNhan(DataSourceLoadOptionsBase loadOptions)
        {
            try
            {
                logger.LogInformation($"Lấy danh sách đơn vị truyền nhận");
                loadOptions.StringToLower = true;
                var donVis = dbDonViTruyenNhan.AsQueryable()
                                              .Select(x => new DanhSachDonViTruyenNhanResponsive
                                              {
                                                  IdDonViTruyenNhan = x._id,
                                                  MaSoThue = x.MaSoThue,
                                                  TenDonVi = x.TenDonVi,
                                                  DiaChi = x.DiaChi,
                                                  NguoiDaiDien = x.NguoiDaiDien,
                                                  SDT_NguoiDaiDien = x.SDT_NguoiDaiDien,
                                                  Email = x.Email,
                                                  IdTrangThai = x.TrangThai.Id,
                                                  TenTrangThai = x.TrangThai.Name,
                                                  MaMauTrangThai = x.TrangThai.Description
                                              });
                var data = DataSourceLoader.Load(donVis, loadOptions);
                logger.LogInformation($"Lấy danh sách đơn vị truyền nhận thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách đơn vị truyền nhận gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<LoadResult> DanhSachDonViTruyenNhanDangHoatDong(DataSourceLoadOptionsBase loadOptions)
        {
            try
            {
                logger.LogInformation($"Lấy danh sách đơn vị truyền nhận đang hoạt động");
                loadOptions.StringToLower = true;
                var donVis = dbDonViTruyenNhan.AsQueryable()
                                              .Where(x => x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id)
                                              .Select(x => new DanhSachDonViTruyenNhanResponsive
                                              {
                                                  IdDonViTruyenNhan = x._id,
                                                  MaSoThue = x.MaSoThue,
                                                  TenDonVi = x.TenDonVi,
                                                  DiaChi = x.DiaChi,
                                                  NguoiDaiDien = x.NguoiDaiDien,
                                                  SDT_NguoiDaiDien = x.SDT_NguoiDaiDien,
                                                  Email = x.Email,
                                                  IdTrangThai = x.TrangThai.Id,
                                                  TenTrangThai = x.TrangThai.Name,
                                                  MaMauTrangThai = x.TrangThai.Description
                                              });
                var data = DataSourceLoader.Load(donVis, loadOptions);
                logger.LogInformation($"Lấy danh sách đơn vị truyền nhận đang hoạt động thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách đơn vị truyền nhận đang hoạt động gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<ChiTietDonViTruyenNhanResponsive> ChiTietCauHinhDonViTruyenNhan(Guid idDonViTruyenNhan)
        {
            try
            {
                logger.LogInformation($"Lấy chi tiết cấu hình đơn vị truyền nhận - {idDonViTruyenNhan}");
                var donVi = dbDonViTruyenNhan.Find(x => x._id == idDonViTruyenNhan).FirstOrDefault();
                if (donVi == null)
                {
                    logger.LogError($"Không tìm thấy đơn vị truyền nhận");
                    return new DataResponsive<ChiTietDonViTruyenNhanResponsive>().returnData(new ChiTietDonViTruyenNhanResponsive(), EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị truyền nhận"));
                }
                var chiTiet = mapper.Map<ChiTietDonViTruyenNhanResponsive>(donVi);
                logger.LogInformation($"Lấy chi tiết cấu hình đơn vị truyền nhận thành công");
                return new DataResponsive<ChiTietDonViTruyenNhanResponsive>().returnData(chiTiet, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy chi tiết cấu hình đơn vị truyền nhận gặp lỗi");
                return new DataResponsive<ChiTietDonViTruyenNhanResponsive>().returnData(new ChiTietDonViTruyenNhanResponsive(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> CauHinhDonViTruyenNhan(CauHinhDonViTruyenNhanParam cauHinhDonViTruyenNhan, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Cấu hình đơn vị truyền nhận - {cauHinhDonViTruyenNhan.Object2Json()}");
                cauHinhDonViTruyenNhan = cauHinhDonViTruyenNhan.ObjectNormalization<CauHinhDonViTruyenNhanParam>();
                //Kiểm tra publicKey
                var cer = DataValidation.GetX509(cauHinhDonViTruyenNhan.PublicKey);
                if (cer == null)
                {
                    logger.LogError("PublicKey không hợp lệ");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, "PublicKey không hợp lệ!");
                }
                var cauHinh = new CauHinh_DonViTruyenNhan()
                {
                    _id = Guid.NewGuid(),
                    MaSoThue = cauHinhDonViTruyenNhan.MaSoThue,
                    TenDonVi = cauHinhDonViTruyenNhan.TenDonVi,
                    DiaChi = cauHinhDonViTruyenNhan.DiaChi,
                    NguoiDaiDien = cauHinhDonViTruyenNhan.NguoiDaiDien,
                    SDT_NguoiDaiDien = cauHinhDonViTruyenNhan.SDTNguoiDaiDien,
                    Email = cauHinhDonViTruyenNhan.Email,
                    NguoiPhuTrachKyThuat = cauHinhDonViTruyenNhan.NguoiPhuTrachKyThuat,
                    SDT_PhuTrachKyThuat = cauHinhDonViTruyenNhan.SDTNguoiPhuTrachKyThuat,
                    IP_MayChu = cauHinhDonViTruyenNhan.IPMayChu,
                    PublicKey = cauHinhDonViTruyenNhan.PublicKey,
                    TrangThai = ETrangThaiCauHinh.CHO_KICH_HOAT,
                    ThoiGianBatDauHieuLuc = cer.NotBefore,
                    ThoiGianKetThucHieuLuc = cer.NotAfter,
                    Created_at = Common.now,
                    Created_by = taiKhoan.IdTaiKhoan,
                    Created_by_name = taiKhoan.HoTen,
                };
                dbDonViTruyenNhan.InsertOne(cauHinh);
                logger.LogInformation($"Cấu hình đơn vị truyền nhận thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Cấu hình đơn vị truyền nhận gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> KichHoatDonViTruyenNhan(KichHoatDonViTruyenNhanParam kichHoatDonViTruyenNhan, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Kích hoạt đơn vị truyền nhận");
                var donVi = dbDonViTruyenNhan.Find(x => x._id == kichHoatDonViTruyenNhan.IdDonViTruyenNhan).FirstOrDefault();
                if (donVi == null)
                {
                    logger.LogError($"Kích hoạt đơn vị truyền nhận gặp lỗi - Không tìm thấy đơn vị truyền nhận");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị truyền nhận"));
                }
                if (donVi.TrangThai.Id != ETrangThaiCauHinh.CHO_KICH_HOAT.Id)
                {
                    logger.LogError($"Kích hoạt đơn vị truyền nhận gặp lỗi - đơn vị truyền nhận đã được xử lý trước đó");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đơn vị truyền nhận đã được xử lý trước đó!");
                }
                var _donVis = dbDonViTruyenNhan.Find(x => x._id != donVi._id && x.MaSoThue == donVi.MaSoThue && (x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id || x.TrangThai.Id == ETrangThaiCauHinh.CHO_KICH_HOAT.Id)).ToList();
                foreach (var _donVi in _donVis)
                {
                    _donVi.TrangThai = ETrangThaiCauHinh.BI_THAY_THE;
                    _donVi.Updated_at = Common.now;
                    _donVi.Updated_by = taiKhoan.IdTaiKhoan;
                    _donVi.Updated_by_name = taiKhoan.HoTen;
                    dbDonViTruyenNhan.ReplaceOne(x => x._id == _donVi._id, _donVi);
                }

                donVi.TrangThai = ETrangThaiCauHinh.DANG_HOAT_DONG;
                donVi.Updated_at = Common.now;
                donVi.Updated_by = taiKhoan.IdTaiKhoan;
                donVi.Updated_by_name = taiKhoan.HoTen;
                dbDonViTruyenNhan.ReplaceOne(x => x._id == donVi._id, donVi);

                logger.LogInformation($"Kích hoạt đơn vị truyền nhận thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Kích hoạt đơn vị truyền nhận gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> TuChoiKichHoatDonViTruyenNhan(TuChoiDonViTruyenNhanParam huyKichHoatDonViTruyenNhan, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Từ chối đơn vị truyền nhận");
                var donVi = dbDonViTruyenNhan.Find(x => x._id == huyKichHoatDonViTruyenNhan.IdDonViTruyenNhan).FirstOrDefault();
                if (donVi == null)
                {
                    logger.LogError($"Từ chối đơn vị truyền nhận gặp lỗi - Không tìm thấy đơn vị truyền nhận");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị truyền nhận"));
                }
                if (donVi.TrangThai.Id != ETrangThaiCauHinh.CHO_KICH_HOAT.Id)
                {
                    logger.LogError($"Từ chối đơn vị truyền nhận gặp lỗi - đơn vị truyền nhận đã được xử lý trước đó");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đơn vị truyền nhận đã được xử lý trước đó!");
                }

                donVi.TrangThai = ETrangThaiCauHinh.TU_CHOI;
                donVi.Updated_at = Common.now;
                donVi.Updated_by = taiKhoan.IdTaiKhoan;
                donVi.Updated_by_name = taiKhoan.HoTen;
                dbDonViTruyenNhan.ReplaceOne(x => x._id == donVi._id, donVi);
                logger.LogInformation($"Từ chối đơn vị truyền nhận thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Từ chối đơn vị truyền nhận gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
