﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lib_Core.Enum;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System.Linq;
using System;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.DungChung;
using CDB_CauHinh.ViewModels.Input;
using Lib_Core.CheckSum;
using Lib_Core.Objects.JWT;
using CDB_CauHinh.ViewModels.Output;
using System.Collections.Generic;

namespace CDB_CauHinh.Services
{
    public partial class CauHinhService
    {

        public DataResponsive<List<DanhSachDoiTuongTuyenTaiResponsive>> DanhSachDoiTuongHoatDong()
        {
            var data = new List<DanhSachDoiTuongTuyenTaiResponsive>()
            {
                new DanhSachDoiTuongTuyenTaiResponsive()
                {
                    IdDoiTuong = EDoiTuongTruyenTai.BEN_XE.Id,
                    TenDoiTuong = EDoiTuongTruyenTai.BEN_XE.Name
                },
                new DanhSachDoiTuongTuyenTaiResponsive()
                {
                    IdDoiTuong = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id,
                    TenDoiTuong = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Name
                }
            };
            return new DataResponsive<List<DanhSachDoiTuongTuyenTaiResponsive>>().returnData(data, EMaLoi.THANH_CONG);
        }

        public DataResponsive<LoadResult> DanhSachCauHinhDonViHoatDong(DataSourceLoadOptionsBase loadOptionsBase)
        {
            try
            {
                logger.LogInformation($"Lấy danh sách cấu hình đơn vị hoạt động");
                loadOptionsBase.StringToLower = true;
                var donVis = (from hd in dbDonViHoatDong.AsQueryable()
                              join tn in dbDonViTruyenNhan.AsQueryable() on hd.IdDonViTruyenNhan equals tn._id
                              select new DanhSachDonViHoatDongResponsive
                              {
                                  IdDonVi = hd._id,
                                  TenDonVi = hd.TenDonVi,
                                  LoaiDonVi = hd.DoiTuongTruyenTai.Name,
                                  Email = hd.Email,
                                  MaSoGTVT = hd.MaSoGTVT,
                                  MaSoThue = hd.MaSoThue,
                                  TenDonViTruyenNhan = tn.TenDonVi,
                                  IdTrangThai = hd.TrangThai.Id,
                                  TenTrangThai = hd.TrangThai.Name,
                                  MaMauTrangThai = hd.TrangThai.Description,
                              });
                var data = DataSourceLoader.Load(donVis, loadOptionsBase);
                logger.LogInformation($"Lấy danh sách cấu hình đơn vị hoạt động thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách cấu hình đơn vị hoạt động gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> CauHinhDonViHoatDong(CauHinhDonViHoatDongParam cauHinhDonViHoatDong, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Cấu hình đơn vị hoạt động - {cauHinhDonViHoatDong.Object2Json()}");
                cauHinhDonViHoatDong = cauHinhDonViHoatDong.ObjectNormalization<CauHinhDonViHoatDongParam>();
                if (!string.IsNullOrWhiteSpace(cauHinhDonViHoatDong.PublicKey))
                {
                    //Kiểm tra publicKey
                    var cer = DataValidation.GetX509(cauHinhDonViHoatDong.PublicKey);
                    if (cer == null)
                    {
                        logger.LogError("PublicKey không hợp lệ");
                        return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, "PublicKey không hợp lệ!");
                    }
                }

                var donViTruyenNhan = dbDonViTruyenNhan.Find(x => x._id == cauHinhDonViHoatDong.IdDonViTruyenNhan).FirstOrDefault();
                if (donViTruyenNhan == null)
                {
                    logger.LogError($"Không tìm thấy đơn vị truyền nhận");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "Đơn vị truyền nhận"));
                }
                if (donViTruyenNhan.TrangThai.Id != ETrangThaiCauHinh.DANG_HOAT_DONG.Id)
                {
                    logger.LogError($"Đơn vị truyền nhận chưa được kích hoạt");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đơn vị truyền nhận chưa được kích hoạt");
                }
                var doiTuongTuyenTai = EDoiTuongTruyenTai.GetDoiTuongTruyenTai(cauHinhDonViHoatDong.LoaiDonVi);
                if (doiTuongTuyenTai == null)
                {
                    logger.LogError($"Loại đơn vị không hợp lệ");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Loại đơn vị không hợp lệ");
                }
                if (doiTuongTuyenTai == EDoiTuongTruyenTai.BEN_XE)
                {
                    if (string.IsNullOrWhiteSpace(cauHinhDonViHoatDong.MaBenXe))
                    {
                        logger.LogError($"Mã bến xe không được bỏ trống");
                        return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Mã bến xe không được bỏ trống!");
                    }
                }
                var cauHinh = new CauHinh_DonViHoatDong()
                {
                    _id = Guid.NewGuid(),
                    IdDonViTruyenNhan = cauHinhDonViHoatDong.IdDonViTruyenNhan,
                    MaSoThue = cauHinhDonViHoatDong.MaSoThue,
                    MaBenXe = cauHinhDonViHoatDong.MaBenXe,
                    TenDonVi = cauHinhDonViHoatDong.TenDonVi,
                    DoiTuongTruyenTai = doiTuongTuyenTai,
                    MaSoGTVT = cauHinhDonViHoatDong.MaSoGTVT,
                    Email = cauHinhDonViHoatDong.Email,
                    DiaChi = cauHinhDonViHoatDong.DiaChi,
                    PublicKey = cauHinhDonViHoatDong.PublicKey,
                    TrangThai = ETrangThaiCauHinh.CHO_KICH_HOAT,
                    Created_at = Common.now,
                    Created_by = taiKhoan.IdTaiKhoan,
                    Created_by_name = taiKhoan.HoTen,
                };
                if (cauHinhDonViHoatDong.DanhSachFile != null)
                {
                    cauHinh.DanhSachFile = cauHinhDonViHoatDong.DanhSachFile.Select(x => LuuFile(cauHinh.MaSoThue, x)).ToList();
                }
                dbDonViHoatDong.InsertOne(cauHinh);
                logger.LogInformation($"Cấu hình đơn vị hoạt động thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Cấu hình đơn vị hoạt động gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<ChiTietDonViHoatDongResponsive> ChiTietDonViHoatDong(Guid idDonVi)
        {
            try
            {
                logger.LogInformation($"Lấy chi tiết đơn vị hoạt động - {idDonVi}");
                var donViHoatDong = dbDonViHoatDong.Find(x => x._id == idDonVi)
                                                   .FirstOrDefault();
                if (donViHoatDong == null)
                {
                    logger.LogError($"Không tìm thấy đơn vị hoạt động");
                    return new DataResponsive<ChiTietDonViHoatDongResponsive>().returnData(new ChiTietDonViHoatDongResponsive(), EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị hoạt động"));
                }
                var donViTruyenNhan = dbDonViTruyenNhan.Find(x => x._id == donViHoatDong.IdDonViTruyenNhan)
                                                       .FirstOrDefault();
                if (donViTruyenNhan == null)
                {
                    logger.LogError($"Không tìm thấy đơn vị truyền nhận");
                    return new DataResponsive<ChiTietDonViHoatDongResponsive>().returnData(new ChiTietDonViHoatDongResponsive(), EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị truyền nhận"));
                }
                var chiTiet = new ChiTietDonViHoatDongResponsive()
                {
                    IdDonVi = donViHoatDong._id,
                    TenDonViTruyenNhan = donViTruyenNhan.TenDonVi,
                    MaSoThue = donViHoatDong.MaSoThue,
                    MaBenXe = donViHoatDong.MaBenXe,
                    TenDonVi = donViHoatDong.TenDonVi,
                    Email = donViHoatDong.Email,
                    MaSoGTVT = donViHoatDong.MaSoGTVT,
                    DiaChi = donViHoatDong.DiaChi,
                    PublicKey = donViHoatDong.PublicKey,
                    DanhSachFile = donViHoatDong.DanhSachFile,
                    IdDoiTuongTruyenTai = donViHoatDong.DoiTuongTruyenTai.Id,
                    TenDoiTuongTruyenTai = donViHoatDong.DoiTuongTruyenTai.Name,
                    IdTrangThai = donViHoatDong.TrangThai.Id,
                    TenTrangThai = donViHoatDong.TrangThai.Name,
                    MaMauTrangThai = donViHoatDong.TrangThai.Description,
                };
                logger.LogInformation($"Lấy chi tiết đơn vị hoạt động thành công");
                return new DataResponsive<ChiTietDonViHoatDongResponsive>().returnData(chiTiet, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy chi tiết đơn vị hoạt động gặp lỗi");
                return new DataResponsive<ChiTietDonViHoatDongResponsive>().returnData(new ChiTietDonViHoatDongResponsive(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> KichHoatDonViHoatDong(KichHoatDonViHoatDongParam kichHoatDonViHoatDong, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Kích hoạt đơn vị hoạt động");
                var donVi = dbDonViHoatDong.Find(x => x._id == kichHoatDonViHoatDong.IdDonVi).FirstOrDefault();
                if (donVi == null)
                {
                    logger.LogError($"Kích hoạt đơn vị hoạt động gặp lỗi - Không tìm thấy đơn vị hoạt động");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị hoạt động"));
                }
                if (donVi.TrangThai.Id != ETrangThaiCauHinh.CHO_KICH_HOAT.Id)
                {
                    logger.LogError($"Kích hoạt đơn vị hoạt động gặp lỗi - đơn vị hoạt động đã được xử lý trước đó");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đơn vị hoạt động đã được xử lý trước đó!");
                }
                var _donVis = dbDonViHoatDong.Find(x => x._id != donVi._id && x.MaSoThue == donVi.MaSoThue && x.TrangThai == ETrangThaiCauHinh.DANG_HOAT_DONG).ToList();
                foreach (var _donVi in _donVis)
                {
                    _donVi.TrangThai = ETrangThaiCauHinh.BI_THAY_THE;
                    _donVi.Updated_at = Common.now;
                    _donVi.Updated_by = taiKhoan.IdTaiKhoan;
                    _donVi.Updated_by_name = taiKhoan.HoTen;
                    dbDonViHoatDong.ReplaceOne(x => x._id == _donVi._id, _donVi);
                }

                donVi.TrangThai = ETrangThaiCauHinh.DANG_HOAT_DONG;
                donVi.Updated_at = Common.now;
                donVi.Updated_by = taiKhoan.IdTaiKhoan;
                donVi.Updated_by_name = taiKhoan.HoTen;
                dbDonViHoatDong.ReplaceOne(x => x._id == donVi._id, donVi);

                logger.LogInformation($"Kích hoạt đơn vị hoạt động thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Kích hoạt đơn vị hoạt động gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> TuChoiKichHoatDonViHoatDong(TuChoiDonViHoatDongParam tuChoiDonViHoatDong, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Từ chối đơn vị hoạt động");
                var donVi = dbDonViHoatDong.Find(x => x._id == tuChoiDonViHoatDong.IdDonVi).FirstOrDefault();
                if (donVi == null)
                {
                    logger.LogError($"Từ chối đơn vị hoạt động gặp lỗi - Không tìm thấy đơn vị hoạt động");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "đơn vị hoạt động"));
                }
                if (donVi.TrangThai.Id != ETrangThaiCauHinh.CHO_KICH_HOAT.Id)
                {
                    logger.LogError($"Từ chối đơn vị hoạt động gặp lỗi - đơn vị hoạt động đã được xử lý trước đó");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đơn vị hoạt động đã được xử lý trước đó!");
                }

                donVi.TrangThai = ETrangThaiCauHinh.TU_CHOI;
                donVi.Updated_at = Common.now;
                donVi.Updated_by = taiKhoan.IdTaiKhoan;
                donVi.Updated_by_name = taiKhoan.HoTen;
                dbDonViHoatDong.ReplaceOne(x => x._id == donVi._id, donVi);

                logger.LogInformation($"Từ chối đơn vị hoạt động thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Từ chối đơn vị hoạt động gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

    }
}
