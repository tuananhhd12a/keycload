﻿using AutoMapper;
using Lib_Core.Objects.Mongo;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.Mongo.LuuTru;
using Lib_Core.Objects.Mongo.TruyenTai;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace CDB_CauHinh.Services
{
    public partial class CauHinhService
    {
        private readonly ILogger<CauHinhService> logger;
        private readonly IMapper mapper;
        private readonly IDistributedCache distributedCache;
        private readonly IMongoCollection<CauHinh_DonViTruyenNhan> dbDonViTruyenNhan;
        private readonly IMongoCollection<CauHinh_DonViHoatDong> dbDonViHoatDong;
        private readonly IMongoCollection<CauHinh_NhanVienKyLenh> dbNhanVien;
        private readonly IMongoCollection<ConfigMinio> dbMinio;

        public CauHinhService(ILogger<CauHinhService> logger, IMapper mapper, IDistributedCache distributedCache, IDatabaseMongoSettings settings)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.distributedCache = distributedCache;
            var client = new MongoClient($"{settings.ConnectionString}");
            this.dbDonViTruyenNhan = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_DonViTruyenNhan>(nameof(CauHinh_DonViTruyenNhan));
            this.dbMinio = client.GetDatabase(settings.DatabaseName).GetCollection<ConfigMinio>(nameof(ConfigMinio));
            this.dbDonViHoatDong = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_DonViHoatDong>(nameof(CauHinh_DonViHoatDong));
            this.dbNhanVien = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_NhanVienKyLenh>(nameof(CauHinh_NhanVienKyLenh));
        }
    }
}
