﻿using CDB_CauHinh.ViewModels.Input;
using CDB_CauHinh.ViewModels.Output;
using DevExtreme.AspNet.Data.ResponseModel;
using DevExtreme.AspNet.Data;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.JWT;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace CDB_CauHinh.Services
{
    public partial class CauHinhService
    {
        public DataResponsive<List<DanhSachLoaiChuKyResponsive>> DanhSachLoaiChuKy()
        {
            var data = new List<DanhSachLoaiChuKyResponsive>()
            {
                new DanhSachLoaiChuKyResponsive()
                {
                    IdLoaiChuKy = ELoaiChuKy.CHU_KY_SO.Id,
                    TenLoaiChuKy = ELoaiChuKy.CHU_KY_SO.Name
                },
                new DanhSachLoaiChuKyResponsive()
                {
                    IdLoaiChuKy = ELoaiChuKy.CHU_KY_THUONG.Id,
                    TenLoaiChuKy = ELoaiChuKy.CHU_KY_THUONG.Name
                }
            };
            return new DataResponsive<List<DanhSachLoaiChuKyResponsive>>().returnData(data, EMaLoi.THANH_CONG);
        }

        public DataResponsive<LoadResult> DanhSachNhanVienKyLenh(DataSourceLoadOptionsBase loadOptionsBase, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Lấy danh sách nhân viên ký lệnh");
                loadOptionsBase.StringToLower = true;
                var donVi = dbDonViHoatDong.Find(x => x.MaBenXe == taiKhoan.CauHinhTaiKhoan.MaBenXe && x.MaSoGTVT == taiKhoan.CauHinhTaiKhoan.MaSoGTVT && x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id).FirstOrDefault();
                if (donVi == null)
                {
                    logger.LogError($"Không tìm thấy đơn vị");
                    return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "Đơn vị"));
                }

                var nhanViens = dbNhanVien.Find(x => x.IdDonVi == donVi._id)
                                          .Project(x => new DanhSachNhanVienKyLenhResponsive
                                          {
                                              IdNhanVien = x._id,
                                              HoTen = x.HoTen,
                                              MaDinhDanh = x.MaDinhDanh,
                                              GhiChu = x.GhiChu,
                                              IdTrangThai = x.TrangThai.Id,
                                              TenTrangThai = x.TrangThai.Name,
                                              MaMauTrangThai = x.TrangThai.Description,
                                          }).ToList();
                var data = DataSourceLoader.Load(nhanViens, loadOptionsBase);
                logger.LogInformation($"Lấy danh sách nhân viên ký lệnh thành công");
                return new DataResponsive<LoadResult>().returnData(data, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy danh sách nhân viên ký lệnh gặp lỗi");
                return new DataResponsive<LoadResult>().returnData(new LoadResult(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<ChiTietNhanVienKyLenhResponsive> ChiTietNhanVienKyLenh(Guid idNhanVien)
        {
            try
            {
                logger.LogInformation($"Lấy chi tiết nhân viên ký lệnh - {idNhanVien}");
                var nhanVien = dbNhanVien.Find(x => x._id == idNhanVien)
                                         .Project(x => new ChiTietNhanVienKyLenhResponsive
                                         {
                                             IdNhanVien = x._id,
                                             LoaiChuKy = x.LoaiChuKy.Id,
                                             HoTen = x.HoTen,
                                             MaDinhDanh = x.MaDinhDanh,
                                             GhiChu = x.GhiChu,
                                             Serial = x.Serial,
                                             PublicKey = x.PublicKey,
                                             TepDinhKem = x.TepDinhKem,
                                             IdTrangThai = x.TrangThai.Id,
                                             TenTrangThai = x.TrangThai.Name,
                                             MaMauTrangThai = x.TrangThai.Description,
                                         })
                                         .FirstOrDefault();
                if (nhanVien == null)
                {
                    logger.LogError($"Không tìm thấy nhân viên ký lệnh");
                    return new DataResponsive<ChiTietNhanVienKyLenhResponsive>().returnData(new ChiTietNhanVienKyLenhResponsive(), EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "nhân viên ký lệnh"));
                }

                logger.LogInformation($"Lấy chi tiết nhân viên ký lệnh thành công");
                return new DataResponsive<ChiTietNhanVienKyLenhResponsive>().returnData(nhanVien, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy chi tiết nhân viên ký lệnh gặp lỗi");
                return new DataResponsive<ChiTietNhanVienKyLenhResponsive>().returnData(new ChiTietNhanVienKyLenhResponsive(), EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<string> CauHinhNhanVienKyLenh(CauHinhNhanVienKyLenhParam cauHinhNhanVienKyLenh, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Cấu hình nhân viên ký lệnh - {cauHinhNhanVienKyLenh.Object2Json()}");
                var donViTruyenTai = dbDonViHoatDong.Find(x => x.MaBenXe == taiKhoan.CauHinhTaiKhoan.MaBenXe && x.MaSoGTVT == taiKhoan.CauHinhTaiKhoan.MaSoGTVT && x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id).FirstOrDefault();
                if (donViTruyenTai == null)
                {
                    logger.LogError($"Không tìm thấy đơn vị");
                    return new DataResponsive<string>().returnData("", EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "Đơn vị"));
                }
                var loaiChuKy = ELoaiChuKy.GetLoaiChuKy(cauHinhNhanVienKyLenh.LoaiChuKy);
                if (loaiChuKy == null)
                {
                    logger.LogError($"Loại chữ ký không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Loại chữ ký không hợp lệ");
                }
                if (loaiChuKy == ELoaiChuKy.CHU_KY_SO)
                {
                    if (string.IsNullOrWhiteSpace(cauHinhNhanVienKyLenh.Serial))
                    {
                        logger.LogError($"Serial không được bỏ trống!");
                        return new DataResponsive<string>().returnData("", EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Serial không được bỏ trống!");
                    }
                    else if (string.IsNullOrWhiteSpace(cauHinhNhanVienKyLenh.PublicKey))
                    {
                        logger.LogError($"PublicKey không được bỏ trống!");
                        return new DataResponsive<string>().returnData("", EMaLoi.DU_LIEU_KHONG_HOP_LE, $"PublicKey không được bỏ trống!");
                    }
                }
                else if (cauHinhNhanVienKyLenh.FileDinhKem == null)
                {
                    logger.LogError($"File đính kèm không được bỏ trống!");
                    return new DataResponsive<string>().returnData("", EMaLoi.DU_LIEU_KHONG_HOP_LE, $"File đính kèm không được bỏ trống!");
                }
                //Thêm cấu hình
                var nhanVien = new CauHinh_NhanVienKyLenh()
                {
                    _id = Guid.NewGuid(),
                    IdDonVi = donViTruyenTai._id,
                    MaDinhDanh = Guid.NewGuid().ToString().Replace("-", "").ToUpper(),
                    HoTen = cauHinhNhanVienKyLenh.HoTen,
                    GhiChu = cauHinhNhanVienKyLenh.GhiChu,
                    Serial = cauHinhNhanVienKyLenh.Serial,
                    PublicKey = cauHinhNhanVienKyLenh.PublicKey,
                    LoaiChuKy = loaiChuKy,
                    TrangThai = ETrangThaiCauHinh.DANG_HOAT_DONG,
                    Created_at = Common.now,
                    Created_by = taiKhoan.IdTaiKhoan,
                    Created_by_name = taiKhoan.HoTen,
                };
                if (cauHinhNhanVienKyLenh.FileDinhKem != null)
                {
                    nhanVien.TepDinhKem = LuuFile(donViTruyenTai.MaSoThue, cauHinhNhanVienKyLenh.FileDinhKem);
                }
                dbNhanVien.InsertOne(nhanVien);
                logger.LogInformation($"Cấu hình nhân viên ký lệnh thành công");
                return new DataResponsive<string>().returnData(nhanVien.MaDinhDanh, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Cấu hình nhân viên ký lệnh gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> KichHoatNhanVienKyLenh(KichHoatNhanVienKyLenhParam kichHoatNhanVienKyLenh, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Kích hoạt nhân viên ký lệnh");
                var nhanVien = dbNhanVien.Find(x => x._id == kichHoatNhanVienKyLenh.IdNhanVien).FirstOrDefault();
                if (nhanVien == null)
                {
                    logger.LogError($"Kích hoạt nhân viên ký lệnh gặp lỗi - Không tìm thấy nhân viên ký lệnh");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "nhân viên ký lệnh"));
                }
                if (nhanVien.TrangThai.Id != ETrangThaiCauHinh.CHO_KICH_HOAT.Id)
                {
                    logger.LogError($"Kích hoạt nhân viên ký lệnh gặp lỗi - nhân viên ký lệnh đã được xử lý trước đó");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Đơn vị hoạt động đã được xử lý trước đó!");
                }

                nhanVien.TrangThai = ETrangThaiCauHinh.DANG_HOAT_DONG;
                nhanVien.Updated_at = Common.now;
                nhanVien.Updated_by = taiKhoan.IdTaiKhoan;
                nhanVien.Updated_by_name = taiKhoan.HoTen;
                dbNhanVien.ReplaceOne(x => x._id == nhanVien._id, nhanVien);

                logger.LogInformation($"Kích hoạt nhân viên ký lệnh thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Kích hoạt nhân viên ký lệnh gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> TuChoiKichHoatNhanVienKyLenh(TuChoiNhanVienKyLenhParam tuChoiNhanVienKyLenh, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Từ chối nhân viên ký lệnh");
                var nhanVien = dbNhanVien.Find(x => x._id == tuChoiNhanVienKyLenh.IdNhanVien).FirstOrDefault();
                if (nhanVien == null)
                {
                    logger.LogError($"Từ chối nhân viên ký lệnh gặp lỗi - Không tìm thấy nhân viên ký lệnh");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "nhân viên ký lệnh"));
                }
                if (nhanVien.TrangThai.Id != ETrangThaiCauHinh.CHO_KICH_HOAT.Id)
                {
                    logger.LogError($"Từ chối nhân viên ký lệnh gặp lỗi - nhân viên ký lệnh đã được xử lý trước đó");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Nhân viên ký lệnh đã được xử lý trước đó!");
                }

                nhanVien.TrangThai = ETrangThaiCauHinh.TU_CHOI;
                nhanVien.Updated_at = Common.now;
                nhanVien.Updated_by = taiKhoan.IdTaiKhoan;
                nhanVien.Updated_by_name = taiKhoan.HoTen;
                dbNhanVien.ReplaceOne(x => x._id == nhanVien._id, nhanVien);

                logger.LogInformation($"Từ chối nhân viên ký lệnh thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Từ chối nhân viên ký lệnh gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> HuyKichHoatNhanVienKyLenh(HuyKichHoatNhanVienKyLenhParam huyKichHoatNhanVienKyLenh, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Hủy kích hoạt nhân viên ký lệnh");
                var nhanVien = dbNhanVien.Find(x => x._id == huyKichHoatNhanVienKyLenh.IdNhanVien).FirstOrDefault();
                if (nhanVien == null)
                {
                    logger.LogError($"Hủy kích hoạt nhân viên ký lệnh gặp lỗi - Không tìm thấy nhân viên ký lệnh");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, Common.GetMessage(EMaLoi.KHONG_TIM_THAY_DU_LIEU_HE_THONG, "nhân viên ký lệnh"));
                }
                if (nhanVien.TrangThai.Id != ETrangThaiCauHinh.DANG_HOAT_DONG.Id)
                {
                    logger.LogError($"Hủy kích hoạt nhân viên ký lệnh gặp lỗi - nhân viên hiện đang không hoạt động");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DU_LIEU_KHONG_HOP_LE, $"Nhân viên không hoạt động!");
                }

                nhanVien.TrangThai = ETrangThaiCauHinh.HUY;
                nhanVien.Updated_at = Common.now;
                nhanVien.Updated_by = taiKhoan.IdTaiKhoan;
                nhanVien.Updated_by_name = taiKhoan.HoTen;
                dbNhanVien.ReplaceOne(x => x._id == nhanVien._id, nhanVien);

                logger.LogInformation($"Hủy kích hoạt nhân viên ký lệnh thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Hủy kích hoạt nhân viên ký lệnh gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

    }
}
