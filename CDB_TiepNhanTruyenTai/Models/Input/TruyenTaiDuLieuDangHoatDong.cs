﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using Lib_Core.Attributes;

namespace CDB_TiepNhanTruyenTai.Models.Input
{
    public class TruyenTaiDuLieuDangHoatDong
    {
        [GuidValidation(ErrorMessage = "{0} không được bỏ trống!")]
        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã lệnh vận chuyển")]
        public Guid MaLenhVanChuyen { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã lệnh")]
        public string MaLenh { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Số khách")]
        public string SoKhach { get; set; }

        [DisplayName("Tọa độ GPS")]
        public string ToaDoGPS { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Checksum")]
        public string Checksum { get; set; }
    }
}
