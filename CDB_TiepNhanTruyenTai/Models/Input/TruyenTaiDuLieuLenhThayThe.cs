﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Lib_Core.Attributes;
using System.Collections.Generic;
using System;
using Lib_Core.CheckSum;

namespace CDB_TiepNhanTruyenTai.Models.Input
{
    public class TruyenTaiDuLieuLenhThayThe
    {
        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã lệnh")]
        [CheckSumOrder(1)]
        public string MaLenh { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã lệnh bị thay thế")]
        [CheckSumOrder(2)]
        public string MaLenhBiThayThe { get; set; }

        [GuidValidation(ErrorMessage = "{0} không được bỏ trống!")]
        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã lệnh vận chuyển")]
        [CheckSumOrder(3)]
        public Guid MaLenhVanChuyen { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("xml")]
        public string XML { get; set; }

        [EnsureElement(1, 2, ErrorMessage = "{0} tối thiểu {1} và tối đa {2} tệp!")]
        [DisplayName("Danh sách tệp đính kèm")]
        public List<TepDinhKem> DanhSachTepDinhKem { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Checksum")]
        public string Checksum { get; set; }
    }
}
