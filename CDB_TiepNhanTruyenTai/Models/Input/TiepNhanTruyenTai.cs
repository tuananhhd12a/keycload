﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections.Generic;
using Lib_Core.Attributes;
using System;
using Lib_Core.CheckSum;

namespace CDB_TiepNhanTruyenTai.Models.Input
{
    public class TiepNhanTruyenTai
    {
        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Mã lệnh")]
        [CheckSumOrder(1)]
        public string MaLenh { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("xml")]
        public string XML { get; set; }

        [EnsureElement(1, 2, ErrorMessage = "{0} tối thiểu {1} và tối đa {2} tệp!")]
        [DisplayName("Danh sách tệp đính kèm")]
        public List<TepDinhKem> DanhSachTepDinhKem { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Checksum")]
        public string Checksum { get; set; }
    }
}
