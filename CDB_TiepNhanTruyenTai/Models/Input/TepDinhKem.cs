﻿using Lib_Core.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CDB_TiepNhanTruyenTai.Models.Input
{
    public class TepDinhKem
    {
        [TepDinhKemValidation(ErrorMessage = "{0} chỉ chấp nhận loại tệp là pdf hoặc jpg!")]
        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Loại tệp đính kèm")]
        public string Loai { get; set; }

        [Required(ErrorMessage = "{0} không được bỏ trống!")]
        [DisplayName("Data tệp đính kèm")]
        public string Data { get; set; }
    }
}
