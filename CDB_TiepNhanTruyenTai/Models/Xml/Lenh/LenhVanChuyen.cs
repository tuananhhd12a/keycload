﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Serialization;
using CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen;
using Lib_Core.Enum;

namespace CDB_TiepNhanTruyenTai.Models.Xml.Lenh
{
    public class LenhVanChuyen : IValidatableObject
    {
        public DuLieu DuLieu { get; set; }

        public DanhSachChuKySo DanhSachChuKySo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();
            var thoiGianKhongHopLe = new DateTime(1970, 01, 01);

            if (this.DuLieu == null)
            {
                errors.Add(new ValidationResult("Dữ liệu không được bỏ trống!"));
            }
            else
            {
                if (this.DuLieu.ThongTinChung == null)
                {
                    errors.Add(new ValidationResult("Thông tin chung không được bỏ trống!"));
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.PhienBan))
                    {
                        errors.Add(new ValidationResult("Phiên bản không được bỏ trống!"));
                    }
                    else if (this.DuLieu.ThongTinChung.PhienBan.Length > 16)
                    {
                        errors.Add(new ValidationResult("Phiên bản không được vượt quá 16 ký tự!"));
                    }

                    if (string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan))
                    {
                        errors.Add(new ValidationResult("Mã số thuế của đơn vị truyền dữ liệu không được bỏ trống!"));
                    }
                    else if (this.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan.Length > 32)
                    {
                        errors.Add(new ValidationResult("Mã số thuế của đơn vị truyền dữ liệu không được vượt quá 16 ký tự!"));
                    }

                    if (string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.TenDonViTruyenNhan))
                    {
                        errors.Add(new ValidationResult("Tên đơn vị truyền dữ liệu không được bỏ trống!"));
                    }
                    else if (this.DuLieu.ThongTinChung.TenDonViTruyenNhan.Length > 512)
                    {
                        errors.Add(new ValidationResult("Tên đơn vị truyền dữ liệu không được vượt quá 512 ký tự!"));
                    }

                    if (string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.MaLenhVanChuyen))
                    {
                        errors.Add(new ValidationResult("Mã lệnh vận chuyển không được bỏ trống!"));
                    }
                    else if (this.DuLieu.ThongTinChung.MaLenhVanChuyen.Length > 64)
                    {
                        errors.Add(new ValidationResult("Mã lệnh vận chuyển không được vượt quá 64 ký tự!"));
                    }

                    if (!string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.MaLenhVanChuyenBiThayThe) && this.DuLieu.ThongTinChung.MaLenhVanChuyen.Length > 64)
                    {
                        errors.Add(new ValidationResult("Mã lệnh vận chuyển bị thay thế không được vượt quá 64 ký tự!"));
                    }

                    if (!string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.KyHieuMauSoLenh) && this.DuLieu.ThongTinChung.KyHieuMauSoLenh.Length > 64)
                    {
                        errors.Add(new ValidationResult("Mẫu số lệnh vận chuyển không được vượt quá 64 ký tự!"));
                    }

                    if (string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.TenTaiLieu))
                    {
                        errors.Add(new ValidationResult("Tên tài liệu không được bỏ trống!"));
                    }
                    else if (this.DuLieu.ThongTinChung.TenTaiLieu.Length > 512)
                    {
                        errors.Add(new ValidationResult("Tên tài liệu không được vượt quá 512 ký tự!"));
                    }

                    var loaiLenh = EMaLoaiLenh.GetLoaiLenh(this.DuLieu.ThongTinChung.MaLoaiLenh);
                    if (loaiLenh == null)
                    {
                        errors.Add(new ValidationResult("Mã loại lệnh không hợp lệ!"));
                    }

                    if (!string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.KyHieuLenh) && this.DuLieu.ThongTinChung.KyHieuLenh.Length > 32)
                    {
                        errors.Add(new ValidationResult("Ký hiệu lệnh không được vượt quá 32 ký tự!"));
                    }

                    if (string.IsNullOrWhiteSpace(this.DuLieu.ThongTinChung.SoLenh))
                    {
                        errors.Add(new ValidationResult("Số lệnh không được bỏ trống!"));
                    }
                    else if (this.DuLieu.ThongTinChung.SoLenh.Length > 32)
                    {
                        errors.Add(new ValidationResult("Số lệnh không được vượt quá 32 ký tự!"));
                    }

                    if (this.DuLieu.ThongTinChung.NgayKhoiTao == null)
                    {
                        errors.Add(new ValidationResult("Ngày khởi tạo không hợp lệ!"));
                    }
                    else if (this.DuLieu.ThongTinChung.NgayKhoiTao.Date == thoiGianKhongHopLe)
                    {
                        errors.Add(new ValidationResult("Ngày khởi tạo không hợp lệ!"));
                    }

                    if (this.DuLieu.ThongTinChung.ThongTinKhac != null)
                    {
                        foreach (var thongTinKhac in this.DuLieu.ThongTinChung.ThongTinKhac)
                        {
                            if (!string.IsNullOrWhiteSpace(thongTinKhac.TenTruong) && thongTinKhac.TenTruong.Length > 256)
                            {
                                errors.Add(new ValidationResult("Tên trường thông tin khác không được vượt quá 256 ký tự!"));
                            }

                            if (!string.IsNullOrWhiteSpace(thongTinKhac.KieuDuLieu) && thongTinKhac.KieuDuLieu.Length > 128)
                            {
                                errors.Add(new ValidationResult("Kiểu dữ liệu thông tin khác không được vượt quá 128 ký tự!"));
                            }

                            if (!string.IsNullOrWhiteSpace(thongTinKhac.DuLieu) && thongTinKhac.DuLieu.Length > 2048)
                            {
                                errors.Add(new ValidationResult("Dữ liệu thông tin khác không được vượt quá 2048 ký tự!"));
                            }
                        }
                    }
                }

                if (this.DuLieu.NoiDungLenhVanChuyen == null)
                {
                    errors.Add(new ValidationResult("Nội dung lệnh vận chuyển không được bỏ trống!"));
                }
                else
                {
                    if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep == null)
                    {
                        errors.Add(new ValidationResult("Thông tin doanh nghiệp không được bỏ trống!"));
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.TenDonViVanTai))
                        {
                            errors.Add(new ValidationResult("Tên đơn vị vận tải không được bỏ trống!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.TenDonViVanTai.Length > 512)
                        {
                            errors.Add(new ValidationResult("Tên đơn vị vận tải không được vượt quá 512 ký tự!"));
                        }

                        if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai))
                        {
                            errors.Add(new ValidationResult("Mã số thuế đơn vị vận tải không được bỏ trống!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai.Length > 32)
                        {
                            errors.Add(new ValidationResult("Mã số thuế đơn vị vận tải không được vượt quá 32 ký tự!"));
                        }

                        if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT))
                        {
                            errors.Add(new ValidationResult("Mã sở GTVT nơi cấp giấy phép kinh doanh vận tải không được bỏ trống!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT.Length > 32)
                        {
                            errors.Add(new ValidationResult("Mã sở GTVT nơi cấp giấy phép kinh doanh vận tải không được vượt quá 32 ký tự!"));
                        }

                        if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.DiaChi) && this.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.DiaChi.Length > 2048)
                        {
                            errors.Add(new ValidationResult("Địa chỉ của đơn vị vận tải không được vượt quá 2048 ký tự!"));
                        }
                    }

                    if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinChuyenXe == null)
                    {
                        errors.Add(new ValidationResult("Thông tin chuyến xe không được bỏ trống!"));
                    }
                    else
                    {
                        if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinChuyenXe.GioXuatBenThucTe == null)
                        {
                            errors.Add(new ValidationResult("Giờ xuất bến thực tế không hợp lệ!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinChuyenXe.GioXuatBenThucTe.Date == thoiGianKhongHopLe)
                        {
                            errors.Add(new ValidationResult("Giờ xuất bến thực tế không hợp lệ!"));
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinChuyenXe.GioXuatBenKeHoach == null)
                        {
                            errors.Add(new ValidationResult("Giờ xuất bến kế hoạch không hợp lệ!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinChuyenXe.GioXuatBenKeHoach.Date == thoiGianKhongHopLe)
                        {
                            errors.Add(new ValidationResult("Giờ xuất bến kế hoạch không hợp lệ!"));
                        }
                    }

                    if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen == null)
                    {
                        errors.Add(new ValidationResult("Thông tin tuyến vận chuyển không được bỏ trống!"));
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.MaSoTuyen))
                        {
                            errors.Add(new ValidationResult("Mã số tuyến không được bỏ trống!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.MaSoTuyen.Length > 16)
                        {
                            errors.Add(new ValidationResult("Mã số tuyến không được vượt quá 16 ký tự!"));
                        }

                        if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.TenTuyen))
                        {
                            errors.Add(new ValidationResult("Tên tuyến không được bỏ trống!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.TenTuyen.Length > 256)
                        {
                            errors.Add(new ValidationResult("Tên tuyến không được vượt quá 16 ký tự!"));
                        }

                        if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.HanhTrinhTuyen) && this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.HanhTrinhTuyen.Length > 2048)
                        {
                            errors.Add(new ValidationResult("Hành trình tuyến không được vượt quá 16 ký tự!"));
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi == null)
                        {
                            errors.Add(new ValidationResult("Bến đi không được bỏ trống!"));
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT))
                            {
                                errors.Add(new ValidationResult("Mã sở GTVT quản lý bến đi không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT.Length > 32)
                            {
                                errors.Add(new ValidationResult("Mã sở GTVT quản lý bến đi không được vượt quá 32 ký tự!"));
                            }

                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenSoGTVT))
                            {
                                errors.Add(new ValidationResult("Tên sở GTVT quản lý bến đi không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenSoGTVT.Length > 256)
                            {
                                errors.Add(new ValidationResult("Tên sở GTVT quản lý bến đi không được vượt quá 256 ký tự!"));
                            }

                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaBenXeNoiDi))
                            {
                                errors.Add(new ValidationResult("Mã bến đi không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaBenXeNoiDi.Length > 32)
                            {
                                errors.Add(new ValidationResult("Mã bến đi không được vượt quá 32 ký tự!"));
                            }

                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenBenNoiDi))
                            {
                                errors.Add(new ValidationResult("Tên bến đi không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenBenNoiDi.Length > 256)
                            {
                                errors.Add(new ValidationResult("Tên bến đi không được vượt quá 256 ký tự!"));
                            }
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen == null)
                        {
                            errors.Add(new ValidationResult("Bến đến không được bỏ trống!"));
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaSoGTVT))
                            {
                                errors.Add(new ValidationResult("Mã sở GTVT quản lý bến đến không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaSoGTVT.Length > 32)
                            {
                                errors.Add(new ValidationResult("Mã sở GTVT quản lý bến đến không được vượt quá 32 ký tự!"));
                            }

                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.TenSoGTVT))
                            {
                                errors.Add(new ValidationResult("Tên sở GTVT quản lý bến đến không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.TenSoGTVT.Length > 256)
                            {
                                errors.Add(new ValidationResult("Tên sở GTVT quản lý bến đến không được vượt quá 256 ký tự!"));
                            }

                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaBenXeNoiDen))
                            {
                                errors.Add(new ValidationResult("Mã bến đến không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaBenXeNoiDen.Length > 32)
                            {
                                errors.Add(new ValidationResult("Mã bến đến không được vượt quá 32 ký tự!"));
                            }

                            if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenBenNoiDi))
                            {
                                errors.Add(new ValidationResult("Tên bến đến không được bỏ trống!"));
                            }
                            else if (this.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenBenNoiDi.Length > 256)
                            {
                                errors.Add(new ValidationResult("Tên bến đến không được vượt quá 256 ký tự!"));
                            }
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien == null)
                        {
                            errors.Add(new ValidationResult("Thông tin phương tiện không được bỏ trống!"));
                        }
                        else
                        {
                            if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach == null)
                            {
                                errors.Add(new ValidationResult("Thông tin phương tiện theo kế hoạch không được bỏ trống!"));
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.BienKiemSoat))
                                {
                                    errors.Add(new ValidationResult("Biển kiểm soát phương tiện theo kế hoạch không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.BienKiemSoat.Length > 32)
                                {
                                    errors.Add(new ValidationResult("Biển kiểm soát phương tiện theo kế hoạch không được vượt quá 32 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.NhanHieu))
                                {
                                    errors.Add(new ValidationResult("Nhãn hiệu phương tiện theo kế hoạch không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.NhanHieu.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Nhãn hiệu phương tiện theo kế hoạch không được vượt quá 64 ký tự!"));
                                }
                            }

                            if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien == null)
                            {
                                errors.Add(new ValidationResult("Thông tin phương tiện thực hiện không được bỏ trống!"));
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien.BienKiemSoat))
                                {
                                    errors.Add(new ValidationResult("Biển kiểm soát phương tiện thực hiện không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien.BienKiemSoat.Length > 32)
                                {
                                    errors.Add(new ValidationResult("Biển kiểm soát phương tiện thực hiện không được vượt quá 32 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien.NhanHieu))
                                {
                                    errors.Add(new ValidationResult("Nhãn hiệu phương tiện thực hiện không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien.NhanHieu.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Nhãn hiệu phương tiện thực hiện không được vượt quá 64 ký tự!"));
                                }
                            }
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.DanhSachLaiXe == null)
                        {
                            errors.Add(new ValidationResult("Danh sách lái xe không được bỏ trống!"));
                        }
                        else if (this.DuLieu.NoiDungLenhVanChuyen.DanhSachLaiXe.Count == 0)
                        {
                            errors.Add(new ValidationResult("Danh sách lái xe không được bỏ trống!"));
                        }
                        else
                        {
                            if (!this.DuLieu.NoiDungLenhVanChuyen.DanhSachLaiXe.Any(x => x.STT == 1))
                            {
                                errors.Add(new ValidationResult("Số thứ tự lái xe không hợp lệ!"));
                            }
                            foreach (var laiXe in this.DuLieu.NoiDungLenhVanChuyen.DanhSachLaiXe)
                            {
                                if (string.IsNullOrWhiteSpace(laiXe.HoTen))
                                {
                                    errors.Add(new ValidationResult("Họ tên lái xe không được bỏ trống!"));
                                }
                                else if (laiXe.HoTen.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Họ tên lái xe không được vượt quá 128 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(laiXe.SoGiayPhepLaiXe))
                                {
                                    errors.Add(new ValidationResult("Số giấy phép lái xe không được bỏ trống!"));
                                }
                                else if (laiXe.SoGiayPhepLaiXe.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Số giấy phép lái xe không được vượt quá 64 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(laiXe.HangGiayPhepLaiXe))
                                {
                                    errors.Add(new ValidationResult("Hạng giấy phép lái xe không được bỏ trống!"));
                                }
                                else if (laiXe.HangGiayPhepLaiXe.Length > 16)
                                {
                                    errors.Add(new ValidationResult("Hạng giấy phép lái xe không được vượt quá 16 ký tự!"));
                                }
                            }
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.DanhSachNhanVienPhucVu != null)
                        {
                            foreach (var nhanVienPhucVu in this.DuLieu.NoiDungLenhVanChuyen.DanhSachNhanVienPhucVu)
                            {
                                if (!string.IsNullOrWhiteSpace(nhanVienPhucVu.HoTen) && nhanVienPhucVu.HoTen.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Họ tên nhân viên phục vụ không được vượt quá 128 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(nhanVienPhucVu.LoaiGiayToTuyThan) && nhanVienPhucVu.LoaiGiayToTuyThan.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Loại giấy tờ tùy thân không được vượt quá 64 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(nhanVienPhucVu.MaSoGiayToTuyThan) && nhanVienPhucVu.MaSoGiayToTuyThan.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Mã số giấy tờ tùy thân không được vượt quá 64 ký tự!"));
                                }
                            }
                        }

                        if (this.DuLieu.NoiDungLenhVanChuyen.ThongTinKhac != null)
                        {
                            foreach (var thongTinKhac in this.DuLieu.NoiDungLenhVanChuyen.ThongTinKhac)
                            {
                                if (!string.IsNullOrWhiteSpace(thongTinKhac.TenTruong) && thongTinKhac.TenTruong.Length > 256)
                                {
                                    errors.Add(new ValidationResult("Tên trường thông tin khác không được vượt quá 256 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(thongTinKhac.KieuDuLieu) && thongTinKhac.KieuDuLieu.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Kiểu dữ liệu thông tin khác không được vượt quá 128 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(thongTinKhac.DuLieu) && thongTinKhac.DuLieu.Length > 2048)
                                {
                                    errors.Add(new ValidationResult("Dữ liệu thông tin khác không được vượt quá 2048 ký tự!"));
                                }
                            }
                        }

                        if (this.DuLieu.NoiDungBenDiXacNhan == null)
                        {
                            errors.Add(new ValidationResult("Nội dung bến đi xác nhận không được bỏ trống!"));
                        }
                        else
                        {
                            if (this.DuLieu.NoiDungBenDiXacNhan.ThoiGianVaoBen == null)
                            {
                                errors.Add(new ValidationResult("Thời gian vào bến không hợp lệ!"));
                            }
                            else if (this.DuLieu.NoiDungBenDiXacNhan.ThoiGianVaoBen.Date == thoiGianKhongHopLe)
                            {
                                errors.Add(new ValidationResult("Thời gian vào bến không hợp lệ!"));
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.GioXuatBenThucTe == null)
                            {
                                errors.Add(new ValidationResult("Giờ xuất bến thực tế không hợp lệ!"));
                            }
                            else if (this.DuLieu.NoiDungBenDiXacNhan.GioXuatBenThucTe.Date == thoiGianKhongHopLe)
                            {
                                errors.Add(new ValidationResult("Giờ xuất bến thực tế không hợp lệ!"));
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.HanPhuHieu == null)
                            {
                                errors.Add(new ValidationResult("Hạn phù hiệu không hợp lệ!"));
                            }
                            else if (this.DuLieu.NoiDungBenDiXacNhan.HanPhuHieu.Date == thoiGianKhongHopLe)
                            {
                                errors.Add(new ValidationResult("Hạn phù hiệu không hợp lệ!"));
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.HanDangKiem == null)
                            {
                                errors.Add(new ValidationResult("Hạn đăng kiểm không hợp lệ!"));
                            }
                            else if (this.DuLieu.NoiDungBenDiXacNhan.HanDangKiem.Date == thoiGianKhongHopLe)
                            {
                                errors.Add(new ValidationResult("Hạn đăng kiểm không hợp lệ!"));
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.HanBaoHiem == null)
                            {
                                errors.Add(new ValidationResult("Hạn bảo hiểm không hợp lệ!"));
                            }
                            else if (this.DuLieu.NoiDungBenDiXacNhan.HanBaoHiem.Date == thoiGianKhongHopLe)
                            {
                                errors.Add(new ValidationResult("Hạn bảo hiểm không hợp lệ!"));
                            }

                            if (!this.DuLieu.NoiDungBenDiXacNhan.LenhDuDieuKien && this.DuLieu.NoiDungBenDiXacNhan.LyDoKhongDuDieuKienXuatBen == null)
                            {
                                errors.Add(new ValidationResult("Lý do không đủ điều kiện xuất bến không được bỏ trống!"));
                            }
                            else if (!this.DuLieu.NoiDungBenDiXacNhan.LenhDuDieuKien && this.DuLieu.NoiDungBenDiXacNhan.LyDoKhongDuDieuKienXuatBen.Count == 0)
                            {
                                errors.Add(new ValidationResult("Lý do không đủ điều kiện xuất bến không được bỏ trống!"));
                            }
                            else if (!this.DuLieu.NoiDungBenDiXacNhan.LenhDuDieuKien)
                            {
                                foreach (var lyDo in this.DuLieu.NoiDungBenDiXacNhan.LyDoKhongDuDieuKienXuatBen)
                                {
                                    if (!string.IsNullOrWhiteSpace(lyDo.Ma) && lyDo.Ma.Length > 64)
                                    {
                                        errors.Add(new ValidationResult("Mã không đủ điều kiện không được vượt quá 64 ký tự!"));
                                    }

                                    if (!string.IsNullOrWhiteSpace(lyDo.NoiDung) && lyDo.NoiDung.Length > 512)
                                    {
                                        errors.Add(new ValidationResult("Nội dung lý do không đủ điều kiện không được vượt quá 512 ký tự!"));
                                    }
                                }
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh == null)
                            {
                                errors.Add(new ValidationResult("Lái xe xác nhận lệnh không được bỏ trống!"));
                            }
                            else
                            {
                                if (this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.ThoiGianXacNhan == null)
                                {
                                    errors.Add(new ValidationResult("Thời gian xác nhận thực hiện lệnh vận chuyển không hợp lệ!"));
                                }
                                else if (this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.ThoiGianXacNhan.Date == thoiGianKhongHopLe)
                                {
                                    errors.Add(new ValidationResult("Thời gian xác nhận thực hiện lệnh vận chuyển không hợp lệ!"));
                                }

                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.SoGiayPhepLaiXe))
                                {
                                    errors.Add(new ValidationResult("Số giấy phép lái xe xác nhận lệnh không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.SoGiayPhepLaiXe.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Số giấy phép lái xe xác nhận lệnh không được vượt quá 64 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.HoTen))
                                {
                                    errors.Add(new ValidationResult("Họ tên lái xe xác nhận lệnh không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.HoTen.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Họ tên lái xe xác nhận lệnh không được vượt quá 128 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.ToaDoGPS) && this.DuLieu.NoiDungBenDiXacNhan.LaiXeXacNhanLenh.ToaDoGPS.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Tọa độ GPS lái xe xác nhận lệnh không được vượt quá 128 ký tự!"));
                                }
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh != null)
                            {
                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.MaDinhDanh))
                                {
                                    errors.Add(new ValidationResult("Mã định danh nhân viên ký lệnh không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.MaDinhDanh.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Mã định danh nhân viên ký lệnh không được vượt quá 64 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.HoTen))
                                {
                                    errors.Add(new ValidationResult("Họ tên nhân viên ký lệnh không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.HoTen.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Họ tên nhân viên ký lệnh không được vượt quá 128 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.LoaiGiayToTuyThan) && this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.LoaiGiayToTuyThan.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Loại giấy tờ tùy thân nhân viên ký lệnh không được vượt quá 64 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.MaSoGiayToTuyThan) && this.DuLieu.NoiDungBenDiXacNhan.NhanVienKyLenh.MaSoGiayToTuyThan.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Mã số giấy tờ tùy thân nhân viên ký lệnh không được vượt quá 64 ký tự!"));
                                }
                            }

                            if (this.DuLieu.NoiDungBenDiXacNhan.ThongTinKhac != null)
                            {
                                foreach (var thongTinKhac in this.DuLieu.NoiDungBenDiXacNhan.ThongTinKhac)
                                {
                                    if (!string.IsNullOrWhiteSpace(thongTinKhac.TenTruong) && thongTinKhac.TenTruong.Length > 256)
                                    {
                                        errors.Add(new ValidationResult("Tên trường thông tin khác không được vượt quá 256 ký tự!"));
                                    }

                                    if (!string.IsNullOrWhiteSpace(thongTinKhac.KieuDuLieu) && thongTinKhac.KieuDuLieu.Length > 128)
                                    {
                                        errors.Add(new ValidationResult("Kiểu dữ liệu thông tin khác không được vượt quá 128 ký tự!"));
                                    }

                                    if (!string.IsNullOrWhiteSpace(thongTinKhac.DuLieu) && thongTinKhac.DuLieu.Length > 2048)
                                    {
                                        errors.Add(new ValidationResult("Dữ liệu thông tin khác không được vượt quá 2048 ký tự!"));
                                    }
                                }
                            }
                        }

                        if (this.DuLieu.NoiDungBenDenXacNhan != null)
                        {
                            if (this.DuLieu.NoiDungBenDenXacNhan.ThoiGianXacNhanTraKhach == null)
                            {
                                errors.Add(new ValidationResult("Thời gian xác nhận trả khách không hợp lệ!"));
                            }
                            else if (this.DuLieu.NoiDungBenDenXacNhan.ThoiGianXacNhanTraKhach.Date == thoiGianKhongHopLe)
                            {
                                errors.Add(new ValidationResult("Thời gian xác nhận trả khách không hợp lệ!"));
                            }

                            if (this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh != null)
                            {
                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.MaDinhDanh))
                                {
                                    errors.Add(new ValidationResult("Mã định danh nhân viên ký lệnh không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.MaDinhDanh.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Mã định danh nhân viên ký lệnh không được vượt quá 64 ký tự!"));
                                }

                                if (string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.HoTen))
                                {
                                    errors.Add(new ValidationResult("Họ tên nhân viên ký lệnh không được bỏ trống!"));
                                }
                                else if (this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.HoTen.Length > 128)
                                {
                                    errors.Add(new ValidationResult("Họ tên nhân viên ký lệnh không được vượt quá 128 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.LoaiGiayToTuyThan) && this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.LoaiGiayToTuyThan.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Loại giấy tờ tùy thân nhân viên ký lệnh không được vượt quá 64 ký tự!"));
                                }

                                if (!string.IsNullOrWhiteSpace(this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.MaSoGiayToTuyThan) && this.DuLieu.NoiDungBenDenXacNhan.NhanVienKyLenh.MaSoGiayToTuyThan.Length > 64)
                                {
                                    errors.Add(new ValidationResult("Mã số giấy tờ tùy thân nhân viên ký lệnh không được vượt quá 64 ký tự!"));
                                }
                            }

                            if (this.DuLieu.NoiDungBenDenXacNhan.ThongTinKhac != null)
                            {
                                foreach (var thongTinKhac in this.DuLieu.NoiDungBenDenXacNhan.ThongTinKhac)
                                {
                                    if (!string.IsNullOrWhiteSpace(thongTinKhac.TenTruong) && thongTinKhac.TenTruong.Length > 256)
                                    {
                                        errors.Add(new ValidationResult("Tên trường thông tin khác không được vượt quá 256 ký tự!"));
                                    }

                                    if (!string.IsNullOrWhiteSpace(thongTinKhac.KieuDuLieu) && thongTinKhac.KieuDuLieu.Length > 128)
                                    {
                                        errors.Add(new ValidationResult("Kiểu dữ liệu thông tin khác không được vượt quá 128 ký tự!"));
                                    }

                                    if (!string.IsNullOrWhiteSpace(thongTinKhac.DuLieu) && thongTinKhac.DuLieu.Length > 2048)
                                    {
                                        errors.Add(new ValidationResult("Dữ liệu thông tin khác không được vượt quá 2048 ký tự!"));
                                    }
                                }
                            }


                        }
                    }
                }
            }
            return errors;
        }
    }
}
