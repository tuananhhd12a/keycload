﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan
{
    public class NoiDungBenDenXacNhan
    {
        public DateTime ThoiGianVaoBen { get; set; }

        public DateTime ThoiGianXacNhanTraKhach { get; set; }

        public int SoKhachKhiKyLenh { get; set; }

        public NhanVienKyLenh NhanVienKyLenh { get; set; }

        public List<ThongTin> ThongTinKhac { get; set; }
    }
}
