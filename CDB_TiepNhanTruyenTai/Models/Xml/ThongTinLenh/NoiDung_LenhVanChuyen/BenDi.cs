﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class BenDi
    {
        public string MaSoGTVT { get; set; }

        public string TenSoGTVT { get; set; }

        public string MaBenXeNoiDi { get; set; }

        public string TenBenNoiDi { get; set; }
    }
}
