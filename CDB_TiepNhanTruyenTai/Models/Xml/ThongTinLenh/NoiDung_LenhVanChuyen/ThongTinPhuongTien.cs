﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class ThongTinPhuongTien
    {
        public PhuongTienTheoKeHoach PhuongTienTheoKeHoach { get; set; }

        public PhuongTienThucHien PhuongTienThucHien { get; set; }
    }
}
