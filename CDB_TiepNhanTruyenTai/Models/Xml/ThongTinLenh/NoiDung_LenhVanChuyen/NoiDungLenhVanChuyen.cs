﻿using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class NoiDungLenhVanChuyen
    {
        public ThongTinDoanhNghiep ThongTinDoanhNghiep { get; set; }

        public ThongTinChuyenXe ThongTinChuyenXe { get; set; }

        public TuyenVanChuyen TuyenVanChuyen { get; set; }

        public ThongTinPhuongTien ThongTinPhuongTien { get; set; }

        public List<LaiXe> DanhSachLaiXe { get; set; }

        public List<NhanVienPhucVu> DanhSachNhanVienPhucVu { get; set; }

        public List<ThongTin> ThongTinKhac { get; set; }

    }
}
