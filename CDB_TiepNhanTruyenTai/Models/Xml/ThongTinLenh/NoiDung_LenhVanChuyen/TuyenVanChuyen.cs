﻿using System;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class TuyenVanChuyen
    {
        public string MaSoTuyen { get; set; }

        public string TenTuyen { get; set; }

        public float CuLyVanChuyen { get; set; }

        public string HanhTrinhTuyen { get; set; }

        public BenDi BenDi { get; set; }

        public BenDen BenDen { get; set; }
    }
}
