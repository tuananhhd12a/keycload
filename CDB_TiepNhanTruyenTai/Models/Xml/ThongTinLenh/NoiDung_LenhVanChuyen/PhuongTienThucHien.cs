﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class PhuongTienThucHien
    {
        public string BienKiemSoat { get; set; }

        public int SoCho { get; set; }

        public string NhanHieu { get; set; }
    }
}
