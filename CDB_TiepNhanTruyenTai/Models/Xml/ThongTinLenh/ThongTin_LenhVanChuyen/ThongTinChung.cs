﻿using CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen
{
    public class ThongTinChung
    {
        public string PhienBan { get; set; }

        public string MaSoThueDonViTruyenNhan { get; set; }

        public string TenDonViTruyenNhan { get; set; }

        public string MaLenhVanChuyen { get; set; }

        public string MaLenhVanChuyenBiThayThe { get; set; }

        public string KyHieuMauSoLenh { get; set; }

        public string TenTaiLieu { get; set; }

        public int MaLoaiLenh { get; set; }

        public string KyHieuLenh { get; set; }

        public string SoLenh { get; set; }

        public DateTime NgayKhoiTao { get; set; }

        public List<ThongTin> ThongTinKhac { get; set; }
    }
}
