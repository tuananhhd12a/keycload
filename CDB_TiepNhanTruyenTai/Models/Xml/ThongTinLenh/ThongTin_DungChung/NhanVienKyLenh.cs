﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung
{
    public class NhanVienKyLenh
    {
        public string MaDinhDanh { get; set; }

        public string HoTen { get; set; }

        public string LoaiGiayToTuyThan { get; set; }

        public string MaSoGiayToTuyThan { get; set; }
    }
}
