﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung
{
    public class ThongTin
    {
        public string TenTruong { get; set; }

        public string KieuDuLieu { get; set; }

        public string DuLieu { get; set; }
    }
}
