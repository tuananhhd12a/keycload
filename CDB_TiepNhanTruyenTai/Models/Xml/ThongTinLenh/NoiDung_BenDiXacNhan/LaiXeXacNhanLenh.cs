﻿using System;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan
{
    public class LaiXeXacNhanLenh
    {
        public int Stt { get; set; }

        public DateTime ThoiGianXacNhan { get; set; }

        public string SoGiayPhepLaiXe { get; set; }

        public string HoTen { get; set; }

        public string ToaDoGPS { get; set; }
    }
}
