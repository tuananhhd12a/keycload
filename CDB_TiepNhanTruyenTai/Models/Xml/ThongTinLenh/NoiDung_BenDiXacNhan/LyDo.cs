﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan
{
    public class LyDo
    {
        public int STT { get; set; }

        public string Ma { get; set; }

        public string NoiDung { get; set; }
    }
}
