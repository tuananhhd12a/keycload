﻿using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan
{
    public class NoiDungBenDiXacNhan
    {
        public DateTime ThoiGianVaoBen { get; set; }

        public DateTime GioXuatBenThucTe { get; set; }

        public int SoKhachKhiKyLenh { get; set; }

        public bool LenhDuDieuKien { get; set; }

        public DateTime HanPhuHieu { get; set; }

        public DateTime HanDangKiem { get; set; }

        public DateTime HanBaoHiem { get; set; }

        public List<LyDo> LyDoKhongDuDieuKienXuatBen { get; set; }

        public LaiXeXacNhanLenh LaiXeXacNhanLenh { get; set; }

        public NhanVienKyLenh NhanVienKyLenh { get; set; }

        public List<ThongTin> ThongTinKhac { get; set; }

    }
}
