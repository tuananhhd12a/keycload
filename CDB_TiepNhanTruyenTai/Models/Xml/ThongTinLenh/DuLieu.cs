﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh
{
    [XmlRoot(ElementName = "DuLieu")]
    public class DuLieu
    {
        [XmlElement(ElementName = "ThongTinChung")]
        public ThongTinChung ThongTinChung { get; set; }

        [XmlElement(ElementName = "NoiDungLenhVanChuyen")]
        public NoiDungLenhVanChuyen NoiDungLenhVanChuyen { get; set; }

        [XmlElement(ElementName = "NoiDungBenDiXacNhan")]
        public NoiDungBenDiXacNhan NoiDungBenDiXacNhan { get; set; }

        [XmlElement(ElementName = "NoiDungBenDenXacNhan")]
        public NoiDungBenDenXacNhan NoiDungBenDenXacNhan { get; set; }
    }
}
