﻿using System.Xml.Serialization;
using CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo
{
    public class BenDi
    {
        public Signature Signature { get; set; }
    }
}
