﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo
{
    public class DanhSachChuKySo
    {
        public DoanhNghiep DoanhNghiep { get; set; }

        public LaiXe LaiXe { get; set; }

        public NhanVienPhucVu NhanVienPhucVu { get; set; }

        public BenDi BenDi { get; set; }

        public BenDen BenDen { get; set; }
    }
}
