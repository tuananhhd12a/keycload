﻿using CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo
{
    public class BenDen
    {
        public Signature Signature { get; set; }
    }
}
