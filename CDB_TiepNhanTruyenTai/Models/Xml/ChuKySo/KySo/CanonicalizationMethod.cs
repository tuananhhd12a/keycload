﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo
{
    [XmlRoot(ElementName = "CanonicalizationMethod")]
    public class CanonicalizationMethod
    {

        [XmlAttribute(AttributeName = "Algorithm")]
        public string Algorithm { get; set; }
    }
}
