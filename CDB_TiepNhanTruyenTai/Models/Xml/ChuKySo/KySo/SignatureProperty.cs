﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo
{
    [XmlRoot(ElementName = "SignatureProperty")]
    public class SignatureProperty
    {
        [XmlElement(ElementName = "SigningTime")]
        public string SigningTime { get; set; }

        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "Target")]
        public string Target { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
}
