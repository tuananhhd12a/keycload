﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo
{
    [XmlRoot(ElementName = "Signature")]
    public class Signature
    {
        [XmlElement(ElementName = "SignedInfo")]
        public SignedInfo SignedInfo { get; set; }

        [XmlElement(ElementName = "SignatureValue")]
        public string SignatureValue { get; set; }

        [XmlElement(ElementName = "KeyInfo")]
        public KeyInfo KeyInfo { get; set; }

        [XmlElement(ElementName = "Object")]
        public Object Object { get; set; }

        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
    }
}
