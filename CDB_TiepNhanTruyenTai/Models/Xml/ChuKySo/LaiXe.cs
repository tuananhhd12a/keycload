﻿using CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo.KySo;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo
{
    public class LaiXe
    {
        public int STT { get; set; }

        public Signature Signature { get; set; }
    }
}
