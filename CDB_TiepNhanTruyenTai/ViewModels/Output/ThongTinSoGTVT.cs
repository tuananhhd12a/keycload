﻿namespace CDB_TiepNhanTruyenTai.ViewModels.Output
{
    public class ThongTinSoGTVT
    {
        public string MaSoGTVT { get; set; }
        public string TenSoGTVT { get; set; }
    }
}
