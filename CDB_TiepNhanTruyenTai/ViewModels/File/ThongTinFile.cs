﻿namespace CDB_TiepNhanTruyenTai.ViewModels.File
{
    public class ThongTinFile
    {
        public byte[] Data { get; set; }
        public string Loai { get; set; }
    }
}
