﻿using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Mvc;
using CDB_TiepNhanTruyenTai.Models.Output;
using Microsoft.AspNetCore.Authorization;
using Lib_Core.Enum;

namespace CDB_TiepNhanTruyenTai.Controllers
{
    public partial class TiepNhanTruyenTaiController
    {
        [ProducesResponseType(200, Type = typeof(DataResponsive<PhanHoiTiepNhanLenh>))]
        [HttpPost("doanh-nghiep-truyen-tai-lenh-van-chuyen-ben-xe-di-da-xac-minh")]
        [Authorize(Roles = EPermission.DOANH_NGHIEP_TRUYEN_TAI_XUAT_BEN)]
        public IActionResult DoanhNghiep_TruyenTaiXuatBen([FromBody] Models.Input.TiepNhanTruyenTai tiepNhanTruyenTai)
        {
            return Ok(truyenTaiService.DoanhNghiep_TruyenTaiXuatBen(tiepNhanTruyenTai, User.ThongTin()));
        }

        [ProducesResponseType(200, Type = typeof(DataResponsive<PhanHoiTiepNhanLenh>))]
        [HttpPost("doanh-nghiep-truyen-tai-lenh-van-chuyen-ben-xe-den-da-xac-minh")]
        [Authorize(Roles = EPermission.DOANH_NGHIEP_TRUYEN_TAI_DEN_BEN)]
        public IActionResult DoanhNghiep_TruyenTaiDenBen([FromBody] Models.Input.TiepNhanTruyenTai tiepNhanTruyenTai)
        {
            return Ok(truyenTaiService.DoanhNghiep_TruyenTaiDenBen(tiepNhanTruyenTai, User.ThongTin()));
        }

        [ProducesResponseType(200, Type = typeof(DataResponsive<PhanHoiTiepNhanLenh>))]
        [HttpPost("doanh-nghiep-truyen-tai-lenh-van-chuyen-dien-tu-da-ky-cho-xe-buyt")]
        [Authorize(Roles = EPermission.DOANH_NGHIEP_TRUYEN_TAI_XE_BUYT)]
        public IActionResult DoanhNghiep_TruyenTaiXeBuyt([FromBody] Models.Input.TiepNhanTruyenTai tiepNhanTruyenTai)
        {
            return Ok(truyenTaiService.DoanhNghiep_TruyenTaiXeBuyt(tiepNhanTruyenTai, User.ThongTin()));
        }

        [ProducesResponseType(200, Type = typeof(DataResponsive<bool>))]
        [HttpPost("doanh-nghiep-truyen-tai-tren-duong-lenh-van-chuyen-dien-tu")]
        [Authorize(Roles = EPermission.DOANH_NGHIEP_TRUYEN_TAI_TREN_DUONG)]
        public IActionResult DoanhNghiep_TruyenTaiLenhDangHoatDong([FromBody] Models.Input.TruyenTaiDuLieuDangHoatDong truyenTaiDuLieuDangHoatDong)
        {
            return Ok(truyenTaiService.DoanhNghiep_TruyenTaiLenhDangHoatDong(truyenTaiDuLieuDangHoatDong, User.ThongTin()));
        }

        [ProducesResponseType(200, Type = typeof(DataResponsive<PhanHoiTiepNhanLenh>))]
        [HttpPost("doanh-nghiep-truyen-tai-lenh-van-chuyen-dien-tu-thay-the-da-ky")]
        [Authorize(Roles = EPermission.DOANH_NGHIEP_TRUYEN_TAI_LENH_THAY_THE)]
        public IActionResult DoanhNghiep_TruyenTaiLenhThayThe([FromBody] Models.Input.TruyenTaiDuLieuLenhThayThe truyenTaiDuLieuLenhThayThe)
        {
            return Ok(truyenTaiService.DoanhNghiep_TruyenTaiLenhThayThe(truyenTaiDuLieuLenhThayThe, User.ThongTin()));
        }
    }
}
