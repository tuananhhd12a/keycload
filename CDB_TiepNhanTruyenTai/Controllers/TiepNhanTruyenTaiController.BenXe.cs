﻿using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Mvc;
using System;
using CDB_TiepNhanTruyenTai.Models.Output;
using Microsoft.AspNetCore.Authorization;
using Lib_Core.Enum;

namespace CDB_TiepNhanTruyenTai.Controllers
{
    public partial class TiepNhanTruyenTaiController
    {
        [ProducesResponseType(200, Type = typeof(DataResponsive<PhanHoiTiepNhanLenh>))]
        [HttpPost("ben-xe-di-truyen-tai-lenh-van-chuyen-dien-tu-da-ky")]
        [Authorize(Roles = EPermission.BEN_XE_TRUYEN_TAI_XUAT_BEN)]
        public IActionResult BenXe_TruyenTaiXuatBen([FromBody] Models.Input.TiepNhanTruyenTai tiepNhanTruyenTai)
        {
            return Ok(truyenTaiService.BenXe_TruyenTaiXuatBen(tiepNhanTruyenTai, User.ThongTin()));
        }

        [ProducesResponseType(200, Type = typeof(DataResponsive<PhanHoiTiepNhanLenh>))]
        [HttpPost("ben-xe-den-truyen-tai-lenh-van-chuyen-dien-tu-da-ky")]
        [Authorize(Roles = EPermission.BEN_XE_TRUYEN_TAI_DEN_BEN)]
        public IActionResult BenXe_TruyenTaiDenBen([FromBody] Models.Input.TiepNhanTruyenTai tiepNhanTruyenTai)
        {
            return Ok(truyenTaiService.BenXe_TruyenTaiDenBen(tiepNhanTruyenTai, User.ThongTin()));
        }
    }
}
