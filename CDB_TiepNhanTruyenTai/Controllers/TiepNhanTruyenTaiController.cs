﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CDB_TiepNhanTruyenTai.Services;

namespace CDB_TiepNhanTruyenTai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class TiepNhanTruyenTaiController : ControllerBase
    {
        private readonly TiepNhanTruyenTaiService truyenTaiService;

        public TiepNhanTruyenTaiController(TiepNhanTruyenTaiService truyenTaiService)
        {
            this.truyenTaiService = truyenTaiService;
        }
    }
}
