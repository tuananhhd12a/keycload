﻿using Lib_Core.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CDB_TiepNhanTruyenTai.Controllers
{
    public partial class TiepNhanTruyenTaiController
    {
        [HttpGet("lay-file")]
        [Authorize(Roles = EPermission.XEM_FILE)]
        public IActionResult LayFile([FromQuery] string path)
        {
            try
            {
                var file = truyenTaiService.LayFile(path, out string contentType);
                if (file == null)
                {
                    return NotFound();
                }
                return File(file, contentType);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
