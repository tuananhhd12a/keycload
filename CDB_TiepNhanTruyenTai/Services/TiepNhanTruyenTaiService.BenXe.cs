﻿using CDB_TiepNhanTruyenTai.Models.Input;
using CDB_TiepNhanTruyenTai.ViewModels.File;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.JWT;
using Lib_Core.Objects.Mongo.TruyenTai;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        public DataResponsive<string> BenXe_TruyenTaiXuatBen(TiepNhanTruyenTai tiepNhanTruyenTai, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Tiếp nhận bến xe truyền tải xuất bến - MaLenh={tiepNhanTruyenTai.MaLenh} - CheckSum={tiepNhanTruyenTai.Checksum}");
                //TODO - Chưa kiểm tra chữ ký

                var dataXml = FileExtension.GetBytes(tiepNhanTruyenTai.XML, out string errorXml);

                var noiDungTiepNhan = new TiepNhanTruyenTai()
                {
                    MaLenh = tiepNhanTruyenTai.MaLenh,
                    Checksum = tiepNhanTruyenTai.Checksum,
                };
                var truyenTai = new CDB_LenhVanChuyen
                {
                    _id = Guid.NewGuid(),
                    MaLenhCapPhat = Guid.Empty,
                    MaLenhTiepNhan = tiepNhanTruyenTai.MaLenh,
                    IdDoiTuongTruyenTai = EDoiTuongTruyenTai.BEN_XE.Id,
                    TenDoiTuongTruyenTai = EDoiTuongTruyenTai.BEN_XE.Name,
                    IdLoaiTruyenTai = ELoaiTruyenTai.XUAT_BEN.Id,
                    TenLoaiTruyenTai = ELoaiTruyenTai.XUAT_BEN.Name,
                    ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan,
                    HoTen_NguoiTruyenTai = taiKhoan.HoTen,
                    NgayTruyenTai = Common.nowWithTimeLocal.Date,
                    ThoiGianTruyenTai = Common.now,
                    MaLoi = EMaLoi.THANH_CONG.Id,
                    ThongTinLoi = EMaLoi.THANH_CONG.Name,
                    DuLieuTiepNhan = noiDungTiepNhan.Object2Json(),
                    MaSoThueDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                    MaSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                };

                if (!string.IsNullOrWhiteSpace(errorXml))
                {
                    truyenTai.MaLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải xuất bến không thành công do file xml không hợp lệ - {errorXml}");
                    return new DataResponsive<string>().returnData("", EMaLoi.FILE_XML_KHONG_HOP_LE, errorXml);
                }

                var xml = Xml2Object(dataXml);
                if (!xml.status)
                {
                    truyenTai.MaLoi = EMaLoi.XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = xml.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải xuất bến không thành công do xml không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.XML_KHONG_HOP_LE, xml.message);
                }

                //Kiểm tra chữ ký của đơn vị truyền nhận
                var kiemTraChuKy = VerifyCheckSum(xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan, tiepNhanTruyenTai, tiepNhanTruyenTai.Checksum);
                if (!kiemTraChuKy.data)
                {
                    truyenTai.MaLoi = kiemTraChuKy.errorCode;
                    truyenTai.ThongTinLoi = kiemTraChuKy.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    return new DataResponsive<string>().returnData("", kiemTraChuKy.errorCode, kiemTraChuKy.message);
                }

                //Kiểm tra chữ ký của doanh nghiệp
                var kiemTraDoanhNghiepKySo = CheckSign(dataXml, EDoiTuongKiemTra.DOANH_NGHIEP, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, "");
                if (!kiemTraDoanhNghiepKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraDoanhNghiepKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải xuất bến không thành công do doanh nghiệp ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraDoanhNghiepKySo.message);
                }

                //Kiểm tra chữ ký của bến đi
                var kiemTraBenDiKySo = CheckSign(dataXml, EDoiTuongKiemTra.BEN_DI, xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT, "", xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaBenXeNoiDi);
                if (!kiemTraBenDiKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraBenDiKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải xuất bến không thành công do bến đi ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraBenDiKySo.message);
                }

                //Lưu file
                var tepDinhKems = new List<ThongTinFile>();
                string errorMessage = "";
                if (tiepNhanTruyenTai.DanhSachTepDinhKem != null && tiepNhanTruyenTai.DanhSachTepDinhKem.Count > 0)
                {
                    foreach (var tep in tiepNhanTruyenTai.DanhSachTepDinhKem)
                    {
                        var data = FileExtension.GetBytes(tep.Data, out errorMessage);
                        if (!string.IsNullOrWhiteSpace(errorMessage))
                        {
                            break;
                        }
                        tepDinhKems.Add(new ThongTinFile()
                        {
                            Data = data,
                            Loai = tep.Loai,
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    truyenTai.MaLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải xuất bến không thành công do tệp đính kèm không hợp lệ - {errorMessage}");
                    return new DataResponsive<string>().returnData("", EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE, errorMessage);
                }

                var mstDonViTruyenNhan = xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan;
                truyenTai.FileXml = LuuFile(mstDonViTruyenNhan, dataXml, EContentType.Xml);
                truyenTai.DanhSachFileDinhKem = tepDinhKems.Select(x => LuuFile(mstDonViTruyenNhan, x.Data, x.Loai)).ToList();

                var lenhTruyenTai = MapLenhVanChuyen(xml.data, truyenTai);

                //Cập nhật tên sở GTVT
                var soGTVT = LaySoGTVT(lenhTruyenTai.MaSoDonViTruyenTai);
                if (!soGTVT.status)
                {
                    lenhTruyenTai.MaLoi = soGTVT.errorCode;
                    lenhTruyenTai.ThongTinLoi = soGTVT.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy thông tin sở GTVT đơn vị truyền tải lỗi");
                    return new DataResponsive<string>().returnData("", soGTVT.errorCode, soGTVT.message);
                }
                lenhTruyenTai.TenSoDonViTruyenTai = soGTVT.data.TenSoGTVT;

                //Kiểm tra xem mã lệnh đã được cấp phát chưa
                var lenhCapPhat = LayLenhCapPhat(lenhTruyenTai.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, lenhTruyenTai.MaLenhTiepNhan, taiKhoan);
                if (!lenhCapPhat.status)
                {
                    lenhTruyenTai.MaLoi = lenhCapPhat.errorCode;
                    lenhTruyenTai.ThongTinLoi = lenhCapPhat.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy lệnh cấp phát không thành công");
                    return new DataResponsive<string>().returnData("", lenhCapPhat.errorCode, lenhCapPhat.message);
                }
                lenhTruyenTai.MaLenhCapPhat = lenhCapPhat.data;

                dbTruyenTai.InsertOne(lenhTruyenTai);
                logger.LogInformation($"Tiếp nhận bến xe truyền tải xuất bến thành công");
                return new DataResponsive<string>().returnData(lenhTruyenTai.MaLenhCapPhat.ToString().ToLower(), EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Tiếp nhận bến xe truyền tải xuất bến gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<string> BenXe_TruyenTaiDenBen(TiepNhanTruyenTai tiepNhanTruyenTai, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Tiếp nhận bến xe truyền tải đến bến - MaLenh={tiepNhanTruyenTai.MaLenh} - CheckSum={tiepNhanTruyenTai.Checksum}");
                //TODO - Chưa kiểm tra chữ ký

                var dataXml = FileExtension.GetBytes(tiepNhanTruyenTai.XML, out string errorXml);

                var noiDungTiepNhan = new TiepNhanTruyenTai()
                {
                    MaLenh = tiepNhanTruyenTai.MaLenh,
                    Checksum = tiepNhanTruyenTai.Checksum,
                };
                var truyenTai = new CDB_LenhVanChuyen
                {
                    _id = Guid.NewGuid(),
                    MaLenhCapPhat = Guid.Empty,
                    MaLenhTiepNhan = tiepNhanTruyenTai.MaLenh,
                    IdDoiTuongTruyenTai = EDoiTuongTruyenTai.BEN_XE.Id,
                    TenDoiTuongTruyenTai = EDoiTuongTruyenTai.BEN_XE.Name,
                    IdLoaiTruyenTai = ELoaiTruyenTai.DEN_BEN.Id,
                    TenLoaiTruyenTai = ELoaiTruyenTai.DEN_BEN.Name,
                    ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan,
                    HoTen_NguoiTruyenTai = taiKhoan.HoTen,
                    NgayTruyenTai = Common.nowWithTimeLocal.Date,
                    ThoiGianTruyenTai = Common.now,
                    MaLoi = EMaLoi.THANH_CONG.Id,
                    ThongTinLoi = EMaLoi.THANH_CONG.Name,
                    DuLieuTiepNhan = noiDungTiepNhan.Object2Json(),
                    MaSoThueDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                    MaSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                };
                if (!string.IsNullOrWhiteSpace(errorXml))
                {
                    truyenTai.MaLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = errorXml;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải đến bến không thành công do xml không hợp lệ - {errorXml}");
                    return new DataResponsive<string>().returnData("", EMaLoi.FILE_XML_KHONG_HOP_LE, errorXml);
                }

                var xml = Xml2Object(dataXml);
                if (!xml.status)
                {
                    truyenTai.MaLoi = EMaLoi.XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = xml.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải đến bến không thành công do xml không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.XML_KHONG_HOP_LE, xml.message);
                }

                //Kiểm tra chữ ký của đơn vị truyền nhận
                var kiemTraChuKy = VerifyCheckSum(xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan, tiepNhanTruyenTai, tiepNhanTruyenTai.Checksum);
                if (!kiemTraChuKy.data)
                {
                    truyenTai.MaLoi = kiemTraChuKy.errorCode;
                    truyenTai.ThongTinLoi = kiemTraChuKy.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    return new DataResponsive<string>().returnData("", kiemTraChuKy.errorCode, kiemTraChuKy.message);
                }

                //Kiểm tra chữ ký của doanh nghiệp
                var kiemTraDoanhNghiepKySo = CheckSign(dataXml, EDoiTuongKiemTra.DOANH_NGHIEP, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, "");
                if (!kiemTraDoanhNghiepKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraDoanhNghiepKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải đến bến không thành công do doanh nghiệp ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraDoanhNghiepKySo.message);
                }

                //Kiểm tra chữ ký của bến đi
                var kiemTraBenDiKySo = CheckSign(dataXml, EDoiTuongKiemTra.BEN_DI, xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT, "", xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaBenXeNoiDi);
                if (!kiemTraBenDiKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraBenDiKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải đến bến không thành công do bến đi ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraBenDiKySo.message);
                }

                //Kiểm tra chữ ký của bến đi
                var kiemTraBenDenKySo = CheckSign(dataXml, EDoiTuongKiemTra.BEN_DI, xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaSoGTVT, "", xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaBenXeNoiDen);
                if (!kiemTraBenDenKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraBenDenKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải đến bến không thành công do bến đến ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraBenDenKySo.message);
                }

                //Lưu file
                var tepDinhKems = new List<ThongTinFile>();
                string errorMessage = "";
                if (tiepNhanTruyenTai.DanhSachTepDinhKem != null && tiepNhanTruyenTai.DanhSachTepDinhKem.Count > 0)
                {
                    foreach (var tep in tiepNhanTruyenTai.DanhSachTepDinhKem)
                    {
                        var data = FileExtension.GetBytes(tep.Data, out errorMessage);
                        if (!string.IsNullOrWhiteSpace(errorMessage))
                        {
                            break;
                        }
                        tepDinhKems.Add(new ThongTinFile()
                        {
                            Data = data,
                            Loai = tep.Loai,
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    truyenTai.MaLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = errorMessage;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận bến xe truyền tải đến bến không thành công do tệp đính kèm không hợp lệ - {errorMessage}");
                    return new DataResponsive<string>().returnData("", EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id, errorMessage);
                }
                var mstDonViTruyenNhan = xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan;
                truyenTai.FileXml = LuuFile(mstDonViTruyenNhan, dataXml, EContentType.Xml);
                truyenTai.DanhSachFileDinhKem = tepDinhKems.Select(x => LuuFile(mstDonViTruyenNhan, x.Data, x.Loai)).ToList();

                var lenhTruyenTai = MapLenhVanChuyen(xml.data, truyenTai);

                //Cập nhật tên sở GTVT
                var soGTVT = LaySoGTVT(lenhTruyenTai.MaSoDonViTruyenTai);
                if (!soGTVT.status)
                {
                    lenhTruyenTai.MaLoi = soGTVT.errorCode;
                    lenhTruyenTai.ThongTinLoi = soGTVT.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy thông tin sở GTVT đơn vị truyền tải lỗi");
                    return new DataResponsive<string>().returnData("", soGTVT.errorCode, soGTVT.message);
                }
                lenhTruyenTai.TenSoDonViTruyenTai = soGTVT.data.TenSoGTVT;

                //Kiểm tra xem mã lệnh đã được cấp phát chưa
                var lenhCapPhat = LayLenhCapPhat(lenhTruyenTai.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, lenhTruyenTai.MaLenhTiepNhan, taiKhoan);
                if (!lenhCapPhat.status)
                {
                    lenhTruyenTai.MaLoi = lenhCapPhat.errorCode;
                    lenhTruyenTai.ThongTinLoi = lenhCapPhat.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy lệnh cấp phát không thành công");
                    return new DataResponsive<string>().returnData("", lenhCapPhat.errorCode, lenhCapPhat.message);
                }
                lenhTruyenTai.MaLenhCapPhat = lenhCapPhat.data;

                dbTruyenTai.InsertOne(lenhTruyenTai);
                logger.LogInformation($"Tiếp nhận bến xe truyền tải đến bến thành công");
                return new DataResponsive<string>().returnData(lenhTruyenTai.MaLenhCapPhat.ToString().ToLower(), EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Tiếp nhận bến xe truyền tải đến bến gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
