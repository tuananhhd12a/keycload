﻿using CDB_TiepNhanTruyenTai.Models.Input;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.ViewModels.File;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.JWT;
using Lib_Core.Objects.Mongo.TruyenTai;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTin_TruyenTaiTrenDuong;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using Lib_Core.Objects.Responsive;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        public DataResponsive<string> DoanhNghiep_TruyenTaiXuatBen(TiepNhanTruyenTai tiepNhanTruyenTai, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Tiếp nhận doanh nghiệp truyền tải xuất bến - MaLenh={tiepNhanTruyenTai.MaLenh} - CheckSum={tiepNhanTruyenTai.Checksum}");

                var dataXml = FileExtension.GetBytes(tiepNhanTruyenTai.XML, out string errorXml);

                var noiDungTiepNhan = new TiepNhanTruyenTai()
                {
                    MaLenh = tiepNhanTruyenTai.MaLenh,
                    Checksum = tiepNhanTruyenTai.Checksum,
                };

                var truyenTai = new CDB_LenhVanChuyen
                {
                    _id = Guid.NewGuid(),
                    MaLenhCapPhat = Guid.Empty,
                    MaLenhTiepNhan = tiepNhanTruyenTai.MaLenh,
                    IdDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id,
                    TenDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Name,
                    IdLoaiTruyenTai = ELoaiTruyenTai.XUAT_BEN.Id,
                    TenLoaiTruyenTai = ELoaiTruyenTai.XUAT_BEN.Name,
                    ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan,
                    HoTen_NguoiTruyenTai = taiKhoan.HoTen,
                    NgayTruyenTai = Common.nowWithTimeLocal.Date,
                    ThoiGianTruyenTai = Common.now,
                    MaLoi = EMaLoi.THANH_CONG.Id,
                    ThongTinLoi = EMaLoi.THANH_CONG.Name,
                    DuLieuTiepNhan = noiDungTiepNhan.Object2Json(),
                    MaSoThueDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                    MaSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                };
                if (!string.IsNullOrWhiteSpace(errorXml))
                {
                    truyenTai.MaLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xuất bến không thành công do file xml không hợp lệ - {errorXml}");
                    return new DataResponsive<string>().returnData("", EMaLoi.FILE_XML_KHONG_HOP_LE, errorXml);
                }
                var xml = Xml2Object(dataXml);
                if (!xml.status)
                {
                    truyenTai.MaLoi = EMaLoi.XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = xml.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xuất bến không thành công do xml không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.XML_KHONG_HOP_LE, xml.message);
                }

                //Kiểm tra chữ ký của đơn vị truyền nhận
                var kiemTraChuKy = VerifyCheckSum(xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan, tiepNhanTruyenTai, tiepNhanTruyenTai.Checksum);
                if (!kiemTraChuKy.data)
                {
                    truyenTai.MaLoi = kiemTraChuKy.errorCode;
                    truyenTai.ThongTinLoi = kiemTraChuKy.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    return new DataResponsive<string>().returnData("", kiemTraChuKy.errorCode, kiemTraChuKy.message);
                }

                //Kiểm tra chữ ký của doanh nghiệp
                var kiemTraDoanhNghiepKySo = CheckSign(dataXml, EDoiTuongKiemTra.DOANH_NGHIEP, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, "");
                if (!kiemTraDoanhNghiepKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraDoanhNghiepKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xuất bến không thành công do doanh nghiệp ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraDoanhNghiepKySo.message);
                }
                //Kiểm tra chữ ký của bến đi
                var kiemTraBenDiKySo = CheckSign(dataXml, EDoiTuongKiemTra.BEN_DI, xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT, "", xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaBenXeNoiDi);
                if (!kiemTraBenDiKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraBenDiKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xuất bến không thành công do bến đi ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraBenDiKySo.message);
                }

                //Lưu file
                var tepDinhKems = new List<ThongTinFile>();
                string errorMessage = "";
                if (tiepNhanTruyenTai.DanhSachTepDinhKem != null && tiepNhanTruyenTai.DanhSachTepDinhKem.Count > 0)
                {
                    foreach (var tep in tiepNhanTruyenTai.DanhSachTepDinhKem)
                    {
                        var data = FileExtension.GetBytes(tep.Data, out errorMessage);
                        if (!string.IsNullOrWhiteSpace(errorMessage))
                        {
                            break;
                        }
                        tepDinhKems.Add(new ThongTinFile()
                        {
                            Data = data,
                            Loai = tep.Loai,
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    truyenTai.MaLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xuất bến không thành công do tệp đính kèm không hợp lệ - {errorMessage}");
                    return new DataResponsive<string>().returnData("", EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE, errorMessage);
                }
                var mstDonViTruyenNhan = xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan;
                truyenTai.FileXml = LuuFile(mstDonViTruyenNhan, dataXml, EContentType.Xml);
                truyenTai.DanhSachFileDinhKem = tepDinhKems.Select(x => LuuFile(mstDonViTruyenNhan, x.Data, x.Loai)).ToList();

                var lenhTruyenTai = MapLenhVanChuyen(xml.data, truyenTai);

                //Cập nhật tên sở GTVT
                var soGTVT = LaySoGTVT(lenhTruyenTai.MaSoDonViTruyenTai);
                if (!soGTVT.status)
                {
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy thông tin sở GTVT đơn vị truyền tải lỗi");
                    return new DataResponsive<string>().returnData("", soGTVT.errorCode, soGTVT.message);
                }
                lenhTruyenTai.TenSoDonViTruyenTai = soGTVT.data.TenSoGTVT;

                //Kiểm tra xem mã lệnh đã được cấp phát chưa
                var lenhCapPhat = LayLenhCapPhat(lenhTruyenTai.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, lenhTruyenTai.MaLenhTiepNhan, taiKhoan);
                if (!lenhCapPhat.status)
                {
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy lệnh cấp phát không thành công");
                    return new DataResponsive<string>().returnData("", lenhCapPhat.errorCode, lenhCapPhat.message);
                }
                lenhTruyenTai.MaLenhCapPhat = lenhCapPhat.data;

                dbTruyenTai.InsertOne(lenhTruyenTai);

                logger.LogInformation($"Tiếp nhận doanh nghiệp truyền tải xuất bến thành công");
                return new DataResponsive<string>().returnData(lenhTruyenTai.MaLenhCapPhat.ToString().ToLower(), EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Tiếp nhận doanh nghiệp truyền tải xuất bến gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<string> DoanhNghiep_TruyenTaiDenBen(TiepNhanTruyenTai tiepNhanTruyenTai, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Tiếp nhận doanh nghiệp truyền tải đến bến - MaLenh={tiepNhanTruyenTai.MaLenh} - CheckSum={tiepNhanTruyenTai.Checksum}");
                //TODO - Chưa kiểm tra chữ ký

                var dataXml = FileExtension.GetBytes(tiepNhanTruyenTai.XML, out string errorXml);

                var noiDungTiepNhan = new TiepNhanTruyenTai()
                {
                    MaLenh = tiepNhanTruyenTai.MaLenh,
                    Checksum = tiepNhanTruyenTai.Checksum,
                };
                var truyenTai = new CDB_LenhVanChuyen
                {
                    _id = Guid.NewGuid(),
                    MaLenhCapPhat = Guid.Empty,
                    MaLenhTiepNhan = tiepNhanTruyenTai.MaLenh,
                    IdDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id,
                    TenDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Name,
                    IdLoaiTruyenTai = ELoaiTruyenTai.DEN_BEN.Id,
                    TenLoaiTruyenTai = ELoaiTruyenTai.DEN_BEN.Name,
                    ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan,
                    HoTen_NguoiTruyenTai = taiKhoan.HoTen,
                    NgayTruyenTai = Common.nowWithTimeLocal.Date,
                    ThoiGianTruyenTai = Common.now,
                    MaLoi = EMaLoi.THANH_CONG.Id,
                    ThongTinLoi = EMaLoi.THANH_CONG.Name,
                    DuLieuTiepNhan = noiDungTiepNhan.Object2Json(),
                    MaSoThueDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                    MaSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                };
                if (!string.IsNullOrWhiteSpace(errorXml))
                {
                    truyenTai.MaLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải đến bến không thành công do file xml không hợp lệ - {errorXml}");
                    return new DataResponsive<string>().returnData("", EMaLoi.FILE_XML_KHONG_HOP_LE, errorXml);
                }

                var xml = Xml2Object(dataXml);
                if (!xml.status)
                {
                    truyenTai.MaLoi = EMaLoi.XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = xml.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải đến bến không thành công do xml không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.XML_KHONG_HOP_LE, xml.message);
                }

                //Kiểm tra chữ ký của đơn vị truyền nhận
                var kiemTraChuKy = VerifyCheckSum(xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan, tiepNhanTruyenTai, tiepNhanTruyenTai.Checksum);
                if (!kiemTraChuKy.data)
                {
                    truyenTai.MaLoi = kiemTraChuKy.errorCode;
                    truyenTai.ThongTinLoi = kiemTraChuKy.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    return new DataResponsive<string>().returnData("", kiemTraChuKy.errorCode, kiemTraChuKy.message);
                }

                //Kiểm tra chữ ký của doanh nghiệp
                var kiemTraDoanhNghiepKySo = CheckSign(dataXml, EDoiTuongKiemTra.DOANH_NGHIEP, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, "");
                if (!kiemTraDoanhNghiepKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraDoanhNghiepKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải đến bến không thành công do doanh nghiệp ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraDoanhNghiepKySo.message);
                }

                //Kiểm tra chữ ký của bến đi
                var kiemTraBenDiKySo = CheckSign(dataXml, EDoiTuongKiemTra.BEN_DI, xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT, "", xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaBenXeNoiDi);
                if (!kiemTraBenDiKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraBenDiKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải đến bến không thành công do bến đi ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraBenDiKySo.message);
                }

                //Kiểm tra chữ ký của bến đi
                var kiemTraBenDenKySo = CheckSign(dataXml, EDoiTuongKiemTra.BEN_DI, xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaSoGTVT, "", xml.data.DuLieu.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaBenXeNoiDen);
                if (!kiemTraBenDenKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraBenDenKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải đến bến không thành công do bến đến ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraBenDenKySo.message);
                }

                //Lưu file
                var tepDinhKems = new List<ThongTinFile>();
                string errorMessage = "";
                if (tiepNhanTruyenTai.DanhSachTepDinhKem != null && tiepNhanTruyenTai.DanhSachTepDinhKem.Count > 0)
                {
                    foreach (var tep in tiepNhanTruyenTai.DanhSachTepDinhKem)
                    {
                        var data = FileExtension.GetBytes(tep.Data, out errorMessage);
                        if (!string.IsNullOrWhiteSpace(errorMessage))
                        {
                            break;
                        }
                        tepDinhKems.Add(new ThongTinFile()
                        {
                            Data = data,
                            Loai = tep.Loai,
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    truyenTai.MaLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải đến bến không thành công do tệp đính kèm không hợp lệ - {errorMessage}");
                    return new DataResponsive<string>().returnData("", EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE, errorMessage);
                }
                var mstDonViTruyenNhan = xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan;
                truyenTai.FileXml = LuuFile(mstDonViTruyenNhan, dataXml, EContentType.Xml);
                truyenTai.DanhSachFileDinhKem = tepDinhKems.Select(x => LuuFile(mstDonViTruyenNhan, x.Data, x.Loai)).ToList();

                var lenhTruyenTai = MapLenhVanChuyen(xml.data, truyenTai);

                //Cập nhật tên sở GTVT
                var soGTVT = LaySoGTVT(lenhTruyenTai.MaSoDonViTruyenTai);
                if (!soGTVT.status)
                {
                    lenhTruyenTai.MaLoi = soGTVT.errorCode;
                    lenhTruyenTai.ThongTinLoi = soGTVT.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy thông tin sở GTVT đơn vị truyền tải lỗi");
                    return new DataResponsive<string>().returnData("", soGTVT.errorCode, soGTVT.message);
                }
                lenhTruyenTai.TenSoDonViTruyenTai = soGTVT.data.TenSoGTVT;

                //Kiểm tra xem mã lệnh đã được cấp phát chưa
                var lenhCapPhat = LayLenhCapPhat(lenhTruyenTai.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, lenhTruyenTai.MaLenhTiepNhan, taiKhoan);
                if (!lenhCapPhat.status)
                {
                    lenhTruyenTai.MaLoi = lenhCapPhat.errorCode;
                    lenhTruyenTai.ThongTinLoi = lenhCapPhat.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);

                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy lệnh cấp phát không thành công");
                    return new DataResponsive<string>().returnData("", lenhCapPhat.errorCode, lenhCapPhat.message);
                }
                lenhTruyenTai.MaLenhCapPhat = lenhCapPhat.data;

                dbTruyenTai.InsertOne(lenhTruyenTai);
                logger.LogInformation($"Tiếp nhận doanh nghiệp truyền tải đến bến thành công");
                return new DataResponsive<string>().returnData(lenhTruyenTai.MaLenhCapPhat.ToString().ToLower(), EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Tiếp nhận doanh nghiệp truyền tải đến bến gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<string> DoanhNghiep_TruyenTaiXeBuyt(TiepNhanTruyenTai tiepNhanTruyenTai, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Tiếp nhận doanh nghiệp truyền tải xe buýt - MaLenh={tiepNhanTruyenTai.MaLenh} - CheckSum={tiepNhanTruyenTai.Checksum}");

                var dataXml = FileExtension.GetBytes(tiepNhanTruyenTai.XML, out string errorXml);

                var noiDungTiepNhan = new TiepNhanTruyenTai()
                {
                    MaLenh = tiepNhanTruyenTai.MaLenh,
                    Checksum = tiepNhanTruyenTai.Checksum,
                };
                var truyenTai = new CDB_LenhVanChuyen
                {
                    _id = Guid.NewGuid(),
                    MaLenhCapPhat = Guid.Empty,
                    MaLenhTiepNhan = tiepNhanTruyenTai.MaLenh,
                    IdDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id,
                    TenDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Name,
                    IdLoaiTruyenTai = ELoaiTruyenTai.XE_BUYT.Id,
                    TenLoaiTruyenTai = ELoaiTruyenTai.XE_BUYT.Name,
                    ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan,
                    HoTen_NguoiTruyenTai = taiKhoan.HoTen,
                    NgayTruyenTai = Common.nowWithTimeLocal.Date,
                    ThoiGianTruyenTai = Common.now,
                    MaLoi = EMaLoi.THANH_CONG.Id,
                    ThongTinLoi = EMaLoi.THANH_CONG.Name,
                    DuLieuTiepNhan = noiDungTiepNhan.Object2Json(),
                    MaSoThueDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                    MaSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                };

                if (!string.IsNullOrWhiteSpace(errorXml))
                {
                    truyenTai.MaLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xe buýt không thành công do file xml không hợp lệ - {errorXml}");
                    return new DataResponsive<string>().returnData("", EMaLoi.FILE_XML_KHONG_HOP_LE, errorXml);
                }

                var xml = Xml2Object(dataXml);
                if (!xml.status)
                {
                    truyenTai.MaLoi = EMaLoi.XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = xml.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xe buýt không thành công do xml không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.XML_KHONG_HOP_LE, xml.message);
                }

                //Kiểm tra chữ ký của đơn vị truyền nhận
                var kiemTraChuKy = VerifyCheckSum(xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan, tiepNhanTruyenTai, tiepNhanTruyenTai.Checksum);
                if (!kiemTraChuKy.data)
                {
                    truyenTai.MaLoi = kiemTraChuKy.errorCode;
                    truyenTai.ThongTinLoi = kiemTraChuKy.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    return new DataResponsive<string>().returnData("", kiemTraChuKy.errorCode, kiemTraChuKy.message);
                }

                //Kiểm tra chữ ký của doanh nghiệp
                var kiemTraDoanhNghiepKySo = CheckSign(dataXml, EDoiTuongKiemTra.DOANH_NGHIEP, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, "");
                if (!kiemTraDoanhNghiepKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraDoanhNghiepKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xe buýt không thành công do doanh nghiệp ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraDoanhNghiepKySo.message);
                }

                //Lưu file
                var tepDinhKems = new List<ThongTinFile>();
                string errorMessage = "";
                if (tiepNhanTruyenTai.DanhSachTepDinhKem != null && tiepNhanTruyenTai.DanhSachTepDinhKem.Count > 0)
                {
                    foreach (var tep in tiepNhanTruyenTai.DanhSachTepDinhKem)
                    {
                        var data = FileExtension.GetBytes(tep.Data, out errorMessage);
                        if (!string.IsNullOrWhiteSpace(errorMessage))
                        {
                            break;
                        }
                        tepDinhKems.Add(new ThongTinFile()
                        {
                            Data = data,
                            Loai = tep.Loai,
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    truyenTai.MaLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải xe buýt không thành công do tệp đính kèm không hợp lệ - {errorMessage}");
                    return new DataResponsive<string>().returnData("", EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE, errorMessage);
                }
                var mstDonViTruyenNhan = xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan;
                truyenTai.FileXml = LuuFile(mstDonViTruyenNhan, dataXml, EContentType.Xml);
                truyenTai.DanhSachFileDinhKem = tepDinhKems.Select(x => LuuFile(mstDonViTruyenNhan, x.Data, x.Loai)).ToList();

                var lenhTruyenTai = MapLenhVanChuyen(xml.data, truyenTai);

                //Cập nhật tên sở GTVT
                var soGTVT = LaySoGTVT(lenhTruyenTai.MaSoDonViTruyenTai);
                if (!soGTVT.status)
                {
                    lenhTruyenTai.MaLoi = soGTVT.errorCode;
                    lenhTruyenTai.ThongTinLoi = soGTVT.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy thông tin sở GTVT đơn vị truyền tải lỗi");
                    return new DataResponsive<string>().returnData("", soGTVT.errorCode, soGTVT.message);
                }
                lenhTruyenTai.TenSoDonViTruyenTai = soGTVT.data.TenSoGTVT;

                //Kiểm tra xem mã lệnh đã được cấp phát chưa
                var lenhCapPhat = LayLenhCapPhat(lenhTruyenTai.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, lenhTruyenTai.MaLenhTiepNhan, taiKhoan);
                if (!lenhCapPhat.status)
                {
                    lenhTruyenTai.MaLoi = lenhCapPhat.errorCode;
                    lenhTruyenTai.ThongTinLoi = lenhCapPhat.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy lệnh cấp phát không thành công");
                    return new DataResponsive<string>().returnData("", lenhCapPhat.errorCode, lenhCapPhat.message);
                }
                lenhTruyenTai.MaLenhCapPhat = lenhCapPhat.data;


                dbTruyenTai.InsertOne(lenhTruyenTai);
                logger.LogInformation($"Tiếp nhận doanh nghiệp truyền tải xe buýt thành công");
                return new DataResponsive<string>().returnData(lenhTruyenTai.MaLenhCapPhat.ToString().ToLower(), EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Tiếp nhận doanh nghiệp truyền tải xe buýt gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<string> DoanhNghiep_TruyenTaiLenhThayThe(TruyenTaiDuLieuLenhThayThe truyenTaiDuLieuLenhThayThe, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Doanh nghiệp truyền tải lệnh thay thế");

                var dataXml = FileExtension.GetBytes(truyenTaiDuLieuLenhThayThe.XML, out string errorXml);
                var maLenhCapPhat = Guid.NewGuid();

                var noiDungTiepNhan = new TruyenTaiDuLieuLenhThayThe()
                {
                    MaLenhVanChuyen = truyenTaiDuLieuLenhThayThe.MaLenhVanChuyen,
                    MaLenh = truyenTaiDuLieuLenhThayThe.MaLenh,
                    MaLenhBiThayThe = truyenTaiDuLieuLenhThayThe.MaLenhBiThayThe,
                    Checksum = truyenTaiDuLieuLenhThayThe.Checksum,
                };

                var truyenTai = new CDB_LenhVanChuyen
                {
                    _id = Guid.NewGuid(),
                    MaLenhCapPhat = maLenhCapPhat,
                    MaLenhTiepNhan = truyenTaiDuLieuLenhThayThe.MaLenh,
                    IdDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id,
                    TenDoiTuongTruyenTai = EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Name,
                    IdLoaiTruyenTai = ELoaiTruyenTai.THAY_THE.Id,
                    TenLoaiTruyenTai = ELoaiTruyenTai.THAY_THE.Name,
                    ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan,
                    HoTen_NguoiTruyenTai = taiKhoan.HoTen,
                    NgayTruyenTai = Common.nowWithTimeLocal.Date,
                    ThoiGianTruyenTai = Common.now,
                    MaLoi = EMaLoi.THANH_CONG.Id,
                    ThongTinLoi = EMaLoi.THANH_CONG.Name,
                    DuLieuTiepNhan = noiDungTiepNhan.Object2Json(),
                    MaSoThueDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenDonViTruyenNhan = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                    MaSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.MaSoThueDonViTruyenNhan,
                    TenSoDonViTruyenTai = taiKhoan.CauHinhTaiKhoan.TenDonViTruyenNhan,
                };

                if (!string.IsNullOrWhiteSpace(errorXml))
                {
                    truyenTai.MaLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.FILE_XML_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải lệnh thay thế không thành công do file xml không hợp lệ - {errorXml}");
                    return new DataResponsive<string>().returnData("", EMaLoi.FILE_XML_KHONG_HOP_LE, errorXml);
                }

                var xml = Xml2Object(dataXml);
                if (!xml.status)
                {
                    truyenTai.MaLoi = EMaLoi.XML_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = xml.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải lệnh thay thế không thành công do xml không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.XML_KHONG_HOP_LE, xml.message);
                }

                //Kiểm tra chữ ký của đơn vị truyền nhận
                var kiemTraChuKy = VerifyCheckSum(xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan, truyenTaiDuLieuLenhThayThe, truyenTaiDuLieuLenhThayThe.Checksum);
                if (!kiemTraChuKy.data)
                {
                    truyenTai.MaLoi = kiemTraChuKy.errorCode;
                    truyenTai.ThongTinLoi = kiemTraChuKy.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    return new DataResponsive<string>().returnData("", kiemTraChuKy.errorCode, kiemTraChuKy.message);
                }

                //Kiểm tra chữ ký của doanh nghiệp
                var kiemTraDoanhNghiepKySo = CheckSign(dataXml, EDoiTuongKiemTra.DOANH_NGHIEP, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT, xml.data.DuLieu.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, "");
                if (!kiemTraDoanhNghiepKySo.status)
                {
                    truyenTai.MaLoi = EMaLoi.CHU_KY_KHONG_DUNG.Id;
                    truyenTai.ThongTinLoi = kiemTraDoanhNghiepKySo.message;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải thay thế không thành công do doanh nghiệp ký số không hợp lệ");
                    return new DataResponsive<string>().returnData("", EMaLoi.CHU_KY_KHONG_DUNG, kiemTraDoanhNghiepKySo.message);
                }

                //Lưu file
                var tepDinhKems = new List<ThongTinFile>();
                string errorMessage = "";
                if (truyenTaiDuLieuLenhThayThe.DanhSachTepDinhKem != null && truyenTaiDuLieuLenhThayThe.DanhSachTepDinhKem.Count > 0)
                {
                    foreach (var tep in truyenTaiDuLieuLenhThayThe.DanhSachTepDinhKem)
                    {
                        var data = FileExtension.GetBytes(tep.Data, out errorMessage);
                        if (!string.IsNullOrWhiteSpace(errorMessage))
                        {
                            break;
                        }
                        tepDinhKems.Add(new ThongTinFile()
                        {
                            Data = data,
                            Loai = tep.Loai,
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    truyenTai.MaLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Id;
                    truyenTai.ThongTinLoi = EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE.Name;

                    dbTruyenTai.InsertOne(truyenTai);
                    logger.LogError($"Tiếp nhận doanh nghiệp truyền tải lệnh thay thế không thành công do tệp đính kèm không hợp lệ - {errorMessage}");
                    return new DataResponsive<string>().returnData("", EMaLoi.TEP_DINH_KEM_KHONG_HOP_LE, errorMessage);
                }
                var mstDonViTruyenNhan = xml.data.DuLieu.ThongTinChung.MaSoThueDonViTruyenNhan;
                truyenTai.FileXml = LuuFile(mstDonViTruyenNhan, dataXml, EContentType.Xml);
                truyenTai.DanhSachFileDinhKem = tepDinhKems.Select(x => LuuFile(mstDonViTruyenNhan, x.Data, x.Loai)).ToList();

                var lenhTruyenTai = MapLenhVanChuyen(xml.data, truyenTai);

                //Cập nhật tên sở GTVT
                var soGTVT = LaySoGTVT(lenhTruyenTai.MaSoDonViTruyenTai);
                if (!soGTVT.status)
                {
                    lenhTruyenTai.MaLoi = soGTVT.errorCode;
                    lenhTruyenTai.ThongTinLoi = soGTVT.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy thông tin sở GTVT đơn vị truyền tải lỗi");
                    return new DataResponsive<string>().returnData("", soGTVT.errorCode, soGTVT.message);
                }
                lenhTruyenTai.TenSoDonViTruyenTai = soGTVT.data.TenSoGTVT;

                //Kiểm tra xem mã lệnh đã được cấp phát chưa
                var lenhCapPhat = LayLenhCapPhat(lenhTruyenTai.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoThueDonViVanTai, lenhTruyenTai.MaLenhTiepNhan, taiKhoan);
                if (!lenhCapPhat.status)
                {
                    lenhTruyenTai.MaLoi = lenhCapPhat.errorCode;
                    lenhTruyenTai.ThongTinLoi = lenhCapPhat.message;

                    dbTruyenTai.InsertOne(lenhTruyenTai);
                    logger.LogError($"Tiếp nhận truyền tải lỗi do lấy lệnh cấp phát không thành công");
                    return new DataResponsive<string>().returnData("", lenhCapPhat.errorCode, lenhCapPhat.message);
                }
                lenhTruyenTai.MaLenhCapPhat = lenhCapPhat.data;

                //Kiểm tra lệnh đã được truyền tải lên từ trước hay chưa
                var truyenTaiTruoc = dbLenhCapPhat.Find(x => x._id == truyenTaiDuLieuLenhThayThe.MaLenhVanChuyen).FirstOrDefault();
                if (truyenTaiTruoc != null)
                {
                    truyenTai.MaLoi = EMaLoi.KHONG_TIM_THAY_DU_LIEU.Id;
                    truyenTai.ThongTinLoi = "Không tìm thấy thông tin truyền tải lệnh bị thay thế";
                }
                dbTruyenTai.InsertOne(lenhTruyenTai);
                if (truyenTai.MaLoi == EMaLoi.THANH_CONG.Id)
                {
                    logger.LogInformation($"Doanh nghiệp truyển tải lệnh thay thế thành công");
                    return new DataResponsive<string>().returnData(maLenhCapPhat.ToString().ToLower(), EMaLoi.THANH_CONG);
                }
                else
                {
                    logger.LogError(truyenTai.ThongTinLoi);
                    return new DataResponsive<string>().returnData("", truyenTai.MaLoi, truyenTai.ThongTinLoi);
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Doanh nghiệp truyền tải lệnh thay thế gặp lỗi");
                return new DataResponsive<string>().returnData("", EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> DoanhNghiep_TruyenTaiLenhDangHoatDong(TruyenTaiDuLieuDangHoatDong truyenTaiDuLieuDangHoatDong, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Doanh nghiệp truyền tải lệnh đang hoạt động");
                //TODO - Chưa xác thực checksum

                var thongTin = mapper.Map<CDB_TruyenTaiTrenDuong>(truyenTaiDuLieuDangHoatDong);
                thongTin.IdTruyenTai = Guid.NewGuid();
                thongTin.ID_NguoiTruyenTai = taiKhoan.IdTaiKhoan;
                thongTin.HoTen_NguoiTruyenTai = taiKhoan.HoTen;
                thongTin.NgayTruyenTai = Common.nowWithTimeLocal.Date;
                thongTin.ThoiGianTruyenTai = Common.now;
                thongTin.DuLieuTiepNhan = truyenTaiDuLieuDangHoatDong.Object2Json();
                thongTin.MaLoi = EMaLoi.THANH_CONG.Id;
                thongTin.ThongTinLoi = EMaLoi.THANH_CONG.Name;

                var truyenTai = dbTruyenTai.Find(x => x.MaLenhCapPhat == truyenTaiDuLieuDangHoatDong.MaLenhVanChuyen && x.MaLoi == EMaLoi.THANH_CONG.Id).FirstOrDefault();
                if (truyenTai == null)
                {
                    thongTin.MaLoi = EMaLoi.KHONG_TIM_THAY_DU_LIEU.Id;
                    thongTin.ThongTinLoi = "Mã lệnh vận chuyển không tồn tại";
                }
                dbTruyenTaiTrenDuong.InsertOne(thongTin);
                if (thongTin.MaLoi == EMaLoi.THANH_CONG.Id)
                {
                    logger.LogInformation($"Doanh nghiệp truyền tải lệnh đang hoạt động thành công");
                    return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
                }
                else
                {
                    logger.LogError(thongTin.ThongTinLoi);
                    return new DataResponsive<bool>().returnData(false, thongTin.MaLoi, thongTin.ThongTinLoi);
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Doanh nghiệp truyền tải lệnh đang hoạt động gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

    }
}
