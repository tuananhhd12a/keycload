﻿using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.Mongo.LuuTru;
using Microsoft.Extensions.Logging;
using Minio;
using System;
using System.IO;
using System.Linq;
using static System.Net.WebRequestMethods;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        public string LuuFile(string maSoThue, byte[] bytes, string contentType)
        {
            try
            {
                var configMinio = LayCauHinhMinio();
                if (configMinio == null)
                {
                    logger.LogWarning($"Chưa cấu hình minio");
                    return "";
                }
                if (contentType.Contains("xml"))
                {
                    contentType = EContentType.Xml;
                }
                else if (contentType.Contains("pdf"))
                {
                    contentType = EContentType.Pdf;
                }
                else if (contentType.Contains("jpg"))
                {
                    contentType = EContentType.Jpg;
                }
                else
                {
                    logger.LogWarning($"Không hỗ trợ loại tệp {contentType}");
                    return "";
                }

                var client = new MinioClient()
                                 .WithEndpoint(configMinio.EndPoint)
                                 .WithCredentials(accessKey: configMinio.AccessKey, secretKey: configMinio.SecretKey);
                var path = $"{maSoThue}/{Common.nowWithTimeLocal.Year.ToString()}/{Common.nowWithTimeLocal.Month.ToString()}/{Common.nowWithTimeLocal.Day.ToString()}/{Guid.NewGuid().ToString().ToLower()}";
                PutObjectArgs putObjectArgs = new PutObjectArgs().WithBucket(configMinio.Bucket)
                                                                 .WithObject(path)
                                                                 .WithStreamData(new MemoryStream(bytes))
                                                                 .WithObjectSize(bytes.Length)
                                                                 .WithContentType(contentType);
                client.PutObjectAsync(putObjectArgs);
                return path;
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lưu file {contentType} gặp lỗi");
                return "";
            }
        }

        public byte[] LayFile(string objectName, out string contentType)
        {
            contentType = "";
            try
            {
                if (string.IsNullOrWhiteSpace(objectName))
                {
                    return null;
                }
                using (MemoryStream memoryStream = new())
                {
                    var configMinio = LayCauHinhMinio();
                    if (configMinio == null)
                    {
                        logger.LogWarning($"Chưa cấu hình minio");
                        return null;
                    }
                    var minioClient = new MinioClient()
                                        .WithEndpoint(configMinio.EndPoint)
                                        .WithCredentials(accessKey: configMinio.AccessKey, secretKey: configMinio.SecretKey); ;

                    GetObjectArgs getObjectArgs = new GetObjectArgs()
                                                    .WithBucket(configMinio.Bucket)
                                                    .WithObject(objectName)
                                                    .WithCallbackStream((stream) => stream.CopyTo(memoryStream));
                    var info = minioClient.GetObjectAsync(getObjectArgs).GetAwaiter().GetResult();

                    contentType = info.ContentType;
                    return memoryStream.ToArray();
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy file gặp lỗi");
                return null;
            }
        }
    }
}
