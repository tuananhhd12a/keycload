﻿using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.Mongo.LuuTru;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        public ConfigMinio LayCauHinhMinio()
        {
            logger.LogInformation($"Thực hiện lấy cấu hình minio");
            try
            {
                var key = $"CONFIG_MINIO_{ELoaiCauHinhLuuTru.TRUYEN_TAI.Id}";
                var _config = new ConfigMinio();
                var _cache = "";
                var redisError = false;
                var option = new DistributedCacheEntryOptions() { SlidingExpiration = Common.cacheTime };
                try
                {
                    _cache = distributedCache.GetString(key);
                }
                catch (Exception e)
                {
                    redisError = true;
                    logger.LogError(e, $"Lấy dữ liệu minio từ cache gặp lỗi");
                }
                if (string.IsNullOrWhiteSpace(_cache))
                {
                    logger.LogInformation($"Cache: {_cache}");

                    var config = dbMinio.Find(x => x.LoaiLuuTru == ELoaiCauHinhLuuTru.TRUYEN_TAI.Id).FirstOrDefault();
                    if (config == null)
                    {
                        logger.LogError($"Không tìm thấy cấu hình minio");
                        return null;
                    }
                    _config.EndPoint = config.EndPoint;
                    _config.Bucket = config.Bucket;
                    _config.AccessKey = config.AccessKey;
                    _config.SecretKey = config.SecretKey;
                    try
                    {
                        if (!redisError)
                        {
                            distributedCache.SetString(key, _config.Object2Json(), option);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogError(e, $"Lưu dữ liệu minio vào cache gặp lỗi");
                    }
                }
                else
                {
                    _config = JsonConvert.DeserializeObject<ConfigMinio>(_cache);
                }
                return _config;
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lỗi khi lấy cấu hình minio");
                return null;
            }
        }

        public string GetPublicKeyDonViTruyenNhan(string maSoThue)
        {
            try
            {
                logger.LogInformation($"Lấy publickey đơn vị truyền nhận - {maSoThue}");
                var cacheKey = $"CONFIG_PUBLICKEY_TRUYENNHAN_{maSoThue}";
                var cache = "";
                try
                {
                    cache = distributedCache.GetString(cacheKey);
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"Lấy dữ liệu từ cache gặp lỗi");
                }
                if (string.IsNullOrWhiteSpace(cache))
                {
                    var donVi = dbDonViTruyenNhan.Find(x => x.MaSoThue == maSoThue).FirstOrDefault();
                    if (donVi == null)
                    {
                        logger.LogError($"Không tìm thấy đơn vị truyền nhận");
                        return "";
                    }
                    else
                    {
                        cache = donVi.PublicKey;
                        try
                        {
                            distributedCache.SetString(cacheKey, cache, new DistributedCacheEntryOptions()
                            {
                                SlidingExpiration = TimeSpan.FromMinutes(60)
                            });
                        }
                        catch (Exception e)
                        {
                            logger.LogError(e, $"Ghi dữ liệu vào cache gặp lỗi");
                        }
                    }
                }
                logger.LogInformation($"Lấy publickey đơn vị truyền nhận thành công");
                return cache;
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy publickey đơn vị truyền nhận gặp lỗi");
                return "";
            }
        }

        public string GetPublicKeyDonViHoatDong(string maSoThue, string maBenXe, string maSo, bool laDoanhNghiep)
        {
            try
            {
                logger.LogInformation($"Lấy publickey đơn vị hoạt động - {maSoThue}");
                var cacheKey = $"CONFIG_PUBLICKEY_HOATDONG_{maSoThue}";
                var cache = "";
                try
                {
                    cache = distributedCache.GetString(cacheKey);
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"Lấy dữ liệu từ cache gặp lỗi");
                }
                if (string.IsNullOrWhiteSpace(cache))
                {
                    if (laDoanhNghiep)
                    {
                        //Là doanh nghiệp vận tải
                        var donVi = dbDonViHoatDong.Find(x => x.MaSoThue == maSoThue && x.MaSoGTVT == maSo && x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id).FirstOrDefault();
                        if (donVi == null)
                        {
                            logger.LogError($"Không tìm thấy đơn vị hoạt động");
                            return "";
                        }
                        else
                        {
                            cache = donVi.PublicKey;
                            try
                            {
                                distributedCache.SetString(cacheKey, cache, new DistributedCacheEntryOptions()
                                {
                                    SlidingExpiration = TimeSpan.FromMinutes(60)
                                });
                            }
                            catch (Exception e)
                            {
                                logger.LogError(e, $"Ghi dữ liệu vào cache gặp lỗi");
                            }
                        }
                    }
                    else
                    {
                        //Là bến xe
                        var donVi = dbDonViHoatDong.Find(x => x.MaBenXe == maBenXe && x.MaSoGTVT == maSo && x.TrangThai.Id == ETrangThaiCauHinh.DANG_HOAT_DONG.Id).FirstOrDefault();
                        if (donVi == null)
                        {
                            logger.LogError($"Không tìm thấy đơn vị hoạt động");
                            return "";
                        }
                        else
                        {
                            cache = donVi.PublicKey;
                            try
                            {
                                distributedCache.SetString(cacheKey, cache, new DistributedCacheEntryOptions()
                                {
                                    SlidingExpiration = TimeSpan.FromMinutes(60)
                                });
                            }
                            catch (Exception e)
                            {
                                logger.LogError(e, $"Ghi dữ liệu vào cache gặp lỗi");
                            }
                        }
                    }
                }
                logger.LogInformation($"Lấy publickey đơn vị hoạt động thành công");
                return cache;
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy publickey đơn vị hoạt động gặp lỗi");
                return "";
            }
        }
    }
}
