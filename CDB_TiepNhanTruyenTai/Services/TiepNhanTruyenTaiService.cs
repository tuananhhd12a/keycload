﻿using AutoMapper;
using Lib_Core.Objects.Mongo;
using Lib_Core.Objects.Mongo.CauHinh;
using Lib_Core.Objects.Mongo.HanhChinh;
using Lib_Core.Objects.Mongo.LuuTru;
using Lib_Core.Objects.Mongo.TruyenTai;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTin_TruyenTaiTrenDuong;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        private readonly ILogger<TiepNhanTruyenTaiService> logger;
        private readonly IMapper mapper;
        private readonly IDistributedCache distributedCache;
        private readonly IMongoCollection<CDB_LenhVanChuyen> dbTruyenTai;
        private readonly IMongoCollection<CDB_TruyenTaiTrenDuong> dbTruyenTaiTrenDuong;
        private readonly IMongoCollection<ConfigMinio> dbMinio;
        private readonly IMongoCollection<CauHinh_DonViTruyenNhan> dbDonViTruyenNhan;
        private readonly IMongoCollection<CDB_LenhCapPhat> dbLenhCapPhat;
        private readonly IMongoCollection<ThongTin_SoGiaoThongVanTai> dbSoGTVT;
        private readonly IMongoCollection<CauHinh_DonViHoatDong> dbDonViHoatDong;

        public TiepNhanTruyenTaiService(ILogger<TiepNhanTruyenTaiService> logger, IDatabaseMongoSettings settings, IMapper mapper, IDistributedCache distributedCache)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.distributedCache = distributedCache;

            //Khởi tạo mongo
            var client = new MongoClient($"{settings.ConnectionString}");
            this.dbTruyenTai = client.GetDatabase(settings.DatabaseName).GetCollection<CDB_LenhVanChuyen>(nameof(CDB_LenhVanChuyen));
            this.dbTruyenTaiTrenDuong = client.GetDatabase(settings.DatabaseName).GetCollection<CDB_TruyenTaiTrenDuong>(nameof(CDB_TruyenTaiTrenDuong));
            this.dbMinio = client.GetDatabase(settings.DatabaseName).GetCollection<ConfigMinio>(nameof(ConfigMinio));
            this.dbDonViTruyenNhan = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_DonViTruyenNhan>(nameof(CauHinh_DonViTruyenNhan));
            this.dbLenhCapPhat = client.GetDatabase(settings.DatabaseName).GetCollection<CDB_LenhCapPhat>(nameof(CDB_LenhCapPhat));
            this.dbSoGTVT = client.GetDatabase(settings.DatabaseName).GetCollection<ThongTin_SoGiaoThongVanTai>(nameof(ThongTin_SoGiaoThongVanTai));
            this.dbDonViHoatDong = client.GetDatabase(settings.DatabaseName).GetCollection<CauHinh_DonViHoatDong>(nameof(CauHinh_DonViHoatDong));

            //Create Index
            this.dbLenhCapPhat.Indexes.CreateOneAsync(
                Builders<CDB_LenhCapPhat>.IndexKeys.Ascending(x => x.MaLenhTiepNhan)
                , new CreateIndexOptions() { Background = true }
                );


            this.dbTruyenTai.Indexes.CreateOneAsync(
               Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.ThoiGianTruyenTai)
               , new CreateIndexOptions() { Background = true }
                );

            this.dbTruyenTai.Indexes.CreateOneAsync(
              Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.IdDoiTuongTruyenTai)
              , new CreateIndexOptions() { Background = true }
               );

            this.dbTruyenTai.Indexes.CreateOneAsync(
              Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.IdDoiTuongTruyenTai)
                                                   .Ascending(x => x.MaSoDonViTruyenTai)
              , new CreateIndexOptions() { Background = true }
               );

            this.dbTruyenTai.Indexes.CreateOneAsync(
               Builders<CDB_LenhVanChuyen>.IndexKeys.Ascending(x => x.ThoiGianTruyenTai)
                                                    .Ascending(x => x.IdDoiTuongTruyenTai)
                                                    .Ascending(x => x.MaSoDonViTruyenTai)
               , new CreateIndexOptions() { Background = true }
                );
        }
    }
}
