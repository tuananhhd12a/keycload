﻿using AutoMapper;
using CDB_TiepNhanTruyenTai.Models.Input;
using CDB_TiepNhanTruyenTai.Models.Xml.Lenh;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_LenhVanChuyen;
using Lib_Core.Objects.Mongo.TruyenTai;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTin_TruyenTaiTrenDuong;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using System;
using System.Collections.Generic;

namespace CDB_TiepNhanTruyenTai.Services
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<ThongTinChung, CDB_LenhVanChuyen>();
            CreateMap<ThongTinDoanhNghiep, CDB_ThongTinDoanhNghiep>();
            CreateMap<ThongTinChuyenXe, CDB_ThongTinChuyenXe>();
            CreateMap<PhuongTienTheoKeHoach, CDB_PhuongTienTheoKeHoach>();
            CreateMap<PhuongTienThucHien, CDB_PhuongTienThucHien>();
            CreateMap<ThongTinPhuongTien, CDB_ThongTinPhuongTien>();
            CreateMap<TuyenVanChuyen, CDB_TuyenVanChuyen>();
            CreateMap<BenDi, CDB_BenDi>();
            CreateMap<BenDen, CDB_BenDen>();
            CreateMap<LaiXe, CDB_LaiXe>();
            CreateMap<NhanVienPhucVu, CDB_NhanVienPhucVu>();
            CreateMap<ThongTin, CDB_ThongTin>();
            CreateMap<LyDo, CDB_LyDo>();
            CreateMap<LaiXeXacNhanLenh, CDB_LaiXeXacNhanLenh>();
            CreateMap<NhanVienKyLenh, CDB_NhanVienKyLenh>();
            CreateMap<NoiDungBenDiXacNhan, CDB_NoiDungBenDiXacNhan>();
            CreateMap<NoiDungBenDenXacNhan, CDB_NoiDungBenDenXacNhan>();
            CreateMap<NoiDungLenhVanChuyen, CDB_NoiDungLenhVanChuyen>();
            CreateMap<TruyenTaiDuLieuDangHoatDong, CDB_TruyenTaiTrenDuong>()
                    .ForMember(dest => dest.MaLenhCapPhat, opt => opt.MapFrom(x => x.MaLenhVanChuyen));
        }
    }
}
