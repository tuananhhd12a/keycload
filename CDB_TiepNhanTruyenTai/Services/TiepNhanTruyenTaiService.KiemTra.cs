﻿using CDB_TiepNhanTruyenTai.Models.Input;
using CDB_TiepNhanTruyenTai.Models.Xml.Lenh;
using CDB_TiepNhanTruyenTai.ViewModels.Output;
using Lib_Core.CheckSum;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.JWT;
using Lib_Core.Objects.Mongo.HanhChinh;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Text;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        public DataResponsive<bool> VerifyCheckSum(string maSoThue, object data, string checksum)
        {
            try
            {
                logger.LogInformation($"Kiểm tra chữ ký");
                var publicKey = GetPublicKeyDonViTruyenNhan(maSoThue);
                if (string.IsNullOrWhiteSpace(publicKey))
                {
                    logger.LogError($"Kiểm tra chữ ký không thành công do đơn vị truyền nhận chưa đăng ký trên hệ thống hoặc chưa khai báo publickey");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DON_VI_TRUYEN_NHAN_KHONG_HOP_LE);
                }
                else
                {
                    if (!Lib_Core.CheckSum.DataValidation.VerifySignData(publicKey, data, checksum))
                    {
                        logger.LogError($"Kiểm tra chữ ký không thành công do checksum không khớp");
                        return new DataResponsive<bool>().returnData(false, EMaLoi.CHU_KY_KHONG_DUNG);
                    }
                }
                logger.LogInformation($"Kiểm tra chữ ký thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"VerifyCheckSum Error");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<bool> CheckSign(byte[] xml, EDoiTuongKiemTra doiTuong, string maSoGTVT, string maSoThue, string maBenXe)
        {
            try
            {
                logger.LogInformation($"Bắt đầu CheckSign {doiTuong.Name}");
                var publicKey = GetPublicKeyDonViHoatDong(maSoThue, maBenXe, maSoGTVT, doiTuong == EDoiTuongKiemTra.DOANH_NGHIEP);
                if (string.IsNullOrWhiteSpace(publicKey))
                {
                    logger.LogError($"CheckSign không thành công do đơn vị hoạt động chưa đăng ký trên hệ thống hoặc chưa khai báo publickey");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DON_VI_HOAT_DONG_KHONG_HOP_LE);
                }
                publicKey = publicKey.Replace("-----BEGIN CERTIFICATE-----", "")
                                     .Replace("-----END CERTIFICATE-----", "")
                                     .Replace("-----BEGIN PUBLIC KEY-----", "")
                                     .Replace("-----END PUBLIC KEY-----", "");
                var dataXml = Encoding.UTF8.GetString(xml);
                if (string.IsNullOrWhiteSpace(dataXml))
                {
                    logger.LogError($"Xml rỗng");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.XML_KHONG_HOP_LE);
                }
                var publicKeyXml = DataValidation.GetCertificateFromXml(dataXml, doiTuong);
                if (string.IsNullOrWhiteSpace(publicKeyXml))
                {
                    logger.LogError($"CheckSign không hợp lệ do xml không hợp lệ");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.XML_KHONG_HOP_LE);
                }
                if (!publicKey.Equals(publicKeyXml))
                {
                    logger.LogError($"CheckSign không thành công - publicKey chưa được khai báo trên hệ thống");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.DON_VI_HOAT_DONG_KHONG_HOP_LE, "PublicKey chưa được khai báo trên hệ thống!");
                }
                if (!DataValidation.VerifyXml(dataXml, doiTuong, out string messageError))
                {
                    logger.LogInformation($"CheckSign không thành công - {messageError}");
                    return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_THONG_TIN_XAC_THUC, messageError);
                }
                logger.LogInformation($"CheckSign thành công");
                return new DataResponsive<bool>().returnData(true, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"CheckSign {doiTuong.Name} gặp lỗi");
                return new DataResponsive<bool>().returnData(false, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<Guid> LayLenhCapPhat(string maSoThue, string maLenh, ThongTinTaiKhoan taiKhoan)
        {
            try
            {
                logger.LogInformation($"Lấy lệnh cấp phát - MST:{maSoThue} - ML:{maLenh}");
                var maLenhCapPhat = Guid.NewGuid();
                var lenh = dbLenhCapPhat.Find(x => x.MaSoThueDoanhNghiep == maSoThue && x.MaLenhTiepNhan == maLenh).FirstOrDefault();
                if (lenh != null)
                {
                    logger.LogInformation($"Mã lệnh đã được cấp phát");
                    maLenhCapPhat = lenh._id;
                }
                else
                {
                    lenh = new Lib_Core.Objects.Mongo.TruyenTai.CDB_LenhCapPhat()
                    {
                        _id = maLenhCapPhat,
                        MaSoThueDoanhNghiep = maSoThue,
                        MaLenhTiepNhan = maLenh,
                        Created_at = Common.now,
                        Created_by = taiKhoan.IdTaiKhoan,
                        Created_by_name = taiKhoan.HoTen,
                    };
                    dbLenhCapPhat.InsertOne(lenh);
                }
                logger.LogInformation($"Lấy lệnh cấp phát thành công");
                return new DataResponsive<Guid>().returnData(maLenhCapPhat, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy lệnh đã cấp phát gặp lỗi");
                return new DataResponsive<Guid>().returnData(Guid.Empty, EMaLoi.LOI_HE_THONG);
            }
        }

        public DataResponsive<ThongTinSoGTVT> LaySoGTVT(string maSo)
        {
            try
            {
                logger.LogInformation($"Lấy sở GTVT - {maSo}");
                var so = dbSoGTVT.Find(x => x.MaSoGTVT == maSo)
                                 .Project(x => new ThongTinSoGTVT
                                 {
                                     MaSoGTVT = x.MaSoGTVT,
                                     TenSoGTVT = x.TenSoGTVT
                                 })
                                 .FirstOrDefault();
                if (so == null)
                {
                    logger.LogError($"Không tìm thấy mã sở - {maSo}");
                    return new DataResponsive<ThongTinSoGTVT>().returnData(new ThongTinSoGTVT(), EMaLoi.KHONG_TIM_THAY_DU_LIEU, $"Không tìm thấy mã sở {maSo}");
                }
                logger.LogInformation($"Lấy sở GTVT thành công");
                return new DataResponsive<ThongTinSoGTVT>().returnData(so, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Lấy sở GTVT gặp lỗi");
                return new DataResponsive<ThongTinSoGTVT>().returnData(new ThongTinSoGTVT(), EMaLoi.LOI_HE_THONG);
            }
        }
    }
}
