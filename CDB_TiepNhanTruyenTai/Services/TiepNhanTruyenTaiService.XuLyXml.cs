﻿using CDB_TiepNhanTruyenTai.Models.Input;
using CDB_TiepNhanTruyenTai.Models.Xml.ChuKySo;
using CDB_TiepNhanTruyenTai.Models.Xml.Lenh;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.ThongTin_DungChung;
using Lib_Core.Enum;
using Lib_Core.Objects.DungChung;
using Lib_Core.Objects.Mongo.TruyenTai;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using Lib_Core.Objects.Responsive;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Services
{
    public partial class TiepNhanTruyenTaiService
    {
        public DataResponsive<LenhVanChuyen> Xml2Object(byte[] bytes)
        {
            try
            {
                var dataXml = Encoding.UTF8.GetString(bytes);
                if (string.IsNullOrWhiteSpace(dataXml))
                {
                    logger.LogError($"Xml rỗng");
                    return new DataResponsive<LenhVanChuyen>().returnData(new LenhVanChuyen(), EMaLoi.XML_KHONG_HOP_LE);
                }
                //Loại bỏ hết namespace và attribute
                dataXml = RemoveAllNamespaces(dataXml);

                //Xml gồm 2 thẻ, 1 - DuLieu, 2 - DanhSachKySo
                if (XElement.Parse(dataXml).Elements().Count() < 2)
                {
                    logger.LogError($"Cấu trúc xml không hợp lệ - {dataXml}");
                    return new DataResponsive<LenhVanChuyen>().returnData(new LenhVanChuyen(), EMaLoi.XML_KHONG_HOP_LE);
                }
                var xmlDuLieu = XElement.Parse(dataXml).Elements().ElementAt(0).ToString();
                var xmlKySo = XElement.Parse(dataXml).Elements().ElementAt(1).ToString();

                var serDuLieu = new XmlSerializer(typeof(DuLieu), new XmlRootAttribute
                {
                    ElementName = typeof(DuLieu).Name,
                });
                var duLieu = new DuLieu();
                using (StringReader sr = new StringReader(xmlDuLieu))
                {
                    duLieu = (DuLieu)serDuLieu.Deserialize(sr);
                }

                var serKySo = new XmlSerializer(typeof(DanhSachChuKySo), new XmlRootAttribute
                {
                    ElementName = typeof(DanhSachChuKySo).Name
                });
                var danhSachKySo = new DanhSachChuKySo();
                using (StringReader sr = new StringReader(xmlKySo))
                {
                    danhSachKySo = (DanhSachChuKySo)serKySo.Deserialize(sr);
                }

                var thongTinLenh = new LenhVanChuyen()
                {
                    DuLieu = duLieu,
                    DanhSachChuKySo = danhSachKySo,
                };
                var listError = thongTinLenh.Validate(new System.ComponentModel.DataAnnotations.ValidationContext(thongTinLenh, null, null));
                if (listError.Count() != 0)
                {
                    var message = string.Join(" ", listError.Select(x => x.ErrorMessage));
                    logger.LogError($"Nội dung xml không hợp lệ - {message}");
                    return new DataResponsive<LenhVanChuyen>().returnData(new LenhVanChuyen(), EMaLoi.XML_KHONG_HOP_LE, message);
                }
                return new DataResponsive<LenhVanChuyen>().returnData(thongTinLenh, EMaLoi.THANH_CONG);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Xử lý xml 2 object gặp lỗi");
                return new DataResponsive<LenhVanChuyen>().returnData(new LenhVanChuyen(), EMaLoi.XML_KHONG_HOP_LE);
            }
        }

        public CDB_LenhVanChuyen MapLenhVanChuyen(LenhVanChuyen lenhVanChuyen, CDB_LenhVanChuyen thongTinKhoiTao)
        {
            var thongTinLenh = mapper.Map<CDB_LenhVanChuyen>(lenhVanChuyen.DuLieu.ThongTinChung);

            var noiDungLenhVanChuyen = mapper.Map<CDB_NoiDungLenhVanChuyen>(lenhVanChuyen.DuLieu.NoiDungLenhVanChuyen);

            var noiDungBenDiXacNhan = mapper.Map<CDB_NoiDungBenDiXacNhan>(lenhVanChuyen.DuLieu.NoiDungBenDiXacNhan);

            var noiDungBenDenXacNhan = mapper.Map<CDB_NoiDungBenDenXacNhan>(lenhVanChuyen.DuLieu.NoiDungBenDenXacNhan);

            thongTinLenh._id = thongTinKhoiTao._id;
            thongTinLenh.MaLenhCapPhat = thongTinKhoiTao.MaLenhCapPhat;
            thongTinLenh.MaLenhTiepNhan = thongTinKhoiTao.MaLenhTiepNhan;
            thongTinLenh.IdDoiTuongTruyenTai = thongTinKhoiTao.IdDoiTuongTruyenTai;
            thongTinLenh.TenDoiTuongTruyenTai = thongTinKhoiTao.TenDoiTuongTruyenTai;
            thongTinLenh.TenLoaiTruyenTai = thongTinKhoiTao.TenLoaiTruyenTai;
            thongTinLenh.IdLoaiTruyenTai = thongTinKhoiTao.IdLoaiTruyenTai;
            thongTinLenh.ID_NguoiTruyenTai = thongTinKhoiTao.ID_NguoiTruyenTai;
            thongTinLenh.HoTen_NguoiTruyenTai = thongTinKhoiTao.HoTen_NguoiTruyenTai;
            thongTinLenh.NgayTruyenTai = thongTinKhoiTao.NgayTruyenTai;
            thongTinLenh.ThoiGianTruyenTai = thongTinKhoiTao.ThoiGianTruyenTai;
            thongTinLenh.MaLoi = thongTinKhoiTao.MaLoi;
            thongTinLenh.ThongTinLoi = thongTinKhoiTao.ThongTinLoi;
            thongTinLenh.DuLieuTiepNhan = thongTinKhoiTao.DuLieuTiepNhan;
            thongTinLenh.FileXml = thongTinKhoiTao.FileXml;
            thongTinLenh.DanhSachFileDinhKem = thongTinKhoiTao.DanhSachFileDinhKem;
            thongTinLenh.NoiDungLenhVanChuyen = noiDungLenhVanChuyen;
            thongTinLenh.NoiDungBenDiXacNhan = noiDungBenDiXacNhan;
            thongTinLenh.NoiDungBenDenXacNhan = noiDungBenDenXacNhan;

            if (thongTinKhoiTao.IdDoiTuongTruyenTai == EDoiTuongTruyenTai.DOANH_NGHIEP_VAN_TAI.Id)
            {
                thongTinLenh.TenDonViTruyenTai = thongTinLenh.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.TenDonViVanTai;
                thongTinLenh.MaSoDonViTruyenTai = thongTinLenh.NoiDungLenhVanChuyen.ThongTinDoanhNghiep.MaSoGTVT;
            }
            else
            {
                if (thongTinKhoiTao.IdLoaiTruyenTai == ELoaiTruyenTai.XUAT_BEN.Id)
                {
                    thongTinLenh.TenDonViTruyenTai = thongTinLenh.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.TenBenNoiDi;
                    thongTinLenh.MaSoDonViTruyenTai = thongTinLenh.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDi.MaSoGTVT;
                }
                else
                {
                    thongTinLenh.TenDonViTruyenTai = thongTinLenh.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.TenBenNoiDen;
                    thongTinLenh.MaSoDonViTruyenTai = thongTinLenh.NoiDungLenhVanChuyen.TuyenVanChuyen.BenDen.MaSoGTVT;
                }
            }

            thongTinLenh.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.BienKiemSoat = ChuanHoaBienKiemSoat(thongTinLenh.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienTheoKeHoach.BienKiemSoat);
            thongTinLenh.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien.BienKiemSoat = ChuanHoaBienKiemSoat(thongTinLenh.NoiDungLenhVanChuyen.ThongTinPhuongTien.PhuongTienThucHien.BienKiemSoat);

            return thongTinLenh;
        }

        private string RemoveAllNamespaces(string xmlDocument)
        {
            try
            {
                var xElement = XElement.Parse(xmlDocument);
                foreach (var node in xElement.DescendantsAndSelf())
                {
                    node.Name = node.Name.LocalName;
                    node.Attributes("xmlns").Remove();
                }
                return xElement.ToString();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"RemoveAllNamespaces Error");
                throw;
            }
        }

        private string ChuanHoaBienKiemSoat(string bienKiemSoat)
        {
            bienKiemSoat = bienKiemSoat.ToUpper();
            StringBuilder sb = new StringBuilder();
            foreach (char c in bienKiemSoat)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}
