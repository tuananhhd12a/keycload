﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Lib_Core.Attributes
{
    public class EnsureElement : ValidationAttribute
    {
        private readonly int _maxElements;
        private readonly int _minElements;

        public EnsureElement(int minElements, int maxElements)
        {
            _minElements = minElements;
            _maxElements = maxElements;
        }

        public override bool IsValid(object value)
        {
            var list = value as IList;

            if (list != null
             && (list.Count < _minElements || list.Count > _maxElements))
            {
                return false;
            }

            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            var format = string.IsNullOrEmpty(ErrorMessage) ? ErrorMessageString : ErrorMessage;

            return string.Format(format, new object[]
            {
                name, _minElements, _maxElements
            });
        }
    }
}
