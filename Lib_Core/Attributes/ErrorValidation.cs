﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Attributes
{
    public class ErrorValidation
    {

        public ErrorValidation(string field, string message)
        {
            Message = message;
            Field = field;
        }

        public string Field { get; set; }
        public string Message { get; set; }
    }
}
