﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Attributes
{
    public class GuidValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }
            if (!Guid.TryParse(value.ToString(), out Guid result))
            {
                return false;
            }
            else if (result == Guid.Empty)
            {
                return false;
            }
            return true;
        }
    }
}
