﻿using DevExtreme.AspNet.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Parameters
{
    public class DevExDataSourceLoadOptions<T>
    {
        public DataSourceLoadOptionsBase loadOptions { get; set; }
        public T? Custom { get; set; }
    }
}
