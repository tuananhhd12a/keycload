﻿using Lib_Core.Objects.API;
using Lib_Core.Objects.JWT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.JWT
{
    public class ThongTinTaiKhoan
    {
        public Guid IdTaiKhoan { get; set; }
        public Guid IdDonVi { get; set; }
        public string TenTaiKhoan { get; set; }
        public string TenDonVi { get; set; }
        public string HoTen { get; set; }
        public List<string> GroupMembership { get; set; }
        public CauHinhTaiKhoan CauHinhTaiKhoan { get; set; }
    }

    public class CauHinhTaiKhoan
    {
        public string CapDoTaiKhoan { get; set; }
        public string MaSoGTVT { get; set; }
        public string MaBenXe { get; set; }
        public string MaSoThue { get; set; }
        public string MaSoThueDonViTruyenNhan { get; set; }
        public string TenDonViTruyenNhan { get; set; }
    }
}
public static class CoreExtension
{
    public static ThongTinTaiKhoan ThongTin(this ClaimsPrincipal User)
    {
        var cauHinh = new CauHinhTaiKhoan();
        if (!string.IsNullOrWhiteSpace(User.FindFirst("CauHinhTaiKhoan")?.Value + ""))
        {
            cauHinh = User.FindFirst("CauHinhTaiKhoan")?.Value.JsonParse<CauHinhTaiKhoan>();
        }
        return new ThongTinTaiKhoan
        {
            IdTaiKhoan = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value + ""),
            TenTaiKhoan = User.FindFirst("preferred_username")?.Value + "",
            HoTen = User.FindFirst(ClaimTypes.Surname)?.Value + " " + User.FindFirst(ClaimTypes.GivenName)?.Value,
            IdDonVi = string.IsNullOrWhiteSpace(User.FindFirst("IdDonVi")?.Value + "") ? Guid.Empty : Guid.Parse(User.FindFirst("IdDonVi")?.Value + ""),
            TenDonVi = User.FindFirst("TenDonVi")?.Value + "",
            GroupMembership = string.IsNullOrWhiteSpace(User.FindFirst("GroupMembership")?.Value + "") ? new List<string>() : (User.FindFirst("GroupMembership")?.Value + "").Split(",").ToList(),
            CauHinhTaiKhoan = cauHinh,
        };
    }
}
