﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.JWT
{
    public class PolicyClaimElement
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}
