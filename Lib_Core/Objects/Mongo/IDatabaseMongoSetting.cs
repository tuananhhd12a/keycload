﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo
{
    public interface IDatabaseMongoSettings
    {
        string DatabaseName { get; set; }
        string ConnectionString { get; set; }
    }

    public class DatabaseMongoSettings
    : IDatabaseMongoSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}
