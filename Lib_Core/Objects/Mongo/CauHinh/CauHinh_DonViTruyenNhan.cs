﻿using Lib_Core.Enum;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.CauHinh
{
    public class CauHinh_DonViTruyenNhan
    {
        [BsonId]
        public Guid _id { get; set; }
        public string TenDonVi { get; set; }
        public string MaSoThue { get; set; }
        public string DiaChi { get; set; }
        public string NguoiDaiDien { get; set; }
        public string SDT_NguoiDaiDien { get; set; }
        public string Email { get; set; }
        public string NguoiPhuTrachKyThuat { get; set; }
        public string SDT_PhuTrachKyThuat { get; set; }
        public string IP_MayChu { get; set; }
        public string PublicKey { get; set; }
        public DateTime ThoiGianBatDauHieuLuc { get; set; }
        public DateTime ThoiGianKetThucHieuLuc { get; set; }
        public ETrangThaiCauHinh TrangThai { get; set; }
        public string GhiChu { get; set; }
        public DateTime Created_at { get; set; }
        public Guid Created_by { get; set; }
        public string Created_by_name { get; set; }
        public DateTime? Updated_at { get; set; }
        public Guid? Updated_by { get; set; }
        public string Updated_by_name { get; set; }
    }
}
