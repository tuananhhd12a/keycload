﻿using Lib_Core.Enum;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.CauHinh
{
    public class CauHinh_NhanVienKyLenh
    {
        [BsonId()]
        public Guid _id { get; set; }
        public Guid IdDonVi { get; set; }
        public string MaDinhDanh { get; set; }
        public string HoTen { get; set; }
        public ELoaiChuKy LoaiChuKy { get; set; }
        public ETrangThaiCauHinh TrangThai { get; set; }
        public string Serial { get; set; }
        public string PublicKey { get; set; }
        public string TepDinhKem { get; set; }
        public string GhiChu { get; set; }
        public DateTime Created_at { get; set; }
        public Guid Created_by { get; set; }
        public string Created_by_name { get; set; }
        public DateTime? Updated_at { get; set; }
        public Guid? Updated_by { get; set; }
        public string Updated_by_name { get; set; }
    }
}
