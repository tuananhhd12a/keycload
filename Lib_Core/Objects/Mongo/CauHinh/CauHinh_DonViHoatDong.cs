﻿using Lib_Core.Enum;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.CauHinh
{
    public class CauHinh_DonViHoatDong
    {
        [BsonId]
        public Guid _id { get; set; }
        public Guid IdDonViTruyenNhan { get; set; }
        public string MaSoThue { get; set; }
        public string MaBenXe { get; set; }
        public string TenDonVi { get; set; }
        public string Email { get; set; }
        public string MaSoGTVT { get; set; }
        public string DiaChi { get; set; }
        public string PublicKey { get; set; }
        public List<string> DanhSachFile { get; set; }
        public EDoiTuongTruyenTai DoiTuongTruyenTai { get; set; }
        public ETrangThaiCauHinh TrangThai { get; set; }
        public DateTime Created_at { get; set; }
        public Guid Created_by { get; set; }
        public string Created_by_name { get; set; }
        public DateTime? Updated_at { get; set; }
        public Guid? Updated_by { get; set; }
        public string Updated_by_name { get; set; }
    }
}
