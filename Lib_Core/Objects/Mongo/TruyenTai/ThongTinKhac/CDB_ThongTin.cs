﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac
{
    public class CDB_ThongTin
    {
        public string TenTruong { get; set; }

        public string KieuDuLieu { get; set; }

        public string DuLieu { get; set; }
    }
}
