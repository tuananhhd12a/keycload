﻿using System.Xml.Serialization;

namespace Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac
{
    public class CDB_NhanVienKyLenh
    {
        public string MaDinhDanh { get; set; }

        public string HoTen { get; set; }

        public string LoaiGiayToTuyThan { get; set; }

        public string MaSoGiayToTuyThan { get; set; }
    }
}
