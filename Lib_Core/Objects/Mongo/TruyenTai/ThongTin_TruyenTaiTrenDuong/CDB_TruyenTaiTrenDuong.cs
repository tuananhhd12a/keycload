﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.TruyenTai.ThongTin_TruyenTaiTrenDuong
{
    public class CDB_TruyenTaiTrenDuong
    {
        [BsonId()]
        [JsonProperty("IdTruyenTai")]
        public Guid IdTruyenTai { get; set; }

        public Guid MaLenhCapPhat { get; set; }

        public string MaLenh { get; set; }

        public int SoKhach { get; set; }

        public string ToaDoGPS { get; set; }

        public int MaLoi { get; set; }

        public string ThongTinLoi { get; set; }

        public string DuLieuTiepNhan { get; set; }

        public Guid ID_NguoiTruyenTai { get; set; }

        public string HoTen_NguoiTruyenTai { get; set; }

        public DateTime NgayTruyenTai { get; set; }

        public DateTime ThoiGianTruyenTai { get; set; }
    }
}
