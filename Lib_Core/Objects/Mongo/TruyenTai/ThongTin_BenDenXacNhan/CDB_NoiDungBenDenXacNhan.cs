﻿using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan
{
    public class CDB_NoiDungBenDenXacNhan
    {
        public DateTime ThoiGianVaoBen { get; set; }

        public DateTime ThoiGianXacNhanTraKhach { get; set; }

        public int SoKhachKhiKyLenh { get; set; }

        public CDB_NhanVienKyLenh NhanVienKyLenh { get; set; }

        public List<CDB_ThongTin> ThongTinKhac { get; set; }
    }
}
