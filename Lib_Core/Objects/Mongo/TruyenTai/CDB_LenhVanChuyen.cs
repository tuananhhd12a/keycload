﻿using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDenXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan;
using CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen;
using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lib_Core.Objects.Mongo.TruyenTai
{
    public class CDB_LenhVanChuyen
    {
        [BsonId()]
        public Guid _id { get; set; }

        public Guid MaLenhCapPhat { get; set; }

        public string IdDoiTuongTruyenTai { get; set; }

        public string TenDoiTuongTruyenTai { get; set; }

        public string IdLoaiTruyenTai { get; set; }

        public string TenLoaiTruyenTai { get; set; }

        public string TenDonViTruyenTai { get; set; }

        public string MaSoDonViTruyenTai { get; set; }

        public string TenSoDonViTruyenTai { get; set; }

        public string MaSoThueDonViTruyenNhan { get; set; }

        public string TenDonViTruyenNhan { get; set; }

        public string MaLenhTiepNhan { get; set; }

        public string MaLenhVanChuyenBiThayThe { get; set; }

        public string KyHieuMauSoLenh { get; set; }

        public int MaLoaiLenh { get; set; }

        public string KyHieuLenh { get; set; }

        public string SoLenh { get; set; }

        public DateTime NgayKhoiTao { get; set; }

        public CDB_NoiDungLenhVanChuyen NoiDungLenhVanChuyen { get; set; }

        public CDB_NoiDungBenDiXacNhan NoiDungBenDiXacNhan { get; set; }

        public CDB_NoiDungBenDenXacNhan NoiDungBenDenXacNhan { get; set; }

        public List<CDB_ThongTin> ThongTinKhac { get; set; }

        public int MaLoi { get; set; }

        public string ThongTinLoi { get; set; }

        public string DuLieuTiepNhan { get; set; }

        public string FileXml { get; set; }

        public List<string> DanhSachFileDinhKem { get; set; }

        public Guid ID_NguoiTruyenTai { get; set; }

        public string HoTen_NguoiTruyenTai { get; set; }

        public DateTime NgayTruyenTai { get; set; }

        public DateTime ThoiGianTruyenTai { get; set; }

    }
}
