﻿using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_BenDiXacNhan
{
    public class CDB_NoiDungBenDiXacNhan
    {
        public DateTime ThoiGianVaoBen { get; set; }

        public DateTime GioXuatBenThucTe { get; set; }

        public int SoKhachKhiKyLenh { get; set; }

        public bool LenhDuDieuKien { get; set; }

        public DateTime HanPhuHieu { get; set; }

        public DateTime HanDangKiem { get; set; }

        public DateTime HanBaoHiem { get; set; }

        public List<CDB_LyDo> LyDoKhongDuDieuKienXuatBen { get; set; }

        public CDB_LaiXeXacNhanLenh LaiXeXacNhanLenh { get; set; }

        public CDB_NhanVienKyLenh NhanVienKyLenh { get; set; }

        public List<CDB_ThongTin> ThongTinKhac { get; set; }

    }
}
