﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.TruyenTai
{
    public class CDB_LenhCapPhat
    {
        [BsonId()]
        public Guid _id { get; set; }

        public string MaSoThueDoanhNghiep { get; set; }

        public string MaLenhTiepNhan { get; set; }

        public DateTime Created_at { get; set; }

        public Guid Created_by { get; set; }

        public string Created_by_name { get; set; }
    }
}
