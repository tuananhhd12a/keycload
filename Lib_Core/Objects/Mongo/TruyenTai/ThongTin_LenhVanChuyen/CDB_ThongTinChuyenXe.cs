﻿using System;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_ThongTinChuyenXe
    {
        public DateTime GioXuatBenKeHoach { get; set; }

        public DateTime GioXuatBenThucTe { get; set; }
    }
}
