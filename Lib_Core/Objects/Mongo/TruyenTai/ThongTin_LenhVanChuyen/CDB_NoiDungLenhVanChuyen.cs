﻿using Lib_Core.Objects.Mongo.TruyenTai.ThongTinKhac;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_NoiDungLenhVanChuyen
    {
        public CDB_ThongTinDoanhNghiep ThongTinDoanhNghiep { get; set; }

        public CDB_ThongTinChuyenXe ThongTinChuyenXe { get; set; }

        public CDB_TuyenVanChuyen TuyenVanChuyen { get; set; }

        public CDB_ThongTinPhuongTien ThongTinPhuongTien { get; set; }

        public List<CDB_LaiXe> DanhSachLaiXe { get; set; }

        public List<CDB_NhanVienPhucVu> DanhSachNhanVienPhucVu { get; set; }

        public List<CDB_ThongTin> ThongTinKhac { get; set; }

    }
}
