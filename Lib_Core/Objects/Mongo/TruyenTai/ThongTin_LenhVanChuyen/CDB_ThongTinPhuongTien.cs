﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_ThongTinPhuongTien
    {
        public CDB_PhuongTienTheoKeHoach PhuongTienTheoKeHoach { get; set; }

        public CDB_PhuongTienThucHien PhuongTienThucHien { get; set; }
    }
}
