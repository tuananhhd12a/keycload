﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_BenDen
    {
        public string MaSoGTVT { get; set; }

        public string TenSoGTVT { get; set; }

        public string MaBenXeNoiDen { get; set; }

        public string TenBenNoiDen { get; set; }
    }
}
