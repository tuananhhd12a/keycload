﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_ThongTinDoanhNghiep
    {
        public string TenDonViVanTai { get; set; }

        public string MaSoThueDonViVanTai { get; set; }

        public string MaSoGTVT { get; set; }

        public string DiaChi { get; set; }
    }
}
