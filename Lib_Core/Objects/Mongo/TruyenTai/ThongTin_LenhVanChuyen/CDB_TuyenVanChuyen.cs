﻿using System;
using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_TuyenVanChuyen
    {
        public string MaSoTuyen { get; set; }

        public string TenTuyen { get; set; }

        public float CuLyVanChuyen { get; set; }

        public string HanhTrinhTuyen { get; set; }

        public CDB_BenDi BenDi { get; set; }

        public CDB_BenDen BenDen { get; set; }
    }
}
