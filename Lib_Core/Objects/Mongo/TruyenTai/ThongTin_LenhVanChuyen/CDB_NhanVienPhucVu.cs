﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_NhanVienPhucVu
    {
        public int STT { get; set; }

        public string HoTen { get; set; }

        public string LoaiGiayToTuyThan { get; set; }

        public string MaSoGiayToTuyThan { get; set; }
    }
}
