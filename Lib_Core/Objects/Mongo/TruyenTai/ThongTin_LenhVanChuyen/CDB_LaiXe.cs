﻿using System.Xml.Serialization;

namespace CDB_TiepNhanTruyenTai.Models.Xml.ThongTinLenh.NoiDung_LenhVanChuyen
{
    public class CDB_LaiXe
    {
        public int STT { get; set; }

        public string HoTen { get; set; }

        public string SoGiayPhepLaiXe { get; set; }

        public string HangGiayPhepLaiXe { get; set; }
    }
}
