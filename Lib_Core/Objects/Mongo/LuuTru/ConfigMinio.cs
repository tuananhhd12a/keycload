﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.LuuTru
{
    public class ConfigMinio
    {
        [BsonId()]
        public Guid _id { get; set; }

        public string EndPoint { get; set; }

        public string Bucket { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string LoaiLuuTru { get; set; }
    }
}
