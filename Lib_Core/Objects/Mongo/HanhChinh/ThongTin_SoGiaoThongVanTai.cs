﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Mongo.HanhChinh
{
    public class ThongTin_SoGiaoThongVanTai
    {
        [BsonId]
        public Guid _id { get; set; }
        public string MaSoGTVT { get; set; }
        public string TenSoGTVT { get; set; }
    }
}
