﻿using Lib_Core.Objects.Responsive;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.API
{
    public static class ApiHelperExt
    {
        public static JObject JsonParse(this string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(json);
        }

        public static DataResponsive<T> JsonResult<T>(this string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<DataResponsive<T>>(json);
        } 
        
        public static T JsonDeserialize<T>(this string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }

        public static T JsonParse<T>(this string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
        public static string JsonStringify(this object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        public static T XmlParse<T>(this string xml)
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(xml))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }
}
