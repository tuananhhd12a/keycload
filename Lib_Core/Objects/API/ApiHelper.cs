﻿using Lib_Core.Objects.DungChung;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.API
{
    public static class ApiHelper
    {
        public static WebHeaderCollection Headers { get; } = new WebHeaderCollection();

        static HttpWebRequest Request(string Path)
        {
            var req = WebRequest.CreateHttp(Path);

            try
            {
                foreach (var Header in Headers.AllKeys)
                {
                    req.Headers.Add(Header, Headers[Header]);
                }
            }
            catch
            {

            }

            return req;
        }

        public static string Get(string _path, string token = "")
        {
            var req = Request(_path);
            if (req == null) return null;

            try
            {
                req.Method = "GET";
                if (!string.IsNullOrWhiteSpace(token))
                {
                    req.Headers.Add("Authorization", token);
                }
                var resp = req.GetResponse();
                var reader = new StreamReader(resp.GetResponseStream());
                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Post(string _domain, string _path, object _data, string token = "")
        {
            var path = "";
            if (string.IsNullOrWhiteSpace(_path))
            {
                path = _domain;
            }
            else
            {
                path = Path.Combine(_domain, _path);
            }
            var req = Request(path);
            if (req == null) return null;
            try
            {
                var json = JsonConvert.SerializeObject(_data);
                req.Method = "POST";
                if (!string.IsNullOrWhiteSpace(token))
                {
                    req.Headers.Add("Authorization", token);
                }
                byte[] byteArray = Encoding.UTF8.GetBytes(json);
                req.ContentType = "application/json";
                req.ContentLength = byteArray.Length;

                Stream dataStream = req.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                var resp = req.GetResponse();
                var reader = new StreamReader(resp.GetResponseStream());
                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IRestResponse RestSharp_Get(string domain, string Path, Dictionary<string, string> headers, List<Parameter> parameters)
        {
            try
            {
                var client = new RestClient(domain);
                if (headers.Count != 0)
                {
                    client.AddDefaultHeaders(headers);
                }
                var request = new RestRequest(Path, method: Method.GET);
                if (parameters.Count != 0)
                {
                    request.AddOrUpdateParameters(parameters);
                }
                return client.Get(request);
            }
            catch (Exception ex)
            {
                throw new Exception($"RestSharp_Get: {domain} / {Path}: {ex.Message}");
            }
        }

        public static IRestResponse RestSharp_Post(string domain, string Path, Dictionary<string, string> headers, object data)
        {
            try
            {
                var client = new RestClient(domain);
                if (headers.Count != 0)
                {
                    client.AddDefaultHeaders(headers);
                }
                var request = new RestRequest(Path, method: Method.POST, dataFormat: DataFormat.Json).AddJsonBody(data.Object2Json());
                return client.Post(request);
            }
            catch (Exception ex)
            {
                throw new Exception($"RestSharp_Post: {domain} / {Path}: {ex.Message}");
            }
        }

        public static IRestResponse RestSharp_Put(string domain, string Path, Dictionary<string, string> headers, object data)
        {
            try
            {
                var client = new RestClient(domain);
                if (headers.Count != 0)
                {
                    client.AddDefaultHeaders(headers);
                }
                var request = new RestRequest(Path, method: Method.PUT, dataFormat: DataFormat.Json).AddJsonBody(data.Object2Json());
                return client.Put(request);
                //if (string.IsNullOrWhiteSpace(response.ErrorMessage))
                //{
                //    return response.StatusCode;
                //}
                //else
                //{
                //    throw new Exception(response.ErrorMessage);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception($"RestSharp_Put: {domain} / {Path}: {ex.Message}");
            }
        }

        public static IRestResponse RestSharp_Delete(string domain, string Path, Dictionary<string, string> headers, object data)
        {
            try
            {
                var client = new RestClient(domain);
                if (headers.Count != 0)
                {
                    client.AddDefaultHeaders(headers);
                }
                var request = new RestRequest(Path, method: Method.DELETE);
                return client.Delete(request);
            }
            catch (Exception ex)
            {
                throw new Exception($"RestSharp_Delete: {domain} / {Path}: {ex.Message}");
            }
        }
    }
}
