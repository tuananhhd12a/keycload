﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Responsive
{
    public class CustomLoadResult<TypeDataResult>
    {
        //
        // Summary:
        //     A resulting dataset.
        public TypeDataResult data { get; set; }

        //
        // Summary:
        //     The total number of data objects in the resulting dataset.
        [DefaultValue(-1)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int totalCount { get; set; }
        //
        // Summary:
        //     The number of top-level groups in the resulting dataset.
        [DefaultValue(-1)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int groupCount { get; set; }
        //
        // Summary:
        //     Total summary calculation results.
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public object[] summary { get; set; }
    }
}
