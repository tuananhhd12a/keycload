﻿using Lib_Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.Responsive
{
    public class DataResponsive<TypeDataResult>
    {   /// <summary>
      ///     True thành công or False thất bại
      /// </summary>
        public bool status { get; set; }

        /// <summary>
        ///     Thông báo kèm theo mô tả lỗi
        /// </summary>
        public string message { get; set; }

        /// <summary>
        ///     Mã của lỗi sẽ được kèm theo khi xảy ra lỗi từ request
        /// </summary>
        public int errorCode { get; set; }

        /// <summary>
        ///     Dữ liệu được trả về nếu có
        /// </summary>
        public TypeDataResult data { get; set; }

        public DataResponsive<TypeDataResult> returnData(TypeDataResult obj, EMaLoi maLoi, string thongTinMoRong = "")
        {
            if (maLoi != EMaLoi.LOI_HE_THONG)
            {
                return new DataResponsive<TypeDataResult>()
                {
                    data = obj,
                    status = maLoi.Id == EMaLoi.THANH_CONG.Id ? true : false,
                    errorCode = maLoi.Id,
                    message = string.IsNullOrWhiteSpace(thongTinMoRong) ? maLoi.Name : thongTinMoRong
                };
            }
            else
            {
#if DEBUG
                return new DataResponsive<TypeDataResult>()
                {
                    data = obj,
                    status = false,
                    errorCode = maLoi.Id,
                    message = string.IsNullOrWhiteSpace(thongTinMoRong) ? maLoi.Name : thongTinMoRong
                };
#else
                return new DataResponsive<TypeDataResult>()
                {
                    data = obj,
                    status = false,
                    errorCode = maLoi.Id,
                    message = maLoi.Name
                };
#endif
            }
        }

        public DataResponsive<TypeDataResult> returnData(TypeDataResult obj, int code, string thongTinMoRong)
        {
            if (code != EMaLoi.LOI_HE_THONG.Id)
            {
                return new DataResponsive<TypeDataResult>()
                {
                    data = obj,
                    status = code == EMaLoi.THANH_CONG.Id ? true : false,
                    errorCode = code,
                    message = thongTinMoRong
                };
            }
            else
            {
#if DEBUG
                return new DataResponsive<TypeDataResult>()
                {
                    data = obj,
                    status = false,
                    errorCode = code,
                    message = thongTinMoRong
                };
#else
                return new DataResponsive<TypeDataResult>()
                {
                    data = obj,
                    status = false,
                    errorCode = code,
                    message = EMaLoi.LOI_HE_THONG.Name
                };
#endif
            }
        }

        public DataResponsive<TypeDataResult> returnData(TypeDataResult obj, System.Net.HttpStatusCode code)
        {
            switch (code)
            {
                case System.Net.HttpStatusCode.Unauthorized:
                    return returnData(obj, EMaLoi.LOI_THONG_TIN_XAC_THUC);
                case System.Net.HttpStatusCode.Forbidden:
                    return returnData(obj, EMaLoi.KHONG_CO_QUYEN);
                case System.Net.HttpStatusCode.Conflict:
                    return returnData(obj, EMaLoi.DU_LIEU_DA_TON_TAI, string.Format(EMaLoi.DU_LIEU_DA_TON_TAI.Name, "Tài khoản"));
                default:
                    return returnData(obj, EMaLoi.LOI_HE_THONG);

            }
        }
    }
}
