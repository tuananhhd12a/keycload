﻿using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.DungChung
{
    public static class FileExtension
    {
        public const int MaxLength = 2097152;

        public static byte[] GetBytes(string base64, out string errorMessage)
        {
            errorMessage = "";
            try
            {
                if (string.IsNullOrWhiteSpace(base64))
                {
                    errorMessage = "File không được bỏ trống!";
                    return null;
                }
                var bytes = Convert.FromBase64String(base64);
                if (bytes.Length > MaxLength)
                {
                    errorMessage = "File vượt quá dung lượng quy định!";
                    return null;
                }
                return bytes;
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, $"Getbytes từ database64 gặp lỗi");
                errorMessage = "File không hợp lệ";
                return null;
            }
        }
    }
}
