﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lib_Core.Enum;
using Lib_Core.Objects.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Objects.DungChung
{
    public static class Common
    {
        public static DateTime now
        {
            get
            {
                return DateTimeOffset.Now.UtcDateTime;
            }
        }

        public static DateTime nowWithTimeLocal
        {
            get
            {
                return now.ToLocalTime();
            }
        }

        public static DateTime minDate
        {
            get
            {
                return new DateTime(1999, 01, 01);
            }
        }

        public static DateTime maxDate
        {
            get
            {
                return new DateTime(9999, 12, 31);
            }
        }

        public static DateTime TimeSpan2DateTime(this TimeSpan time)
        {
            return DateTimeOffset.Now.UtcDateTime.Date.AddHours(-7) + time;
        }

        public static string cacheKey { get { return "CACHE_KEY"; } }

        public static TimeSpan cacheTime
        {
            get
            {
                return new TimeSpan(0, 5, 0);
            }
        }

        public static string Object2Json(this object a)
        {
            var setting = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                MaxDepth = 1
            };
            return JsonConvert.SerializeObject(a, setting);
        }

        public static string GetMessage(EMaLoi maLoi, string content)
        {
            var _content = string.Format(maLoi.Name, content).Replace("{0}", "").ToLower().Trim().ToUpperInvariant();

            return string.Format(maLoi.Name, content);
        }

        public static T ObjectNormalization<T>(this object myObject)
        {
            Type myType = myObject.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
            foreach (PropertyInfo prop in props)
            {
                if (prop.PropertyType == typeof(string))
                {
                    var propValue = prop.GetValue(myObject, null) + "";
                    if (!string.IsNullOrWhiteSpace(propValue))
                    {
                        propValue = propValue.Trim();
                        prop.SetValue(myObject, propValue);
                    }
                }
            }
            return (T)myObject;
        }

        public static int MaxDay
        {
            get
            {
                return 31;
            }
        }
    }
}
