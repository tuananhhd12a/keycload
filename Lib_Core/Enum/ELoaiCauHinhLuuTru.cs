﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class ELoaiCauHinhLuuTru : Enumeration<string>
    {
        public static readonly ELoaiCauHinhLuuTru CAU_HINH =
            new ELoaiCauHinhLuuTru("CAU_HINH", "Cấu hình đăng ký", "");

        public static readonly ELoaiCauHinhLuuTru TRUYEN_TAI =
            new ELoaiCauHinhLuuTru("TRUYEN_TAI", "Truyền tải lệnh vận chuyển", "");

        public static ELoaiCauHinhLuuTru GetLoaiCauHinhLuuTru(string id)
        {
            switch (id)
            {
                case "CAU_HINH":
                    return CAU_HINH;
                case "TRUYEN_TAI":
                    return TRUYEN_TAI;
                default:
                    return null;
            }
        }

        public ELoaiCauHinhLuuTru(string id, string name, string description) : base(id, name, description)
        {
        }

    }
}
