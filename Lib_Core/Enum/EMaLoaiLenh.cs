﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class EMaLoaiLenh : Enumeration<int>
    {
        public static readonly EMaLoaiLenh TUYEN_CO_DINH =
            new EMaLoaiLenh(1, "Tuyến cố định", "");

        public static readonly EMaLoaiLenh DI_THAY =
            new EMaLoaiLenh(2, "Đi thay", "");

        public static readonly EMaLoaiLenh TANG_CUONG =
            new EMaLoaiLenh(3, "Tăng cường", "");

        public static readonly EMaLoaiLenh XE_BUYT =
            new EMaLoaiLenh(4, "Xe buýt", "");

        public static EMaLoaiLenh GetLoaiLenh(int id)
        {
            switch (id)
            {
                case 1:
                    return TUYEN_CO_DINH;
                case 2:
                    return DI_THAY;
                case 3:
                    return TANG_CUONG;
                case 4:
                    return XE_BUYT;
                default:
                    return null;
            }
        }

        public EMaLoaiLenh(int id, string name, string description) : base(id, name, description)
        {
        }
    }
}
