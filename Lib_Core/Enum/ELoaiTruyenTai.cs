﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class ELoaiTruyenTai : Enumeration<string>
    {
        public static readonly ELoaiTruyenTai XUAT_BEN =
            new ELoaiTruyenTai("XUAT_BEN", "Xuất bến", "");

        public static readonly ELoaiTruyenTai DEN_BEN =
            new ELoaiTruyenTai("DEN_BEN", "Đến bến", "");

        public static readonly ELoaiTruyenTai TREN_DUONG =
            new ELoaiTruyenTai("TREN_DUONG", "Trên đường", "");

        public static readonly ELoaiTruyenTai THAY_THE =
            new ELoaiTruyenTai("THAY_THE", "Thay thế", "");

        public static readonly ELoaiTruyenTai XE_BUYT =
            new ELoaiTruyenTai("XE_BUYT", "Xe buýt", "");

        public static ELoaiTruyenTai GetLoaiTruyenTai(string id)
        {
            switch (id)
            {
                case "XUAT_BEN":
                    return XUAT_BEN;
                case "DEN_BEN":
                    return DEN_BEN;
                case "TREN_DUONG":
                    return TREN_DUONG;
                case "THAY_THE":
                    return THAY_THE;
                default:
                    return XE_BUYT;

            }
        }

        public ELoaiTruyenTai(string id, string name, string description) : base(id, name, description)
        {
        }
    }
}
