﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class ETrangThaiTruyenTai : Enumeration<string>
    {
        public static readonly ETrangThaiTruyenTai THANH_CONG =
            new ETrangThaiTruyenTai("THANH_CONG", "Thành công", "#13B000");

        public static readonly ETrangThaiTruyenTai KHONG_THANH_CONG =
            new ETrangThaiTruyenTai("KHONG_THANH_CONG", "Không thành công", "#D10909");

        public ETrangThaiTruyenTai(string id, string name, string description) : base(id, name, description)
        {
        }

    }
}
