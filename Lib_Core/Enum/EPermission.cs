﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public static class EPermission
    {
        #region Truyền tải

        public const string BEN_XE_TRUYEN_TAI_XUAT_BEN = "TruyenTai.BenXeTruyenTaiXuatBen";
        public const string BEN_XE_TRUYEN_TAI_DEN_BEN = "TruyenTai.BenXeTruyenTaiDenBen";
        public const string DOANH_NGHIEP_TRUYEN_TAI_XUAT_BEN = "TruyenTai.DoanhNghiepTruyenTaiXuatBen";
        public const string DOANH_NGHIEP_TRUYEN_TAI_DEN_BEN = "TruyenTai.DoanhNghiepTruyenTaiDenBen";
        public const string DOANH_NGHIEP_TRUYEN_TAI_XE_BUYT = "TruyenTai.DoanhNghiepTruyenTaiXeBuyt";
        public const string DOANH_NGHIEP_TRUYEN_TAI_LENH_THAY_THE = "TruyenTai.DoanhNghiepTruyenTaiLenhThayThe";
        public const string DOANH_NGHIEP_TRUYEN_TAI_TREN_DUONG = "TruyenTai.DoanhNghiepTruyenTaiTrenDuong";
        public const string CAU_HINH_LUU_TRU_FILE = "TruyenTai.CauHinhLuuTruFile";
        public const string XEM_FILE = "TruyenTai.XemFile";

        #endregion

        #region Cấu hình

        public const string DANH_SACH_CAU_HINH_DON_VI_TRUYEN_NHAN = "CauHinh.DanhSachDonViTruyenNhan";
        public const string CHI_TIET_CAU_HINH_DON_VI_TRUYEN_NHAN = "CauHinh.ChiTietDonViTruyenNhan";
        public const string CAU_HINH_DON_VI_TRUYEN_NHAN = "CauHinh.DonViTruyenNhan";
        public const string KICH_HOAT_CAU_HINH_DON_VI_TRUYEN_NHAN = "CauHinh.KichHoatDonViTruyenNhan";
        public const string TU_CHOI_KICH_HOAT_CAU_HINH_DON_VI_TRUYEN_NHAN = "CauHinh.TuChoiKichHoatDonViTruyenNhan";

        public const string DANH_SACH_CAU_HINH_DON_VI_HOAT_DONG = "CauHinh.DanhSachDonViHoatDong";
        public const string CAU_HINH_DON_VI_HOAT_DONG = "CauHinh.DonViHoatDong";
        public const string CHI_TIET_CAU_HINH_DON_VI_HOAT_DONG = "CauHinh.ChiTietDonViHoatDong";
        public const string KICH_HOAT_CAU_HINH_DON_VI_HOAT_DONG = "CauHinh.KichHoatDonViHoatDong";
        public const string TU_CHOI_KICH_HOAT_CAU_HINH_DON_VI_HOAT_DONG = "CauHinh.TuChoiDonViHoatDong";

        public const string DANH_SACH_NHAN_VIEN_KY_LENH = "CauHinh.DanhSachNhanVienKyLenh";
        public const string CAU_HINH_NHAN_VIEN_KY_LENH = "CauHinh.NhanVienKyLenh";
        public const string CHI_TIET_NHAN_VIEN_KY_LENH = "CauHinh.ChiTietNhanVienKyLenh";
        public const string KICH_HOAT_NHAN_VIEN_KY_LENH = "CauHinh.KichHoatNhanVienKyLenh";
        public const string TU_CHOI_NHAN_VIEN_KY_LENH = "CauHinh.TuChoiNhanVienKyLenh";
        public const string HUY_NHAN_VIEN_KY_LENH = "CauHinh.HuyNhanVienKyLenh";

        public const string CAU_HINH_XEM_FILE = "CauHinh.XemFile";

        #endregion

        #region Báo cáo

        public const string DANH_SACH_BAO_CAO_TRUYEN_TAI_DU_LIEU = "BaoCao.DanhSachTruyenTaiDuLieu";
        public const string DANH_SACH_BAO_CAO_TRUYEN_TAI_DU_LIEU_CUA_DOANH_NGHIEP = "BaoCao.DanhSachTruyenTaiDuLieuCuaDoanhNghiep";
        public const string DANH_SACH_BAO_CAO_TRUYEN_TAI_DU_LIEU_CUA_BEN_XE = "BaoCao.DanhSachTruyenTaiDuLieuCuaBenXe";
        public const string CHI_TIET_BAO_CAO_TRUYEN_TAI_DU_LIEU = "BaoCao.ChiTietTruyenTaiDuLieu";

        public const string BAO_CAO_XEM_FILE = "BaoCao.XemFile";
        public const string BAO_CAO_TAI_FILE = "BaoCao.TaiFile";

        #endregion
    }
}
