﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class ELoaiChuKy : Enumeration<string>
    {
        public static readonly ELoaiChuKy CHU_KY_SO =
            new ELoaiChuKy("CHU_KY_SO", "Chữ ký số", "");

        public static readonly ELoaiChuKy CHU_KY_THUONG =
            new ELoaiChuKy("CHU_KY_THUONG", "Chữ ký thường", "");

        public static ELoaiChuKy GetLoaiChuKy(string id)
        {
            switch (id)
            {
                case "CHU_KY_SO":
                    return CHU_KY_SO;
                case "CHU_KY_THUONG":
                    return CHU_KY_THUONG;
                default:
                    return null;
            }
        }

        public ELoaiChuKy(string id, string name, string description) : base(id, name, description)
        {
        }

    }
}
