﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class EDoiTuongKiemTra : Enumeration<string>
    {
        public static readonly EDoiTuongKiemTra DOANH_NGHIEP =
            new EDoiTuongKiemTra("DoanhNghiep", "Doanh nghiệp", "");

        public static readonly EDoiTuongKiemTra BEN_DI =
            new EDoiTuongKiemTra("BenDi", "Bến đi", "");

        public static readonly EDoiTuongKiemTra BEN_DEN =
            new EDoiTuongKiemTra("BenDen", "Bến đến", "");

        public static readonly EDoiTuongKiemTra THONG_TIN_CHUNG =
            new EDoiTuongKiemTra("ThongTinChung", "Thông tin chung", "");

        public static readonly EDoiTuongKiemTra NOI_DUNG_LENH_VAN_CHUYEN =
            new EDoiTuongKiemTra("NoiDungLenhVanChuyen", "Nội dung lệnh vận chuyển", "");

        public static readonly EDoiTuongKiemTra NOI_DUNG_BEN_DI_XAC_NHAN =
            new EDoiTuongKiemTra("NoiDungBenDiXacNhan", "Nội dung bến đi xác nhận", "");

        public static readonly EDoiTuongKiemTra NOI_DUNG_BEN_DEN_XAC_NHAN =
            new EDoiTuongKiemTra("NoiDungBenDenXacNhan", "Nội dung bến đến xác nhận", "");

        public EDoiTuongKiemTra(string id, string name, string description) : base(id, name, description)
        {
        }
    }
}
