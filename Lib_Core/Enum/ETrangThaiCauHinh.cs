﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class ETrangThaiCauHinh : Enumeration<string>
    {
        public static readonly ETrangThaiCauHinh CHO_KICH_HOAT =
            new ETrangThaiCauHinh("CHO_KICH_HOAT", "Chờ kích hoạt", "#FB8C00");

        public static readonly ETrangThaiCauHinh DANG_HOAT_DONG =
            new ETrangThaiCauHinh("DANG_HOAT_DONG", "Đang hoạt động", "#13B000");

        public static readonly ETrangThaiCauHinh TU_CHOI =
            new ETrangThaiCauHinh("TU_CHOI", "Từ chối", "#D10909");

        public static readonly ETrangThaiCauHinh BI_THAY_THE =
            new ETrangThaiCauHinh("BI_THAY_THE", "Bị thay thế", "#D10909");

        public static readonly ETrangThaiCauHinh HUY =
            new ETrangThaiCauHinh("HUY", "Hủy", "#D10909");

        public ETrangThaiCauHinh(string id, string name, string description) : base(id, name, description)
        {
        }

    }
}
