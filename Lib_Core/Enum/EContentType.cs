﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public static class EContentType
    {
        public const string Xml = "application/xml";
        public const string Pdf = "application/pdf";
        public const string Jpg = "image/jpeg";
    }
}
