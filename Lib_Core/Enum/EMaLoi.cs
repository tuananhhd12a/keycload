﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class EMaLoi : Enumeration<int>
    {
        public static readonly EMaLoi THANH_CONG =
            new EMaLoi(00, "Thành công", "");

        public static readonly EMaLoi LOI_THONG_TIN_XAC_THUC =
            new EMaLoi(401, "Thông tin xác thực không hợp lệ", "");

        public static readonly EMaLoi KHONG_CO_QUYEN =
            new EMaLoi(403, "Tài khoản không có quyền thực hiện", "");

        public static readonly EMaLoi LOI_HE_THONG =
            new EMaLoi(500, "Lỗi hệ thống. Vui lòng thử lại!", "");

        public static readonly EMaLoi DU_LIEU_KHONG_DUNG_DINH_DANG =
            new EMaLoi(1001, "Dữ liệu không đúng định dạng", "");

        public static readonly EMaLoi KHONG_TIM_THAY_DU_LIEU =
            new EMaLoi(1002, "Không tìm thấy dữ liệu", "");

        public static readonly EMaLoi CHU_KY_KHONG_DUNG =
            new EMaLoi(1009, "Chữ ký không đúng", "");

        public static readonly EMaLoi DON_HANG_KHONG_HOP_LE =
            new EMaLoi(1201, "Trong các đơn hàng truyền sang có đơn hàng không hợp lệ", "");

        public static readonly EMaLoi GIAO_DICH_KHONG_TON_TAI =
            new EMaLoi(1102, "Giao dịch không tồn tại", "");

        public static readonly EMaLoi GIAO_DICH_HET_HAN =
            new EMaLoi(1103, "Phiên giao dịch đã hết hạn", "");

        public static readonly EMaLoi GIAO_DICH_DA_TON_TAI =
             new EMaLoi(1104, "Giao dịch đã tồn tại", "");

        public static readonly EMaLoi GIAO_DICH_BI_HUY =
            new EMaLoi(1105, "Giao dịch đã bị hủy", "");

        public static readonly EMaLoi TRANG_THAI_GHE_KHONG_HOP_LE =
             new EMaLoi(1106, "Trạng thái chỗ không hợp lệ", "");

        public static readonly EMaLoi TRANG_THAI_CHUYEN_DI_KHONG_HOP_LE =
             new EMaLoi(1107, "Trạng thái chuyến đi không hợp lệ", "");

        public static readonly EMaLoi TRANG_THAI_LENH_KHONG_HOP_LE =
             new EMaLoi(1108, "Trạng thái lệnh không hợp lệ", "");

        public static readonly EMaLoi MA_DAT_CHO_BI_HUY =
             new EMaLoi(1109, "Mã đặt chỗ đã bị hủy", "");

        public static readonly EMaLoi TAI_KHOAN_KHONG_DUOC_PHAN_CONG =
             new EMaLoi(1110, "Tài khoản không được phân công", "");

        public static readonly EMaLoi GIAO_DICH_DANG_DUOC_THUC_HIEN =
             new EMaLoi(1111, "Giao dịch đang được thực hiện", "");

        public static readonly EMaLoi DU_LIEU_DA_BI_XOA =
             new EMaLoi(1112, "Dữ liệu {0} đã bị xóa", "");

        public static readonly EMaLoi DU_LIEU_KHONG_HOP_LE =
             new EMaLoi(1113, "Dữ liệu không hợp lệ", "");

        public static readonly EMaLoi LENH_KHONG_HOP_LE =
             new EMaLoi(1114, "Lệnh không hợp lệ", "");

        public static readonly EMaLoi KHONG_SU_DUNG_DICH_VU_LENH_DIEN_TU =
            new EMaLoi(1115, "Doanh nghiệp không sử dụng lệnh điện tử", "");

        public static readonly EMaLoi DU_LIEU_DA_DUOC_SU_DUNG =
             new EMaLoi(1116, "Dữ liệu đã được sử dụng nên không thể xoá", "");

        public static readonly EMaLoi TONG_TIEN_KHONG_DUNG =
            new EMaLoi(1265, "Tổng tiền không đúng", "");

        public static readonly EMaLoi GIAO_DICH_BI_TREO =
            new EMaLoi(3188, "Giao dịch đang bị treo", "");

        public static readonly EMaLoi DU_LIEU_DA_TON_TAI =
            new EMaLoi(1202, "{0} đã tồn tại", "");

        public static readonly EMaLoi DOANH_NGHIEP_CHUA_SU_DUNG_DICH_VU =
            new EMaLoi(1203, "Doanh nghiệp chưa sử dụng dịch vụ", "");

        public static readonly EMaLoi LAI_XE_CHUA_TIEP_NHAN =
            new EMaLoi(1204, "Lái xe chưa tiếp nhận lệnh", "");

        public static readonly EMaLoi CHUA_CAU_HINH_CAI_DAT =
            new EMaLoi(1205, "Chưa cấu hình cài đặt {0}", "");

        public static readonly EMaLoi KHONG_TIM_THAY_DU_LIEU_HE_THONG =
            new EMaLoi(1207, "Không tìm thấy dữ liệu {0}!", "");

        public static readonly EMaLoi DU_LIEU_HE_THONG_DA_BI_XOA =
            new EMaLoi(1208, "{0} đã bị xoá!", "");

        public static readonly EMaLoi DU_LIEU_HE_THONG_KHONG_HOP_LE =
             new EMaLoi(1209, "{0} không hợp lệ", "");

        public static readonly EMaLoi XE_KHONG_SU_DUNG_SO_DO_CHO =
             new EMaLoi(1210, "Xe không sử dụng sơ đồ chỗ!", "");

        public static readonly EMaLoi XML_KHONG_HOP_LE =
             new EMaLoi(1211, "XML không hợp lệ!", "");

        public static readonly EMaLoi DON_VI_TRUYEN_NHAN_KHONG_HOP_LE =
             new EMaLoi(1212, "Đơn vị truyền nhận không hợp lệ!", "");

        public static readonly EMaLoi DON_VI_HOAT_DONG_KHONG_HOP_LE =
             new EMaLoi(1213, "Đơn vị hoạt động không hợp lệ!", "");

        public static readonly EMaLoi FILE_XML_KHONG_HOP_LE =
             new EMaLoi(1214, "File xml không hợp lệ!", "");

        public static readonly EMaLoi TEP_DINH_KEM_KHONG_HOP_LE =
             new EMaLoi(1215, "Tệp đính kèm không hợp lệ!", "");


        public EMaLoi(int id, string name, string description) : base(id, name, description)
        {
        }
    }
}
