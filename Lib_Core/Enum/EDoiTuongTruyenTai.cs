﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class EDoiTuongTruyenTai : Enumeration<string>
    {
        public static readonly EDoiTuongTruyenTai BEN_XE =
            new EDoiTuongTruyenTai("BEN_XE", "Bến xe", "");

        public static readonly EDoiTuongTruyenTai DOANH_NGHIEP_VAN_TAI =
            new EDoiTuongTruyenTai("DOANH_NGHIEP_VAN_TAI", "Doanh nghiệp vận tải", "");

        public static EDoiTuongTruyenTai GetDoiTuongTruyenTai(string id)
        {
            switch (id)
            {
                case "BEN_XE":
                    return BEN_XE;
                case "DOANH_NGHIEP_VAN_TAI":
                    return DOANH_NGHIEP_VAN_TAI;
                default:
                    return null;
            }
        }

        public EDoiTuongTruyenTai(string id, string name, string description) : base(id, name, description)
        {
        }

    }
}
