﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.Enum
{
    public class Enumeration<TypeId>
         where TypeId : IComparable
    {
        protected Enumeration(TypeId id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }

        public string Description { get; }

        public string Name { get; }

        public TypeId Id { get; }

        public override string ToString()
        {
            return Name;
        }

        public static IEnumerable<T> GetAll<T>() where T : Enumeration<TypeId>
        {
            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration<TypeId>;

            if (otherValue == null)
            {
                return false;
            }

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = Id.Equals(otherValue.Id);

            return typeMatches && valueMatches;
        }

        public int CompareTo(object other)
        {
            return Id.CompareTo(((Enumeration<TypeId>)other).Id);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
