﻿using Lib_Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lib_Core.CheckSum
{
    public class DataValidation
    {
        /// <summary>
        /// Xác thực checksum
        /// </summary>
        /// <param name="publicKey"></param>
        /// <param name="data"></param>
        /// <param name="checkSum"></param>
        /// <returns></returns>
        public static bool VerifySignData(string publicKey, object data, string checkSum)
        {
            try
            {
                var props = data.GetType().GetProperties()
                .Select(prop => new
                {
                    prop,
                    order = prop.GetCustomAttributes(typeof(CheckSumOrder), true).FirstOrDefault() as CheckSumOrder///prop.GetAllAttributes<CheckSumOrder>().FirstOrDefault()
                })
                .Where(prop => prop.order != null)
                .OrderBy(prop => prop.order.Index)
                .Select(prop => prop.prop);
                string sSource = string.Join("", props.Select(prop => prop.GetValue(data)));
                byte[] bSign = Convert.FromBase64String(checkSum);
                byte[] bSource = Encoding.UTF8.GetBytes(sSource);
                publicKey = publicKey.Replace("-----BEGIN CERTIFICATE-----", "")
                                     .Replace("-----END CERTIFICATE-----", "")
                                     .Replace("-----BEGIN PUBLIC KEY-----", "")
                                     .Replace("-----END PUBLIC KEY-----", "");
                byte[] bKey = Convert.FromBase64String(publicKey);
                using (var cer = new X509Certificate2(bKey))
                {
                    using (var pubKey = cer.GetRSAPublicKey()!)
                    {
                        if (pubKey is not null)
                        {
                            if (pubKey.VerifyData(bSource, bSign, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1))
                            {
                                Serilog.Log.Error($"VerifySignData - Xác thực hợp lệ!");
                                return true;
                            }
                            else
                            {
                                Serilog.Log.Error($"VerifySignData - Xác thực không khớp!");
                                return false;
                            }
                        }
                        else
                        {
                            Serilog.Log.Error($"VerifySignData - Không tìm thấy publicKey!");
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, $"VerifySignData gặp lỗi!");
                return false;
            }
        }

        /// <summary>
        /// Tạo checksum
        /// </summary>
        /// <param name="publicKey"></param>
        /// <param name="privateKey"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string CreateSignData(string publicKey, string privateKey, object data)
        {
            try
            {
                Serilog.Log.Information($"CreateSignData - Bắt đầu gen chữ ký");
                var props = data.GetType().GetProperties()
                .Select(prop => new
                {
                    prop,
                    order = prop.GetCustomAttributes(typeof(CheckSumOrder), true).FirstOrDefault() as CheckSumOrder///prop.GetAllAttributes<CheckSumOrder>().FirstOrDefault()
                })
                .Where(prop => prop.order != null)
                .OrderBy(prop => prop.order.Index)
                .Select(prop => prop.prop);
                string sSource = string.Join("", props.Select(prop => prop.GetValue(data)));
                var bSource = Encoding.UTF8.GetBytes(sSource);
                using (var cer = X509Certificate2.CreateFromPem(publicKey, privateKey))
                {
                    using (var priKey = cer.GetRSAPrivateKey()!)
                    {
                        if (priKey is not null)
                        {
                            var sign = priKey.SignData(bSource, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
                            Serilog.Log.Information($"CreateSignData - Gen chữ ký thành công");
                            return Convert.ToBase64String(sign);
                        }
                        else
                        {
                            Serilog.Log.Error($"CreateSignData - Không tìm thấy privateKey");
                            return "";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, $"CreateSignData - Gen chữ ký gặp lỗi");
                return "";
            }
        }

        public static X509Certificate2 GetX509(string publicKey)
        {
            try
            {
                publicKey = publicKey.Replace("-----BEGIN CERTIFICATE-----", "")
                                     .Replace("-----END CERTIFICATE-----", "")
                                     .Replace("-----BEGIN PUBLIC KEY-----", "")
                                     .Replace("-----END PUBLIC KEY-----", "");
                byte[] bKey = Convert.FromBase64String(publicKey);
                return new X509Certificate2(bKey);
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, $"GetX509 Error");
                return null;
            }
        }

        public static string GetCertificateFromXml(string xml, EDoiTuongKiemTra doiTuong)
        {
            try
            {
                var cer = "";
                XmlDocument xmlDocument = new XmlDocument()
                {
                    PreserveWhitespace = false,
                };
                xmlDocument.LoadXml(xml);

                SignedXml signedXml = new SignedXml(xmlDocument);
                XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");
                foreach (var node in nodeList)
                {
                    var data = node as XmlNode;
                    if (data.ParentNode.Name == doiTuong.Name)
                    {
                        var x509Certificate = ((XmlElement)node).GetElementsByTagName("X509Certificate");
                        if (x509Certificate.Count != 0)
                        {
                            var n = x509Certificate.Item(0) as XmlNode;
                            cer = n.FirstChild.Value;
                        }
                    }
                }
                if (string.IsNullOrWhiteSpace(cer))
                {
                    Serilog.Log.Error($"Không thấy dữ liệu publicKey");
                }
                return cer;
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, $"GetCertificateFromXml Error");
                return "";
            }
        }

        public static bool VerifyXml(string xml, EDoiTuongKiemTra doiTuong, out string messageError)
        {
            messageError = "";
            try
            {
                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.PreserveWhitespace = false;

                xmlDocument.LoadXml(xml);

                XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");

                var isVerify = false;

                foreach (var node in nodeList)
                {
                    if (((XmlNode)node).ParentNode.Value == doiTuong.Id)
                    {
                        isVerify = true;

                        SignedXml signedXml = new SignedXml(xmlDocument);

                        signedXml.LoadXml((XmlElement)node);

                        if (!signedXml.CheckSignature())
                        {
                            messageError = $"Chữ ký số thẻ {doiTuong.Name} không hợp lệ";
                        }
                    }
                }
                if (!isVerify)
                {
                    messageError = $"{doiTuong.Name} chưa thực hiện ký số";
                }
                return string.IsNullOrWhiteSpace(messageError);
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, $"VerifyXml Error");
                messageError = "Xml không hợp lệ";
                return false;
            }
        }
    }
}
