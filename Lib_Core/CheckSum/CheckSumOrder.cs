﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Core.CheckSum
{
    public class CheckSumOrder : Attribute
    {
        public int Index { get; }

        public CheckSumOrder(int index)
        {
            Index = index;
        }
    }
}
